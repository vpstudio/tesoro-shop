<?php
/**
 * Single Product Rating
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/rating.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( 'no' === get_option( 'woocommerce_enable_review_rating' ) ) {
	return;
}

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();
$brand = !empty(wp_get_post_terms( $product->get_id(), 'brand' )) ? wp_get_post_terms( $product->get_id(), 'brand' )[0] : null;
$is_hide_price = !is_null($brand) ? fw_get_db_term_option($brand->term_id, 'brand', 'product_іs_hide_price', null) : false;

$wrapper_class = '';
if ($is_hide_price) $wrapper_class = 'get-price-block';
?>
<div class="ts-single-wrapper <?php echo $wrapper_class; ?>">
<?php //if ( $rating_count >= 0 ) : ?>
	<div class="woocommerce-product-rating">
	<?php echo wc_get_rating_html( $average, $rating_count ); ?>
	<?php //if ( comments_open() && $rating_count > 0 ) : ?>
	<a href="#reviews" class="woocommerce-review-link" rel="nofollow">(<?php printf( _n( '%s customer review', '%s customer reviews', $review_count, 'woocommerce' ), '<span class="count">' . esc_html( $review_count ) . '</span>' ); ?>)</a>
	<?php //endif ?>
	<div class="ts-guaranty"><?php _e('Гарантия от производителя — <br>6 месяцев. Бесплатная доставка.', 'tesoro-shop'); ?></div>
</div>
<?php //endif; ?>
