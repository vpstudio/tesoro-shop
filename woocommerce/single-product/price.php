<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

global $product;

$brand = !empty(wp_get_post_terms( $product->get_id(), 'brand' )) ? wp_get_post_terms( $product->get_id(), 'brand' )[0] : null;
$is_hide_price = !is_null($brand) ? fw_get_db_term_option($brand->term_id, 'brand', 'product_іs_hide_price', false) : false;

if (!$is_hide_price) {
?>
	<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

		<p class="price"><?php echo $product->get_price_html(); ?></p>

		<meta itemprop="price" content="<?php echo $product->get_price(); ?>" /> 
		<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
		<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
	</div>
</div>
<?php } else { ?> 

	<div class="get-price-schema" itemprop="offers" itemscope itemtype="http://schema.org/Offer">

		<p class="price"><?php _e('Цена по запросу', 'tesoro-shop'); ?></p>

		<meta itemprop="price" content="<?php _e('Цена по запросу', 'tesoro-shop'); ?>" /> 
		<meta itemprop="priceCurrency" content=" " />
		<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
	</div>
	<style>
		.yith-par-message {
    		display: none !important;
		}
	</style>
</div>
<?php }


