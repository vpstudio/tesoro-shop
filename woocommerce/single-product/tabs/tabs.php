<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} 
global $product;

$brand = wp_get_post_terms( $product->get_id(), 'brand' );
$gift_box = fw_get_db_post_option( $product->get_id(), 'gift_box' );
$bundle_id = fw_get_db_post_option( $product->get_id(), 'bundle_id' );
$phone = fw_get_db_settings_option('phone');

$exchange_page_link = fw_get_db_settings_option('exchange_page_link');
$shipping_payment_page_link = fw_get_db_settings_option('shipping_payment_page_link');

?>
<div class="woocommerce-product-boxes">
		
		<div class="row owl-carousel owl-theme">
			<?php if ( !empty($brand) ) { 
				$brand_name = $brand[0]->name;
				$brand_img_id = fw_get_db_term_option($brand[0]->term_id, 'brand', 'brand_thumbnail') ? fw_get_db_term_option($brand[0]->term_id, 'brand', 'brand_thumbnail')['attachment_id'] : '';
				$brand_img = wp_get_attachment_image_src( $brand_img_id, 'tesoro-shop-product-box' );
			?>
			<div class="col">
				<a href="<?php echo get_term_link( $brand[0]->term_id, 'brand' ); ?>" rel="details" target="_blank" title="<?php _e('Страница бренда', 'teso-shop'); echo ' '.$brand_name; ?>" class="woocommerce-product-box">
					<div class="woocommerce-product-box__content">	
						<h4 class="woocommerce-product-box__title woocommerce-product-box__title--grey"><?php _e('Официальный представитель', 'tesoro-shop'); ?></h4>
						<div class="woocommerce-product-box__text">
						<?php printf( __("Мы являемся официальным представителем бренда %s в Украине", 'tesoro-shop'), $brand_name ); ?>
						</div>
					</div>
					<img class="woocommerce-product-box__image img-responsive" src="<?php echo $brand_img[0]; ?>" alt="<?php echo $brand_name; ?>">	
				</a>
			</div>
			<?php } ?>
			<?php if ( $gift_box['gadget'] == 'yes' ) { 
				$gift_box_id = !empty( $gift_box['yes']['gift_box_img'] ) ? $gift_box['yes']['gift_box_img']['attachment_id'] : '';

				if (!empty( wp_get_attachment_image_src( $gift_box_id, 'tesoro-shop-product-box' ) ) ) {
					$gift_box_img = wp_get_attachment_image_src( $gift_box_id, 'tesoro-shop-product-box' );
					$gift_box_alt = get_post_meta($gift_box_id, '_wp_attachment_image_alt', true);
				} else {
					$gift_box_img[] = get_template_directory_uri() . '/dist/images/gift-box.jpg';
					$gift_box_img[] = '135';
					$gift_box_img[] = '122';
					$gift_box_alt = __('Подарочная упаковка', 'tesoro-shop');
				}
			?>
			<div class="col">
				<div class="woocommerce-product-box">
					<div class="woocommerce-product-box__content">	
						<h4 class="woocommerce-product-box__title woocommerce-product-box__title--grey"><?php _e('Подарочная упаковка', 'tesoro-shop'); ?></h4>
						<div class="woocommerce-product-box__text"><?php _e('Покупая товар в нашем магазине, Вы получаете его в фирменной брендовой упаковке.', 'tesoro-shop'); ?></div>
					</div>
					<img class="woocommerce-product-box__image" width="<?php echo $gift_box_img[1]; ?>" height="<?php echo $gift_box_img[2]; ?>" src="<?php echo $gift_box_img[0]; ?>" alt="<?php echo $gift_box_alt; ?>">
				</div>
			</div>
			<?php } ?>
			<div class="col">
				<div class="woocommerce-product-box woocommerce-product-box--social">
					<div class="woocommerce-product-box__content">	
						<h4 class="woocommerce-product-box__title woocommerce-product-box__title--grey"><?php _e('Поделиться с друзьями', 'tesoro-shop'); ?></h4>
						<ul class="woocommerce-product-box__soc">
							<li>
								<!-- Load Facebook SDK for JavaScript -->
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
								js = d.createElement(s); js.id = id;
								js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.2';
								fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>
								<!-- Your share button code -->
								<div 
									lass="fb-share-button" 
									data-href="<?php echo urlencode( get_permalink($product->get_ID()) ); ?>" 
									data-layout="button" 
									data-size="large" 
									data-mobile-iframe="true">
									<a rel="noopener noreferrer nofollow" target="_blank" 
									href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( get_permalink($product->get_ID()) ); ?>&amp;src=sdkpreparse" 
									class="fb-xfbml-parse-ignore"><i class="fa fa-facebook"></i></a></div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="row owl-carousel owl-theme">
			<div class="col">			
				<div class="woocommerce-product-box woocommerce-product-box--icon woocommerce-product-box--grey">
					<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						<rect width="32" height="32" fill="url(#pattern0)"/>
						<defs>
						<pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
						<use xlink:href="#image0" transform="scale(0.03125)"/>
						</pattern>
						<image id="image0" width="32" height="32" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NEQwRUExNkQxQjUxMTFFNjlFRTNDREUzMzcxRkI2QzMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NEQwRUExNkUxQjUxMTFFNjlFRTNDREUzMzcxRkI2QzMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0RDBFQTE2QjFCNTExMUU2OUVFM0NERTMzNzFGQjZDMyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0RDBFQTE2QzFCNTExMUU2OUVFM0NERTMzNzFGQjZDMyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pn2gG1EAAANHSURBVHja7JfLSxtRFIedxEWgaAwxoCTERI1NiFr62BixKiql1HZT4rou2iy6KRQUWlDbrvIHuEgkQldFSnHRQmnzMJLUqsRCm4ImYB6SkBpB4yIgltaeM3hlHGcymUkLXXghZO7Mvef33XPOfVFHR0dVUkoikej1er1T+Dw0NDTV3Ny8KMUOJRaACMN/H0VRdGewQQFAUApI2QBsYYvF8nZwcPAZfvP5fBPr6+t3pIAIArCFzWbzOxTQarVfmO2y2ewVAJkEkNtiQHgBmMLknU6ni9jt9tGGhobvfAYRBPttbGwMlwNyBgAFwcAkEdbr9csGgyG8urp6/+DgQAle+N3R0fFmYGDgBYBES4BcRTtCICcAbGGj0RgCkecmk8mHdRRfWlp6GA6HHxWLRQ2CtLe3z2ObxsbGb3wgmUzmGoYGQG5xgVCbm5unhFtaWhYwufhcdnh4eGFlZeVBKBR6vL+/r8W8sFqt89AHQb4KgUCODGOdgMgVCkVyb2/PQBqq1eoEQARVKtUWlyG5XP6zqalpuaura1qpVGa3t7et6XTaBlCOXC53SaPRxGpqan6w++3s7JhjsdjNQqGgxzpqrq2t3ZN3d3fTi4nD4eiHl0bwSH8kEhlNJpO9AJGCX5oLRCaT/cKktNls0widz+ctqVSqB3LFAfG/DCDx2traHHp2bm7upd/vn0Bx9OzIyMgoitMhGBsbo5PA6XRSjFzA7O89dtUihgRDU2o6QXxl0Wj0biAQeIqewNDU19fHYeQXiZ3j2AexPj4+TutWsw1hA/BGHxPE7XYHMCkRpLW11c85nSApOzs7X9fV1W29grK7u2tEcbYwu1TzjYgLZGZmxgdT8hNmfltb20dmewhZD44+Ho/f4BoxX6kWWiq5QDwezwdIxM8Igq4G4ScAcF2McNkApUBmZ2ffM76LEhYNwAXicrkWyAwSK3wym6okFqagVPGKAP5WOQc4B/h/AHBOkw3oXxbUIOsHvRDhIkLOfvAhKPV4LeZUzVw52Ucy5jYsCEK2VLKVixEmixffobQskFIAQsLlHMsFQbgAyhUWczHhBWECiBWWcjU7A8IQC4oVruRyegqk0vMAVcH1vI91PZe0Jf8RYADMYLhkim/BdgAAAABJRU5ErkJggg=="/>
						</defs>
					</svg>
					<div class="woocommerce-product-box__content">	
						<h4 class="woocommerce-product-box__title woocommerce-product-box__title--grey"><?php _e('Доставка и оплата', 'tesoro-shop'); ?></h4>
						<div class="woocommerce-product-box__text"><?php printf( __('Мы заботимся о комфорте своих клиентов, поэтому мы предоставляем выбор в способах доставки. Ознакомиться с ними Вы можете <a href="%s" target="_blank" rel="details">тут</a>.', 'tesoro-shop'), $shipping_payment_page_link ); ?></div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="woocommerce-product-box woocommerce-product-box--icon woocommerce-product-box--grey">
					<svg width="24" height="18" viewBox="0 0 24 18" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M0 5L5 -0.000328064V3.99967L15 3.99967V5.99967L5 5.99967L5 9.99967L0 5ZM19 13.9997L9 13.9997V11.9997L19 11.9997V7.99967L24 13L19 17.9997V13.9997Z" fill="#757575"/>
					</svg>
					<div class="woocommerce-product-box__content">	
						<h4 class="woocommerce-product-box__title woocommerce-product-box__title--grey"><?php _e('Обмен', 'tesoro-shop'); ?></h4>
						<div class="woocommerce-product-box__text"><?php printf( __('Приобретенные товары Вы можете обменять или вернуть в течении 14 дней. С условиями обмена и возврата Вы можете ознакомиться <a href="%s" target="_blank" rel="details">тут</a>.', 'tesoro-shop'), $exchange_page_link ); ?></div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="woocommerce-product-box woocommerce-product-box--phone woocommerce-product-box--grey">
					<div class="woocommerce-product-box__content">	
						<h4 class="woocommerce-product-box__title woocommerce-product-box__title--grey"><?php _e('Колл-центр', 'tesoro-shop'); ?></h4>
						<div class="woocommerce-product-box__text"><?php _e('Мы всегда готовы помочь Вам подобрать товар и ответить на любые Ваши вопросы, звоните:', 'tesoro-shop'); ?></div>
						<a class="woocommerce-product-box__phone" rel="noopener noreferrer nofollow" href="tel:<?php echo str_replace([' ', '(', ')', '+', '-'], '', $phone); ?>"><i class="fa fa-phone "></i><?php echo $phone; ?></a>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="woocommerce-product-box woocommerce-product-box--callback woocommerce-product-box--grey">
					<div class="woocommerce-product-box__content">	
						<h4 class="woocommerce-product-box__title woocommerce-product-box__title--grey"><?php _e('Обратный звонок', 'tesoro-shop'); ?></h4>
						<div class="woocommerce-product-box__text"><?php _e('Если желаете, мы можем перезвонить Вам.', 'tesoro-shop'); ?></div>
						<a class="callback button" rel="noopener noreferrer nofollow" href="#callback-form"><?php _e('Заказать звонок', 'tesoro-shop'); ?></a>
					</div>
				</div>
			</div>

		</div>
		

</div>
<?php
/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

	<div class="woocommerce-tabs wc-tabs-wrapper">
		<ul class="tabs wc-tabs" role="tablist">
			<?php foreach ( $tabs as $key => $tab ) : ?>
				<li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-<?php echo esc_attr( $key ); ?>" role="tab" aria-controls="tab-<?php echo esc_attr( $key ); ?>">
					<a href="#tab-<?php echo esc_attr( $key ); ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php foreach ( $tabs as $key => $tab ) : ?>
			<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content wc-tab" id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel" aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
				<?php if ( isset( $tab['callback'] ) ) { call_user_func( $tab['callback'], $key, $tab ); } ?>
			</div>
		<?php endforeach; ?>
	</div>

<?php endif; ?>
<?php $bundle_product = wc_get_product( $bundle_id );
if ( $bundle_product && $bundle_product->is_type( 'yith_bundle' ) ) { 
	$bundle_products = $bundle_product->get_bundled_items();	?>
<section class="woocommerce-bundle-section">
		<h2><?php _e('Комплектом дешевле', 'tesoro-shop'); ?></h2>
		<div class="woocommerce-bundle">
			<div class="woocommerce-bundle-products">
				<?php echo ts_build_products($bundle_products, 'bundle'); ?>
			</div>
			<div class="woocommerce-bundle-price">
				<?php 
					if ( $bundle_product->get_regular_price() != $bundle_product->get_price() ) {
						echo '<div class="product-price-wrapper" >';
						echo '<span class="old-price">'. $bundle_product->get_regular_price() .'</span> ';
						echo ts_get_sale($bundle_product);
						echo '</div>';
						echo wc_price( $bundle_product->get_price() );
					} else {
						echo '<span class="old-price"></span> ';
						echo wc_price( $bundle_product->get_price() );
					}

					echo apply_filters( 'woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.
					sprintf( '<a href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s product_type_%s" %s>%s</a>',
						esc_url( $bundle_product->add_to_cart_url() ),
						esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 ),
						esc_attr( $bundle_product->get_id() ),
						esc_attr( $bundle_product-> get_sku() ),
						esc_attr( isset( $args['class'] ) ? $args['class'] : 'button alt' ),
						esc_attr( $bundle_product->get_type() ),
						isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',
						esc_html( __('Купить комплект', 'tesoro-shop') )
					),
					$bundle_product, $args );
				?>
			</div>
		</div>
</section>
<?php } ?>