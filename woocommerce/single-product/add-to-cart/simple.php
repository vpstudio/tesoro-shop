<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$brand = !empty(wp_get_post_terms( $product->get_id(), 'brand' )) ? wp_get_post_terms( $product->get_id(), 'brand' )[0] : null;
$is_hide_price = !is_null($brand) ? fw_get_db_term_option($brand->term_id, 'brand', 'product_іs_hide_price', false) : false;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok. ?>



	<?php do_action( 'woocommerce_before_add_to_cart_form' );
	if ( $product->is_in_stock() ) : ?>
	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input( array(
			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
		) );

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>

		<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>
	<?php endif; ?>
	<?php if ($is_hide_price && $product->is_in_stock()): ?>
		<div>
		<a href="#get-price-form" title="<?php _e('Узнать цену', 'tesoro-shop'); ?>" rel="noopener noreferrer" class="button alt get-price-button"><?php _e('Узнать цену', 'tesoro-shop'); ?></a>
			<div id="get-price-form" class="get-price-form white-popup-block mfp-hide">
				<form action="" method="post">
					<p>
						<span><?php _e('Пожалуйста, введите Ваш email и мы вышлем Вам стоимость товара.', 'tesoro-shop'); ?></span>
						<input type="email" name="get-price-email" id="get-pirce-email" placeholder="<?php _e('Ваш email', 'tesoro-shop'); ?>" required>
					</p>
					<p><input type="submit" id="get-price-submit" name="get-price-submit" value="<?php _e('Отправить', 'tesoro-shop'); ?>"></p>
					<input type="hidden" id="ts-product-id" name="ts-product-id" value="<?php echo $product->get_id(); ?>">
					<input type="hidden" id="ts-product-type" name="ts-product-type" value="<?php echo $product->get_type(); ?>">
				</form>
			</div>
		</div>
		<style>
			.woocommerce-variation-price,
			.yith-par-message {
				display: none !important;
			}
		</style>
	<?php elseif ($is_hide_price && !$product->is_in_stock())  : ?>
		<div>
		<a href="#get-price-form" title="<?php _e('Узнать цену/Заказать', 'tesoro-shop'); ?>" rel="noopener noreferrer" class="button alt get-price-button"><?php _e('Узнать цену/Заказать', 'tesoro-shop'); ?></a>
			<div id="get-price-form" class="get-price-form white-popup-block mfp-hide">
				<form action="" method="post">
					<p>
						<span><?php _e('Пожалуйста, введите Ваш email и мы вышлем Вам стоимость товара.', 'tesoro-shop'); ?></span>
						<input type="email" name="get-price-email" id="get-pirce-email" placeholder="<?php _e('Ваш email', 'tesoro-shop'); ?>" required>
					</p>
					<p><input type="submit" id="get-price-submit" name="get-price-submit" value="<?php _e('Отправить', 'tesoro-shop'); ?>"></p>
					<input type="hidden" id="ts-product-id" name="ts-product-id" value="<?php echo $product->get_id(); ?>">
					<input type="hidden" id="ts-product-type" name="ts-product-type" value="<?php echo $product->get_type(); ?>">
				</form>
			</div>
		</div>
		<style>
			.woocommerce-variation-price,
			.yith-par-message {
				display: none !important;
			}
		</style>
	<?php elseif (!$product->is_in_stock())  : ?>
		<div>
		<a href="#get-price-form" title="<?php _e('Заказать', 'tesoro-shop'); ?>" rel="noopener noreferrer" class="button alt get-price-button"><?php _e('Заказать', 'tesoro-shop'); ?></a>
			<div id="get-price-form" class="get-price-form white-popup-block mfp-hide">
				<form action="" method="post">
					<p>
						<span><?php _e('Пожалуйста, введите Ваш email и свяжемся с вами.', 'tesoro-shop'); ?></span>
						<input type="email" name="get-price-email" id="get-pirce-email" placeholder="<?php _e('Ваш email', 'tesoro-shop'); ?>" required>
					</p>
					<p><input type="submit" id="get-price-submit" name="get-price-submit" value="<?php _e('Отправить', 'tesoro-shop'); ?>"></p>
					<input type="hidden" id="ts-product-id" name="ts-product-id" value="<?php echo $product->get_id(); ?>">
					<input type="hidden" id="ts-product-type" name="ts-product-type" value="<?php echo $product->get_type(); ?>">
				</form>
			</div>
		</div>
		<style>
			.woocommerce-variation-price,
			.yith-par-message {
				display: none !important;
			}
		</style>
	<?php endif; ?>
	<?php echo do_shortcode( '[ti_wishlists_addtowishlist]' ); ?>
	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
