<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author     WooThemes
 * @package    WooCommerce/Templates
 * @version    3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;
$display_dimensions = apply_filters( 'wc_product_enable_dimensions_display', $product->has_weight() || $product->has_dimensions() );
$attributes = array_filter( $product->get_attributes(), 'wc_attributes_array_filter_visible' );
$count = 10;
$brand = wp_get_post_terms( $product->get_id(), 'brand' );
$instock = $product->is_in_stock() ? 'instock' : '';

the_title( '<h1 class="product_title entry-title" itemprop="name">', '</h1>' );

$html = '<div class="woocommerce-single-product-hover__attributes">';
$html .= '<div class="woocommerce-single-product-hover__attribute '. $instock .'">';
$html .= '<span class="title">'. __('Статус', 'tesoro-shop') .'</span>';
$html .= $product->is_in_stock() ? '<span class="value">'.  __('В наличии','tesoro-shop') .'</span>' : '<span class="value">'.  __('Под заказ','tesoro-shop') .'</span>';
$html .= '</div>';
$count -= 1;
if ($display_dimensions && $product->has_weight()) {
	$html .= '<div class="woocommerce-single-product-hover__attribute" ><span class="title">'. __('Вес', 'tesoro-shop') .'</span><span class="value">'. esc_html( wc_format_weight( $product->get_weight() ) ) .'</span></div>';
	$count -= 1;
}	
if ($display_dimensions && $product->has_dimensions()) {
	$html .= '<div class="woocommerce-single-product-hover__attribute" ><span class="title">'. __('Размер', 'tesoro-shop') .'</span><span class="value">'. esc_html( wc_format_dimensions( $product->get_dimensions( false ) ) ) .'</span></div>';
	$count -= 1;
}
foreach ( $attributes as $attribute ) {
	if ( $count == 0 ) break;
	$values = array();
		if ( $attribute->is_taxonomy() ) {
			$attribute_values = wc_get_product_terms( $product->get_id(), $attribute->get_name(), array( 'fields' => 'names' ) );
			$attribute_values_len = strlen(implode(', ', $attribute_values));
			if ( $attribute_values_len > 40 ) continue;
			$html .= '<div class="woocommerce-single-product-hover__attribute" ><span class="title">'. wc_attribute_label( $attribute->get_name() ) .'</span><span class="value" >';
			foreach ( $attribute_values as $attribute_value ) {
				$values[] = esc_html( $attribute_value );
			}
			$html .= apply_filters( 'woocommerce_attribute', wptexturize( implode( ', ', $values ) ), $attribute, $values ) .'</span></div>';
	} else {
		$attribute_values = $attribute->get_options();
		$attribute_values_len = strlen(implode(', ', $attribute_values));
		if ( $attribute_values_len > 40 ) continue;
		$html .= '<div class="woocommerce-single-product-hover__attribute" ><span class="title">'. wc_attribute_label( $attribute->get_name() ) .'</span><span class="value" >';
		foreach ( $attribute_values as $value ) {
			$values[] = $value;
		}
		$html .= apply_filters( 'woocommerce_attribute', wptexturize( implode( ', ', $values ) ), $attribute, $values ) .'</span></div>';
	}
	$count -= 1;
}
$html .= '<a href="#tab-title-additional_information" title="'. __('Подробнее', 'tesoro-shop') .'" class="woocommerce-single-product-hover__link">'. __('Подробнее', 'tesoro-shop') .'</a>';
$html .= '</div>';

echo $html;