<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$brand = !empty(wp_get_post_terms( $product->get_id(), 'brand' )) ? wp_get_post_terms( $product->get_id(), 'brand' )[0] : null;
$is_hide_price = !is_null($brand) ? fw_get_db_term_option($brand->term_id, 'brand', 'product_іs_hide_price', null) : false;
?>

<?php if ( $price_html = $product->get_price_html() ) : ?>
	<?php if ($is_hide_price) : ?>
	<span class="price"><?php _e( 'Цена по запросу', 'tesoro-shop' ); ?></span>
	<?php else : ?>
	<span class="price"><?php echo $price_html; ?></span>
	<?php endif; ?>
<?php endif; ?>