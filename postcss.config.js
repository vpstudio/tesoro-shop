const autoprefixer = require('autoprefixer');
const cssMqpacker = require('css-mqpacker');
const cssnano = require('cssnano');

module.exports = {
	plugins: [
		autoprefixer({
			grid: true
		}),
		cssMqpacker(),
		cssnano({
			preset: 'default'
		})
	]
};
