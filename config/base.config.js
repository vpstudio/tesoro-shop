const path = require('path');
const webpack = require('webpack');
const env = process.env.NODE_ENV;

let publicPathVar =
  env === 'production' ? '//tesoro-jewelry.com.ua/' : '//tesoro.loc/';

process.argv.forEach(argv => {
  if (argv.indexOf('-d=') !== -1) {
    publicPathVar = argv.replace('-d=', '');
  }
});

module.exports = {
  entry: path.resolve(__dirname, '../src/index.js'),
  output: {
    path: path.resolve(__dirname, '../dist'),
    publicPath: `${publicPathVar}wp-content/themes/tesoro-shop/dist/`,
    filename: 'js/bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(png|jp(e*)g|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /.(ttf|otf|eot|woff(2)?)(\?[a-z0-9]+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'fonts/[name].[ext]'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    })
  ],
  resolve: {
    alias: {
      Assets: path.resolve(__dirname, '../src/assets')
    }
  }
};
