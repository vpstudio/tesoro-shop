const webpack = require('webpack');
const merge = require('webpack-merge');
const baseConfig = require('./base.config.js');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const env = process.env.NODE_ENV;

let publicPathVar =
  env === 'production' ? '//tesoro-jewelry.com.ua/' : '//tesoro.loc/';

process.argv.forEach(argv => {
  if (argv.indexOf('-d=') !== -1) {
    publicPathVar = argv.replace('-d=', '');
  }
});

module.exports = merge(baseConfig, {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader', 'postcss-loader'],
      },
      {
        test: /\.sass|scss$/,
        use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
      },
    ],
  },
  devtool: '#eval-source-map',
  performance: {
    hints: false,
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"development"',
      },
    }),
    new BrowserSyncPlugin({
      proxy: publicPathVar.replace('//', ''),
      files: ['**/*.php'],
      reloadDelay: 0,
      notify: false,
      open: false,
    }),

    new webpack.NamedModulesPlugin(),
  ],
});
