<?php
/**
 * The template for displaying news
 *
 * This is the template that displays all news by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tesoro-shop
 */

$args = array(
    'post_type'   => 'post',
    'post_status' => 'publish',
    'numberposts' => '1',
    'orderby'     => 'date',
    'order'       => 'DESC',
);

$news = get_posts( $args );
$soc_instagram_link =  !empty(fw_get_db_settings_option('soc_instagram')) ? basename(fw_get_db_settings_option('soc_instagram')) : null;
get_header();
?>

<div class="container">
	<div class="row">
		<div class="col"><?php woocommerce_breadcrumb(); ?></div>
	</div>
	<div class="row">
		<div class="col d-flex justify-content-center">
			<h1 class="section__title"><?php wp_title("");?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col col-xl-9">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
					
					<div class="row">
						<div class="col">
							<?php foreach ( $news as $new_item ) { 
								setup_postdata($new_item);
								$categories = get_the_category($new_item->ID);
							?>
								<article id="post-<?php the_ID(); ?>" class="post recent-post" style="background-image: url(<?php the_post_thumbnail_url( 'full' ); ?>);">
									<div class="post-wrapper">
										<div class="post__header">
											<div class="entry-date entry-date--desktop">
												<div class="date">
													<div class="mounth">
														<span><?php echo get_the_date('M', $post->ID); ?></span>
													</div>
													<div class="day">
														<time datetime="<?php echo $post->post_date; ?>"><?php echo get_the_date('d', $post->ID); ?></time>
													</div>
												</div>
											</div>
											<div class="post__heading">
												<a href="<?php echo get_permalink($new_item->ID); ?>" title="<?php _e('Подробнее', 'tesoro-shop'); ?>" ><h3 class="post__title"><?php echo $new_item->post_title; ?></h3></a>
												<div class="post__data">
													<time datetime="<?php echo $new_item->post_date; ?>" class="post__date post__date--mobile"><?php echo get_the_date('d M Y', $new_item->ID); ?></time>
													<?php if (!empty($categories)) {
														foreach ($categories as $category ) { ?>
														<a class="post__cat" href="<?php echo get_category_link( $category->term_id ); ?>" ><?php echo $category->name; ?></a>
														<?php } 
													}?>
												</div>
											</div>
										</div>
										<div class="post__excerpt">
											<?php echo wp_trim_words( $new_item->post_content, 25, '...' ); ?>
										</div>
									</div>
								</article >
							<?php } 
								wp_reset_postdata();
							?>
						</div>
					</div>
					<div id="posts-loop" class="row">

						<?php while ( have_posts() ) : the_post(); 
						$categories = get_the_category($post->ID); ?>
						<div class="col-12 col-lg-6 col-xl-4">
							<article id="post-<?php the_ID(); ?>" class="post">
								<a href="<?php echo get_permalink($post->ID); ?>" title="<?php _e('Подробнее', 'tesoro-shop'); ?>" class="post__image-wrapper">
									<div class="post__image" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>);"></div>
								</a>
								<a href="<?php echo get_permalink($post->ID); ?>" title="<?php _e('Подробнее', 'tesoro-shop'); ?>" ><h3 class="post__title"><?php echo $post->post_title; ?></h3></a>
								<div class="post__data">
									<time datetime="<?php echo $post->post_date; ?>" class="post__date"><?php echo get_the_date('d M Y', $post->ID); ?></time>
									<?php if (!empty($categories)) { echo "|";
									foreach ($categories as $category ) { ?>
										<a class="post__cat" href="<?php echo get_category_link( $category->term_id ); ?>" ><?php echo $category->name; ?></a>
									<?php } 
									}?>
								</div>
								<div class="post__excerpt">
									<?php echo wp_trim_words( get_the_content(), 25, '...' ); ?>
								</div>
							</article >
						</div>
						<?php endwhile; // End of the loop. ?>
					</div>
					<div class="row">
						<div class="col d-flex justify-content-center">
						<?php if (  $wp_query->max_num_pages > 1 ) : ?>
							<script>
								var ts_posts = '<?php echo serialize($wp_query->query_vars); ?>';
								var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
								var max_pages = '<?php echo $wp_query->max_num_pages; ?>';
							</script>
							<a href="javascript:void(0)" rel="noopener noreferrer" id="loadmore" class="button alt"><?php _e('Показать больше', 'tesoro-shop'); ?></a>
						<?php endif; ?>
						</div>
					</div>
				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
		<div class="col-3 d-none d-xl-block"><?php get_sidebar(); ?></div>
	</div>
</div>

<?php
	if (!empty($soc_instagram_link)) { ?>
		<!-- instagram -->
		<section class="section section--instagram">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2 class="section__title">Instagram</h2> <a href="https://www.instagram.com/<?php echo $soc_instagram_link; ?>" target="_blank" class="instagram-username">@<?php echo $soc_instagram_link; ?></a>
						<span class="section__subtitle"><?php _e('Подписывайся на наш канал в Instagram!', 'tesoro-shop'); ?></span>
					</div>
				</div>
			</div> 
			<?php echo do_shortcode('[iscwp-slider slidestoshow="5" show_comments_count="false" show_likes_count="false" popup="false" dots="false" autoplay="false" username="'. $soc_instagram_link .'"]'); ?>
		</section>
		<!-- #instagram -->
	<?php } 

get_footer();
