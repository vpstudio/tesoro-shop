<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package tesoro-shop
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function ts_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'filterbar' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'ts_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function ts_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'ts_pingback_header' );

/**
 * Return percent of sale
 * 
 * @param array $product single woocommerce product
 * @return string $html
 */
function ts_get_sale( $product = null) {

	if ( is_null($product) ) return;

    $html = '';

	if( $product->is_type( 'variable' ) && $product->is_on_sale() ) { 
		$variation_prices = $product->get_variation_prices();
		$max_sale_id = array_keys($variation_prices['sale_price'], min($variation_prices['sale_price']))[0];
		$sale = $variation_prices['sale_price'][$max_sale_id];
		$regular_price = $variation_prices['regular_price'][$max_sale_id];
		$sale_price = floatval( $regular_price ) - floatval( $sale );
		$sale_rate = round( $sale_price / floatval( $regular_price ) * 100);
		$html .= '<div class="product-item__sale-rate"> -'. $sale_rate .'%</div>';
		return $html;
	}
	elseif ( $product->is_type( 'grouped' ) && $product->is_on_sale() ) {
		$child_prices = array();
		foreach ( $product->get_children() as $child_id ) {
			$child_prices[$child_id] = get_post_meta( $child_id, '_regular_price', true );
		}
		$child_sale_prices = array();
		foreach ( $product->get_children() as $child_id ) {
			if ( empty(get_post_meta( $child_id, '_sale_price', true )) ) continue;
			$child_sale_prices[$child_id] = get_post_meta( $child_id, '_sale_price', true );
		}
		$max_sale_id = array_keys($child_sale_prices, min($child_sale_prices))[0];
		$sale = $child_sale_prices[$max_sale_id];
		$regular_price = $child_prices[$max_sale_id];

		$sale_price = floatval( $regular_price ) - floatval( $sale );
		$sale_rate = round( $sale_price / floatval( $regular_price ) * 100);
		$html .= '<div class="product-item__sale-rate"> -'. $sale_rate .'%</div>';
		return $html;
	}
    elseif( $product->is_type( 'simple' ) && $product->is_on_sale() ) {
		$sale_price = floatval( $product->get_regular_price() ) - floatval( $product->get_sale_price() );
		$sale_rate = round( $sale_price / floatval( $product->get_regular_price() ) * 100);
		$html .= '<div class="product-item__sale-rate"> -'. $sale_rate .'%</div>';
		return $html;

	} elseif( $product->is_type( 'yith_bundle' ) && $product->is_on_sale() ) {
		$sale_price = floatval( $product->get_regular_price() ) - floatval( $product->get_sale_price() );
		$sale_rate = round( $sale_price / floatval( $product->get_regular_price() ) * 100);
		$html .= '<div class="product-item__sale-rate"> -'. $sale_rate .'%</div>';
		return $html;
		
	} else {
		return '';
	}
}


/**
 * Return attributes of product
 * 
 * @param array $product single woocommerce product
 * @param array $attribute
 * @param bool $display_dimensions
 * @param int $count
 * @return string $html
 */
function ts_get_attributes( $product = null, $display_dimensions = false, $attributes = null, $count) {
	$html = '<div class="woocommerce-loop-product-hover__attributes">';

	if ($display_dimensions && $product->has_weight()) {
		$html .= '<div class="woocommerce-loop-product-hover__attribute" ><span class="title">'. __('Вес', 'tesoro-shop') .'</span><span class="value">'. esc_html( wc_format_weight( $product->get_weight() ) ) .'</span></div>';
		$count -= 1;
	}	
	if ($display_dimensions && $product->has_dimensions()) {
	$html .= '<div class="woocommerce-loop-product-hover__attribute" ><span class="title">'. __('Размер', 'tesoro-shop') .'</span><span class="value">'. esc_html( wc_format_dimensions( $product->get_dimensions( false ) ) ) .'</span></div>';
		$count -= 1;
	}
	foreach ( $attributes as $attribute ) {
		if ( $count == 0 ) break;
		if ( count( $attribute->get_options() ) > 1 ) continue;

		$values = array();
		if ( $attribute->is_taxonomy() ) {
			$attribute_taxonomy = $attribute->get_taxonomy_object();
			$attribute_values = wc_get_product_terms( $product->get_id(), $attribute->get_name(), array( 'fields' => 'all' ) );
			foreach ( $attribute_values as $attribute_value ) {
				$value_name = esc_html( $attribute_value->name );
				if ( strlen($value_name) > 20 ) continue;
				$html .= '<div class="woocommerce-loop-product-hover__attribute" ><span class="title">'. wc_attribute_label( $attribute->get_name() ) .'</span><span class="value" >';
				if ( $attribute_taxonomy->attribute_public ) {
					$values[] = $value_name;
				} else {
					$values[] = $value_name;
				}
				$html .= apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values ) .'</span></div>';
			}
		} else {
			foreach ( $attribute->get_options() as $value ) {
				if (strlen($value) > 20) continue;
				$html .= '<div class="woocommerce-loop-product-hover__attribute" ><span class="title">'. wc_attribute_label( $attribute->get_name() ) .'</span><span class="value" >';
				$values[] = $value;
				$html .= apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values ) .'</span></div>';
			}
		}
		$count -= 1;
	}
	$html .= '</div>';

	return $html;
}


/**
 * Return html product
 * 
 * @param array $products list of woocommerce products
 * @param string $tyep list type
 * @return string $html
 */
function ts_build_products( array $products = null, string $type = 'new' ) {
    $html = '';
    foreach ( $products as $product ) {
        if ( $type == 'sale' ) {
            if ( !$product->is_on_sale() ) continue;
		}
		if ( $type == 'bundle' ) {
            $product = $product->product;
        }
        $image = !empty( wp_get_attachment_image_src( $product->get_image_id(), 'woocommerce_thumbnail' ) ) ? wp_get_attachment_image_src( $product->get_image_id(), 'woocommerce_thumbnail' )[0] : '';
        $alt = get_post_meta( $product->get_image_id(), '_wp_attachment_image_alt', true );
		$title = get_the_title($product->get_image_id());
		$brand = !empty(wp_get_post_terms( $product->get_id(), 'brand' )) ? wp_get_post_terms( $product->get_id(), 'brand' )[0] : null;
		$is_hide_price = !is_null($brand) ? fw_get_db_term_option($brand->term_id, 'brand', 'product_іs_hide_price', false) : false;

        $html .= '<div class="product-item">';
        $html .= ts_get_sale($product);
        $html .= '<a href="'. get_permalink( $product->get_id() ) .'" class="product-item__image">';
        $html .= '<img src="'. $image .'" class="img-responsive" title="'. $title .'" alt="'. $alt .'">';
        $html .= '</a>';
        $html .= '<div class="product-item__content">';
		$html .= '<a href="'. get_permalink( $product->get_id() ) .'" class="product-item__title">'. $product->get_name() .'</a>';
        $html .= '<div class="product-item__price">';
        if ( $product->get_stock_status() == 'outofstock' ) {
			$html .= '<span class="semi outofstock">'. __( 'Под заказ', 'tesoro-shop' ) .'</span> ';
        } elseif ( $is_hide_price ) {
            $html .= '<span class="semi">'. __( 'Цена по запросу', 'tesoro-shop' ) .'</span> ';
        } elseif ( $product->is_on_sale() && $product->get_type() == 'simple' ) {
            $html .= '<span class="semi old-price">'. $product->get_regular_price() .'</span> ';
            $html .= wc_price( $product->get_price() );
        } else {
            $html .= '<span class="semi">'. __( 'В наличии', 'tesoro-shop' ) .'</span> ';
            $html .= wc_price( $product->get_price() );
        }
        $html .= '</div></div></div>';
    }
    return $html;
}


function share_buttons($porduct_id) {
	$content = '';

	if ( is_singular() && is_woocommerce() ) {
		$product_url = urlencode(get_permalink($porduct_id));
		$product_title = htmlspecialchars(urlencode(html_entity_decode(get_the_title($porduct_id), ENT_COMPAT, 'UTF-8')));
		$porduct_image = wp_get_attachment_image_src( get_post_thumbnail_id( $porduct_id ), 'full' );

		$twitterURL = 'https://twitter.com/intent/tweet?text='.$product_title.'&amp;url='.$product_url.'&amp;via=Crunchify';
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$product_url;
		$googleURL = 'https://plus.google.com/share?url='.$product_url;
		$bufferURL = 'https://bufferapp.com/add?url='.$product_url.'&amp;text='.$product_title;
		$whatsappURL = 'whatsapp://send?text='.$product_title . ' ' . $product_url;
		$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$product_url.'&amp;title='.$product_title;

		$content .= '<div class="crunchify-social">';
		$content .= '<h5>SHARE ON</h5> <a class="crunchify-link crunchify-twitter" href="'. $twitterURL .'" target="_blank">Twitter</a>';
		$content .= '<a class="crunchify-link crunchify-facebook" href="'.$facebookURL.'" target="_blank">Facebook</a>';
		$content .= '<a class="crunchify-link crunchify-googleplus" href="'.$googleURL.'" target="_blank">Google+</a>';
		$content .= '</div>';
		return $content;
	} else {
		return $content;
	}
}


function schema_org_markup() {
    $schema = 'http://schema.org/';
    // Is single post
    if ( is_woocommerce() ) {
    	$type = 'Product';
    }
    elseif ( is_single() ) {
        $type = "Article";
    } 
    else {
        if ( is_page('contacts') ) { // Contact form page ID
            $type = 'ContactPage';
        } // Is author page
        elseif ( is_author() ) {
            $type = 'ProfilePage';
        } // Is search results page
        elseif ( is_search() ) {
            $type = 'SearchResultsPage';
        } // Is of movie post type
        else {
            $type = 'WebPage';
        }
    }
    echo 'itemscope="itemscope" itemtype="' . $schema . $type . '"';
}

function get_wc_product_by_title( $title ){
    global $wpdb;

    $post_title = strval($title);

    $post_table = $wpdb->prefix . "posts";
    $result = $wpdb->get_col("
        SELECT ID
        FROM $post_table
        WHERE post_title LIKE '$post_title'
        AND post_type LIKE 'product'
    ");

    // We exit if title doesn't match
    if( empty( $result[0] ) ) 
        return;
    else
        return wc_get_product( intval( $result[0] ) );
}

function get_wc_products_ids_by_titles( $titles ){
	global $wpdb;
	$safe_titles = array_filter($titles, function($title) { return $title !== ''; });

	$safe_titles = implode(",", array_map(function($title) {
		return "'" . strval($title) . "'";
	}, $titles));
	$post_table = $wpdb->prefix . "posts";

	$result = $wpdb->get_col("
	SELECT
	ID
	FROM
	$post_table
	WHERE
	post_name IN($safe_titles) AND post_type IN('product')
	");
	
	return $result;
}

function ts_remove_empty($arr) {
	return array_filter($arr, function($item) { return $item !== ''; });
}