<?php

function tesoro_shop_load_posts(){
	$args = unserialize( stripslashes( $_POST['query'] ) );
	$args['paged'] = $_POST['page'] + 1;
	$args['post_status'] = 'publish';
	

	query_posts( $args );

	if( have_posts() ) :

		while( have_posts() ): the_post(); ?>
			
			<div class="col-12 col-lg-6 col-xl-4">
				<article id="post-<?php the_ID(); ?>" class="post">
					<a href="<?php echo get_permalink($post->ID); ?>" class="post__image-wrapper">
						<div class="post__image" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'medium'); ?>);"></div>
					</a>
					<a href="<?php echo get_permalink($post->ID); ?>"><h3 class="post__title"><?php the_title(); ?></h3></a>
					<div class="post__data">
						<time date="<?php echo $post->post_date; ?>" class="post__date"><?php echo get_the_date('d M Y', $post->ID); ?></time>
						<?php if (!empty($categories)) { echo "|";
						foreach ($categories as $category ) { ?>
							<a class="post__cat" href="<?php echo get_category_link( $category->term_id ); ?>" ><?php echo $category->name; ?></a>
						<?php } 
						}?>
					</div>
					<div class="post__excerpt">
						<?php echo wp_trim_words( get_the_content(), 25, '...' ); ?>
					</div>
				</article >
			</div>
		
	<?php endwhile;
	endif;
	die();
}
add_action('wp_ajax_loadmore', 'tesoro_shop_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'tesoro_shop_load_posts');