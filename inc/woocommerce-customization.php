<?php

/**
 * WooCommerce Customization
 */

add_filter('add_to_cart_fragments', 'header_add_to_cart_fragment');
function header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;
    ob_start();
    ?>
    <?php echo '<span class="card-count">'. sprintf($woocommerce->cart->cart_contents_count) .'</span>'; ?>
    <?php
		$fragments['.card-count'] = ob_get_clean();
    return $fragments;
}

add_filter( 'woocommerce_get_availability', 'wcs_custom_get_availability', 1, 2);
function wcs_custom_get_availability( $availability, $_product ) {
    
    if ( ! $_product->is_in_stock() ) {
        $availability['availability'] = __('Под заказ', 'woocommerce');
    }
    return $availability;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'ts_get_catalog_ordering_args' );
function ts_get_catalog_ordering_args( $args ) {
	$orderby_value = isset( $_GET['orderby'] ) ? woocommerce_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
	 
	if ( 'on_sale' == $orderby_value ) {
		$args['orderby'] = 'meta_value_num';
		$args['order'] = 'DESC';
		$args['meta_key'] = '_sale_price'; 
	} else if ( 'random' == $orderby_value ) {
		$args['orderby'] = 'menu_order rand';
		$args['order'] = 'ASC';
		$args['meta_key'] = ''; 
	}
	
	return $args;
}
function custom_woocommerce_catalog_orderby( $orderby ) {
	$orderby['date'] = __('Новинки','tesoro-shop');
	$orderby['price'] = __('Возрастанию цены','tesoro-shop');
	$orderby['price-desc'] = __('Убыванию цены','tesoro-shop');
	$orderby['on_sale'] = __('Скидка', 'tesoro-shop');
	$orderby['random'] = __('Случайно', 'tesoro-shop');
	return $orderby;
	}
add_filter( "woocommerce_default_catalog_orderby_options", "custom_woocommerce_catalog_orderby" );
add_filter( "woocommerce_catalog_orderby", "custom_woocommerce_catalog_orderby", 20 );


/**
 * Change a currency symbol
 */
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);
function change_existing_currency_symbol( $currency_symbol, $currency ) {
    switch( $currency ) {
		case 'UAH': $currency_symbol = ' грн'; break;
    }
    return $currency_symbol;
}

function custom_my_account_menu_items( $items ) {
    unset($items['downloads']);
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'custom_my_account_menu_items' );

add_filter('woocommerce_show_page_title', '__return_false');
add_filter( 'wc_product_sku_enabled', '__return_false' );

add_action( 'woocommerce_shop_loop_item_title', function (){
	global $product;
	$brand = !empty(wp_get_post_terms( $product->get_id(), 'brand' )) ? wp_get_post_terms( $product->get_id(), 'brand' )[0] : null;
	$is_hide_price = !is_null($brand) ? fw_get_db_term_option($brand->term_id, 'brand', 'product_іs_hide_price', null) : false;
	
	$html = '';
	$html .= '<div class="woocommerce-loop-product__button-wrapper mobile" >';
	$html .= do_shortcode("[ti_wishlists_addtowishlist loop=yes]");
	if ($is_hide_price) {
		$html .= '<a href="'. get_permalink( $product->get_id() ) .'" title="'. __('Подробнее', 'tesoro-shop') .'" class="%s product_type_%s button"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="19" fill="none"><path fill="#757575" d="M8.82 7.66h1.68V5.13H13V3.45H10.5V.92H8.82v2.53h-2.5v1.68h2.5v2.53zm-3.35 7.6c-.92 0-1.66.76-1.66 1.68 0 .93.74 1.7 1.66 1.7a1.69 1.69 0 0 0 0-3.38zm8.38 0c-.92 0-1.67.76-1.67 1.68 0 .93.75 1.7 1.67 1.7a1.69 1.69 0 0 0 0-3.38zm-8.23-2.74l.02-.1.76-1.38h6.23c.63 0 1.19-.35 1.47-.87l3.23-5.91-1.45-.81h-.01l-.92 1.68-2.32 4.22H6.76l-.11-.23-1.88-3.99-.8-1.68-.78-1.7H.45v1.7h1.67l3.02 6.4L4 11.92a1.69 1.69 0 0 0 1.47 2.5h10.04v-1.7h-9.7c-.1 0-.2-.09-.2-.2z"/></svg></a>';
	} else {
	$html .= apply_filters( 'woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.
	sprintf( '<a href="%s" data-quantity="1" data-product_id="%s" data-product_sku="%s" class="%s product_type_%s button"><svg xmlns="http://www.w3.org/2000/svg" width="18" height="19" fill="none"><path fill="#757575" d="M8.82 7.66h1.68V5.13H13V3.45H10.5V.92H8.82v2.53h-2.5v1.68h2.5v2.53zm-3.35 7.6c-.92 0-1.66.76-1.66 1.68 0 .93.74 1.7 1.66 1.7a1.69 1.69 0 0 0 0-3.38zm8.38 0c-.92 0-1.67.76-1.67 1.68 0 .93.75 1.7 1.67 1.7a1.69 1.69 0 0 0 0-3.38zm-8.23-2.74l.02-.1.76-1.38h6.23c.63 0 1.19-.35 1.47-.87l3.23-5.91-1.45-.81h-.01l-.92 1.68-2.32 4.22H6.76l-.11-.23-1.88-3.99-.8-1.68-.78-1.7H.45v1.7h1.67l3.02 6.4L4 11.92a1.69 1.69 0 0 0 1.47 2.5h10.04v-1.7h-9.7c-.1 0-.2-.09-.2-.2z"/></svg></a>',
		esc_url( $product->add_to_cart_url() ),
		esc_attr( $product->get_id() ),
		esc_attr( $product->get_sku() ),
		esc_attr( $product->get_type() == 'simple' ? 'add_to_cart_button ajax_add_to_cart' : '' ),
		esc_attr( $product->get_type() )
	),
	$product );
	}
	$html .= '</div>';
	echo $html;
});
add_action( 'woocommerce_before_shop_loop_item_title', function (){
	global $product;
	echo ts_get_sale($product);
},  10, 0);

add_action( 'woocommerce_after_shop_loop_item', function () {
	global $product;
	$display_dimensions = apply_filters( 'wc_product_enable_dimensions_display', $product->has_weight() || $product->has_dimensions() );
	$attributes = array_filter( $product->get_attributes(), 'wc_attributes_array_filter_visible' );

	$is_new = fw_get_db_post_option($product->get_id(), 'is_new');
	$is_popular = fw_get_db_post_option($product->get_id(), 'is_popular');
	$brand = !empty(wp_get_post_terms( $product->get_id(), 'brand' )) ? wp_get_post_terms( $product->get_id(), 'brand' )[0] : null;
	$is_hide_price = !is_null($brand) ? fw_get_db_term_option($brand->term_id, 'brand', 'product_іs_hide_price', null) : false;
	$html  = '<div class="woocommerce-loop-product-hover">';
	$html .= ts_get_sale($product);
	$html .= '<a href="'. get_permalink( $product->get_id() ) .'" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">';
	$html .= '<h2 class="woocommerce-loop-product-hover__title">'. $product->get_name() .'</h2>';
	$html .= '</a>';
	
	if ( $product->is_on_sale() || $is_new || $is_popular ) {
	$html .= '<div class="woocommerce-loop-product-hover__badges-wrapper">';
	if ( $product->is_on_sale() ) $html .= '<a href="'. home_url('shop?onsale_filter=1') .'" class="badge badge--sale" >'. __('Скидка', 'tesoro-shop') .'</a>';
	if ( $is_new ) $html .= '<a href="'. home_url('shop/?orderby=date') .'" class="badge badge--new" >'. __('Новинка', 'tesoro-shop') .'</a>';
	// if ( isset($is_sale) ) $html .= '<a href="#" class="badge badge--promotion" >'. __('Акция', 'tesoro-shop') .'</a>';
	if ( $is_popular ) $html .= '<a href="'. home_url('shop/?orderby=popularity') .'" class="badge badge--popular" >'. __('Хит', 'tesoro-shop') .'</a>';
	$html .= '</div>';
	}
	
	array_shift($attributes);
	$html .= ts_get_attributes($product, $display_dimensions, $attributes, 3);

	if ( $price_html = $product->get_price_html() ) {
		if ($is_hide_price) {
			$html .= '<span class="woocommerce-loop-product-hover__price">'. __( 'Цена по запросу', 'tesoro-shop' ) .'</span>';
		} else {
			$html .= '<span class="woocommerce-loop-product-hover__price">'. $price_html .'</span>';
		}
	}
	$html .= $product->get_stock_status() == 'instock' ? '<span class="woocommerce-loop-product-hover__stock-status instock">'.  __('В наличии','tesoro-shop') .'</span>' : '<span class="stock_status">'.  __('Под заказ','tesoro-shop') .'</span>';
	
	$html .= '<div class="woocommerce-loop-product-hover__button-wrapper" >';
	$html .= do_shortcode("[ti_wishlists_addtowishlist loop=yes]");
	if ( $is_hide_price  ) {
		$html .= '<a href="'. get_permalink( $product->get_id() ) .'" title="'. __('Подробнее', 'tesoro-shop') .'" class="button product_type_'. $product->get_type() .'">'. __('Подробнее', 'tesoro-shop') .'</a>';
	} else {
		$html .= apply_filters( 'woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.
		sprintf( '<a href="%s" data-quantity="1" data-product_id="%s" data-product_sku="%s" class="%s button product_type_%s">%s</a>',
			esc_url( $product->add_to_cart_url() ),
			esc_attr( $product->get_id() ),
			esc_attr( $product->get_sku() ),
			esc_attr( $product->get_type() == 'simple' ? 'add_to_cart_button ajax_add_to_cart' : '' ),
			esc_attr( $product->get_type() ),
			esc_html( $product->add_to_cart_text() )
		),
		$product );
	}
	$html .= '<a class="woocommerce-loop-product-hover__view" title="'. __('Просмотр', 'tesoro-shop') .'" href="'. get_permalink( $product->get_id() ) .'"><i class="fa fa-list"></i></a>';
	$html .= '</div>';

	$html .= '</div>';
	
	// end block 


	echo $html;
});

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
// Remove the product rating display on product loops
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
function ts_display_zero_rating_html( $rating_html, $rating ) {
    if ( $rating_html == '' ) {
        $rating_html  = '<div class="star-rating">';
        $rating_html .= wc_get_star_rating_html( $rating, 0 );
        $rating_html .= '</div>';
    }
    
    return $rating_html;
}
add_filter( 'woocommerce_product_get_rating_html', 'ts_display_zero_rating_html' , 10, 2 );

/**
 * Customize single product
 */
add_filter('comment_form_fields', 'ts_reorder_comment_fields' );
function ts_reorder_comment_fields( $fields ){
	$new_fields = array();
	$order = array('author','email','comment');
	foreach( $order as $key ){
		$new_fields[ $key ] = $fields[ $key ];
		unset( $fields[ $key ] );
	}
	if( $fields )
		foreach( $fields as $key => $val )
			$new_fields[ $key ] = $val;
	return $new_fields;
}
add_action('wp_ajax_cloadmore', 'comments_loadmore_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_cloadmore', 'comments_loadmore_handler'); // wp_ajax_nopriv_{action}

function comments_loadmore_handler(){

	// maybe it isn't the best way to declare global $post variable, but it is simple and works perfectly!
	global $post;
	$post = get_post( $_POST['post_id'] );
	setup_postdata( $post );

	// actually we must copy the params from wp_list_comments() used in our theme
	wp_list_comments( array(
		'page' => $_POST['cpage'], // current comment page
		'per_page' => get_option('comments_per_page'),
		'callback' => 'woocommerce_comments'
	) );

	wp_die(); // don't forget this thing if you don't want "0" to be displayed
}


add_filter('comment_form_default_fields', function ($fields) { 
    unset($fields['url']);
    return $fields;
});

/* Remove product meta */
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );


function ts_min_password_strength( $strength ) {
    return 1;
}
 
add_filter( 'woocommerce_min_password_strength', 'ts_min_password_strength', 10, 1 );

/**
 * Hide the term description in the edit form
 */
add_action( 'brand_add_form', function( $taxonomy )
{
    ?><style>.term-description-wrap{display:none;}</style><?php
}, 10, 2 );
add_action( 'brand_edit_form', function( $taxonomy )
{
    ?><style>.term-description-wrap{display:none;}</style><?php
}, 10, 2 );

add_action( 'wp_footer', 'cart_update_qty_script' );
function cart_update_qty_script() {
    if (is_cart()) :
        ?>
        <script type="text/javascript">
            (function($){
                $(function(){
                    $('div.woocommerce').on( 'change', '.qty', function(){
                        $("[name='update_cart']").trigger('click');
                    });
                });
            })(jQuery);
        </script>
        <?php
    endif;
}

// Billing and shipping addresses fields
add_filter( 'woocommerce_default_address_fields' , 'filter_default_address_fields', 20, 1 );
function filter_default_address_fields( $address_fields ) {

	unset($address_fields['postcode']);
	unset($address_fields['country']);
	unset($address_fields['address_2']);
	unset($address_fields['billing_address_2']);
    // Only on checkout page
    if( is_checkout() ) {
		// All field keys in this array
		$key_fields = array('city','state','address_1');
	} else {	
		// All field keys in this array
		$key_fields = array('first_name','last_name','address_1','city','state');

		// Loop through each address fields (billing and shipping)
		foreach( $key_fields as $key_field )
        	$address_fields[$key_field]['required'] = false;
	}
		
    

    return $address_fields;
}

add_filter( 'woocommerce_billing_fields', 'filter_billing_fields', 20, 1 );
function filter_billing_fields( $billing_fields ) {
    // Only on checkout page
    if( is_checkout() ) {
		return $billing_fields;
	} 

    $billing_fields['billing_phone']['required'] = false;
    $billing_fields['billing_email']['required'] = false;
    return $billing_fields;
}


add_filter( 'woocommerce_form_field' , 'remove_checkout_optional_fields_label', 10, 4 );
function remove_checkout_optional_fields_label( $field, $key, $args, $value ) {
    // Only on checkout page
    if( is_checkout() && ! is_wc_endpoint_url() ) {
        $optional = '&nbsp;<span class="optional">(' . esc_html__( 'optional', 'woocommerce' ) . ')</span>';
        $field = str_replace( $optional, '', $field );
    }
    return $field;
}

function premove_billing_checkout_fields($fields) {
    global $woocommerce;
    $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
	$chosen_shipping = $chosen_methods[0];
	
	$fields['billing']['billing_city']['priority'] = 50;
	$fields['billing']['billing_state']['priority'] = 70;
	$fields['billing']['billing_address_1']['priority'] = 80;

    if( $chosen_shipping === 'local_pickup:4' ) {
		$fields['billing']['billing_address_1']['required'] = false;
		$fields['billing']['billing_city']['required'] = false;
		$fields['billing']['billing_state']['required'] = false;
    }

    return $fields;
}
add_filter('woocommerce_checkout_fields', 
'premove_billing_checkout_fields', 990 );

// Conditional Show hide checkout fields based on chosen shipping methods
add_action( 'wp_footer', 'custom_checkout_field_script' );
function custom_checkout_field_script() {

    // HERE your shipping methods rate IDs
    $shipping = 'free_shipping:5';
    // $pickpoint = 'wc_custom_shipping_pickpoint';

    $required_text = esc_attr__( 'required', 'woocommerce' );
    $required_html = '<abbr class="required" title="' . $required_text . '">*</abbr>';
    ?>
    <script>
        jQuery(function($){
			
			var ism = 'input[name^="shipping_method"]',
				ismc = ism+':checked',
				rq = '-required',
				vr = 'validate'+rq,
				w = 'woocommerce',
				wv = w+'-validated',
				iv = '-invalid',
				fi = '-field',
				wir = w+iv+' '+w+iv+rq+fi,
				b = '#billing_',
				s = '#shipping_',
				f = '_field',
				a1 = 'country',
				a2 = 'address_1',
				a3 = 'city',
				a5 = 'state',
				billingAddressField = b+a2+f,
				billingCityField = b+a3+f,
				billingStateField = b+a5+f,

				shipping = '<?php echo $shipping; ?>';

            // Utility function to shows or hide checkout fields
            function showHide( action='show', selector='' ){
                if( action == 'show' )
                    $(selector).show(function(){
                        $(this).addClass(vr);
                        $(this).removeClass(wv);
                        $(this).removeClass(wir);
                        if( $(selector+' > label > abbr').html() == undefined )
                            $(selector+' label').append('<?php echo $required_html; ?>');
                    });
                else
                    $(selector).hide(function(){
                        $(this).removeClass(vr);
                        $(this).removeClass(wv);
                        $(this).removeClass(wir);
                        if( $(selector+' > label > abbr').html() != undefined )
                            $(selector+' label > .required').remove();
                    });
            }

            // Initializing at start after checkout init (Based on the chosen shipping method)
            setTimeout(function(){
                if( $(ismc).val() != shipping ) // Choosen "Local pickup" (Hidding "Take away")
                {
                    showHide('hide',billingAddressField);
                    showHide('hide',billingCityField);
                    showHide('hide',billingStateField);
                }
                else
                {
                    showHide('show',billingAddressField);
                    showHide('show',billingCityField);
                    showHide('show',billingStateField);
                }
            }, 1000);

            // When shipping method is changed (Live event)
            $( 'form.checkout' ).on( 'change', ism, function() {
                setTimeout(function(){
					if( $(ismc).val() != shipping ) // Choosen "Local pickup" (Hidding "Take away")
					{
						showHide('hide',billingAddressField);
						showHide('hide',billingCityField);
						showHide('hide',billingStateField);
					}
					else
					{
						showHide('show',billingAddressField);
						showHide('show',billingCityField);
						showHide('show',billingStateField);
					}
				}, 1000);
            });

        });
    </script>
    <?php
}

// define the woocommerce_breadcrumb_defaults callback 
function filter_woocommerce_breadcrumb_defaults( $array ) { 
	$array['delimiter'] = '<span class="separator"><i class="fa fa-angle-right"></i></span>';
	$array['wrap_before'] = '<nav class="breadcrumbs">';
    return $array; 
};  
add_filter( 'woocommerce_breadcrumb_defaults', 'filter_woocommerce_breadcrumb_defaults', 10, 1 );


add_action('wp_ajax_get_price', 'get_price_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_get_price', 'get_price_handler'); // wp_ajax_nopriv_{action}

function get_price_handler(){
	global $woocommerce;

	$output = '';
	$product_type = $_POST['product_type'];
	$product_id = $_POST['product_id'];
	$email = $_POST['email'];

	if ( is_email($email) ) {
		if ($product_type == 'variable') {
			$product = new WC_Product_Variation($product_id);

		} else {
			$product = new WC_Product($product_id);
		}
		$product_name = $product->get_name();

		if (empty($product_name) && $product_type == 'variable') {
			$output = __('Выберите опции товара.', 'tesoro-shop');
		} elseif (empty($product_name)) {
			$output = __('Ой! Произошла ошибка, напишите нам и мы ее исправим', 'tesoro-shop');
		} else {

			$project_name = get_option('blogname');
			$admin_email = get_option('admin_email');
			if ($product_type == 'simple' && !$product->is_in_stock()) {
				$subject = sprintf( __('Заказ товара %s в магазине %s', 'tesoro-shop'),
				esc_attr( $product->get_name() ),
				esc_attr( get_option('blogname') )
				);
				$message = sprintf( __('Клиент %s заказл товар <a href="%s">%s</a>. </br> <a href="%s">Вернуться на страницу товара для его оформления</a>', 'tesoro-shop'),
				esc_attr( $email ),
				esc_url( get_permalink($product->get_ID()) ),
				esc_attr( $product->get_name() ),
				esc_url( get_permalink($product->get_ID()))
				);
				$headers = array(
					'From: '. $project_name .' <'. $admin_email .'>',
					'Content-type: text/html',
					'Reply-to: '.$email
				);
				$result = wp_mail($admin_email, $subject, $message, $headers);
				if ($result) {
					$output = sprintf( __('Ваша заявка успешно отправлена.', 'tesoro-shop'), $product_name, $email);
				} else {
					$output = __('Ой! Произошла ошибка отправки, напишите нам и мы ее исправим', 'tesoro-shop');
				}
			} else {

				$subject = sprintf( __('Цена товара %s в магазине %s', 'tesoro-shop'),
				esc_attr( $product->get_name() ),
				esc_attr( get_option('blogname') )
				);
				$message = sprintf( __('Стоимость <a href="%s">%s</a> составляет %s грн. </br> <a href="%s">Вернуться на страницу товара для его оформления</a>', 'tesoro-shop'),
				esc_url( get_permalink($product->get_ID()) ),
				esc_attr( $product->get_name() ),
				esc_attr( $product->get_price()  ),
				esc_url( get_permalink($product->get_ID()))
				);
				$headers = array(
					'From: '. $project_name .' <'. $admin_email .'>',
					'Content-type: text/html',
					'Reply-to: '.$admin_email
				);
				$result = wp_mail($email, $subject, $message, $headers);
				if ($result) {
					$output = sprintf( __('Мы отправили цену товара %s на почту %s', 'tesoro-shop'), $product_name, $email);
				} else {
					$output = __('Ой! Произошла ошибка отправки, напишите нам и мы ее исправим', 'tesoro-shop');
				}
			}
		}
	} else {
		$output = __('Некорректно введен email', 'tesoro-shop');
	}

	echo $output;
 
	wp_die(); // don't forget this thing if you don't want "0" to be displayed
}

add_filter( 'woocommerce_product_add_to_cart_text' , 'ts_woocommerce_product_add_to_cart_text' );
function ts_woocommerce_product_add_to_cart_text() {
    global $product;    
    $product_type = $product->product_type;  
	switch ($product_type) {
		case 'variable':
			return __( 'Выбрать', 'tesoro-shop' );
			break;
		
		default:
			return __( 'В корзину', 'tesoro-shop' );
			break;
	}
} 

function wc_varb_price_range( $wcv_price, $product ) {
 
    $prefix = sprintf('%s: ', __('От', 'tesoro-shop'));
 
    $wcv_reg_min_price = $product->get_variation_regular_price( 'min', true );
    $wcv_min_sale_price    = $product->get_variation_sale_price( 'min', true );
    $wcv_max_price = $product->get_variation_price( 'max', true );
    $wcv_min_price = $product->get_variation_price( 'min', true );
 
    $wcv_price = ( $wcv_min_sale_price == $wcv_reg_min_price ) ?
        wc_price( $wcv_reg_min_price ) :
        '<del>' . wc_price( $wcv_reg_min_price ) . '</del>' . '<ins>' . wc_price( $wcv_min_sale_price ) . '</ins>';
 
    return ( $wcv_min_price == $wcv_max_price ) ?
        $wcv_price :
        sprintf('%s%s', $prefix, $wcv_price);
}
 
add_filter( 'woocommerce_variable_sale_price_html', 'wc_varb_price_range', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'wc_varb_price_range', 10, 2 );

remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
add_action( 'woocommerce_after_main_content', 'woocommerce_taxonomy_archive_description', 100 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_taxonomy_archive_description', 100 );