<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme tesoro-shop for publication on WordPress.org
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 *
 * Depending on your implementation, you may want to change the include call:
 *
 * Parent Theme:
 * require_once get_template_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Child Theme:
 * require_once get_stylesheet_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Plugin:
 * require_once dirname( __FILE__ ) . '/path/to/class-tgm-plugin-activation.php';
 */
require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'tesoro_shop_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function tesoro_shop_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.
		// WooCommerce
		array(
			'name'               => 'WooCommerce', 
			'slug'               => 'woocommerce', 
			'source'             => get_template_directory() . '/plugins/woocommerce.3.4.4.zip', 
			'required'           => true, 
			'version'            => '3.4.4', 
			'force_activation'   => true, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/woocommerce/', 
			'is_callable'        => '',
		),
		// Unyson
		array(
			'name'               => 'Unyson', 
			'slug'               => 'unyson', 
			'source'             => get_template_directory() . '/plugins/unyson.2.7.19.zip', 
			'required'           => true, 
			'version'            => '2.7.19', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/unyson/', 
			'is_callable'        => '',
		),
		// Art woo order one click
		array(
			'name'               => 'Art woo order one click', 
			'slug'               => 'art-woo-order-one-click', 
			'source'             => get_template_directory() . '/plugins/art-woo-order-one-click.zip', 
			'required'           => false, 
			'version'            => '1.8.0', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://github.com/tarotum/art-woo-order-one-click', 
			'is_callable'        => '',
		),
		// Woocommerce Quick Buy
		array(
			'name'               => 'Woocommerce Quick Buy', 
			'slug'               => 'woocommerce-quick-buy', 
			'source'             => get_template_directory() . '/plugins/woocommerce-quick-buy.1.9.zip', 
			'required'           => true, 
			'version'            => '1.9', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/woocommerce-quick-buy/', 
			'is_callable'        => '',
		),
		// YITH WooCommerce Added to Cart Popup
		array(
			'name'               => 'YITH WooCommerce Added to Cart Popup', 
			'slug'               => 'yith-woocommerce-added-to-cart-popup', 
			'source'             => get_template_directory() . '/plugins/yith-woocommerce-added-to-cart-popup.1.3.4.zip', 
			'required'           => true, 
			'version'            => '1.3.4', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/yith-woocommerce-added-to-cart-popup/', 
			'is_callable'        => '',
		),
		// woo-local-pickup
		array(
			'name'               => 'Woo Local Pickup', 
			'slug'               => 'woo-local-pickup', 
			'source'             => get_template_directory() . '/plugins/woo-local-pickup.zip', 
			'required'           => true, 
			'version'            => '1.0.1', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/woo-local-pickup/', 
			'is_callable'        => '',
		),
		// Advanced AJAX Product Filters
		array(
			'name'               => 'Advanced AJAX Product Filters', 
			'slug'               => 'woocommerce-ajax-filters', 
			'source'             => get_template_directory() . '/plugins/woocommerce-ajax-products-filter-2.2.1.0.1.zip', 
			'required'           => true, 
			'version'            => '2.2.1.0.1', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://berocket.com/user', 
			'is_callable'        => '',
		),
		// YITH WooCommerce Recently Viewed Products
		array(
			'name'               => 'YITH WooCommerce Recently Viewed Products', 
			'slug'               => 'yith-woocommerce-recently-viewed-products', 
			'source'             => get_template_directory() . '/plugins/yith-woocommerce-recently-viewed-products.1.2.2.zip', 
			'required'           => false, 
			'version'            => '1.2.2', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/yith-infinite-scrolling/', 
			'is_callable'        => '',
		),
		// Menu Image
		array(
			'name'               => 'Menu Image', 
			'slug'               => 'menu-image', 
			'source'             => get_template_directory() . '/plugins/menu-image.2.9.1.zip', 
			'required'           => true, 
			'version'            => '2.9.1', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/menu-image/', 
			'is_callable'        => '',
		),
		// WooCommerce Wishlist
		array(
			'name'               => 'WooCommerce Wishlist', 
			'slug'               => 'ti-woocommerce-wishlist', 
			'source'             => get_template_directory() . '/plugins/ti-woocommerce-wishlist.1.8.14.zip', 
			'required'           => true, 
			'version'            => '1.8.14', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/ti-woocommerce-wishlist/', 
			'is_callable'        => '',
		),
		// Instagram Slider and Carousel Plus Widget
		array(
			'name'               => 'Instagram Slider and Carousel Plus Widget', 
			'slug'               => 'slider-and-carousel-plus-widget-for-instagram', 
			'source'             => get_template_directory() . '/plugins/slider-and-carousel-plus-widget-for-instagram.zip', 
			'required'           => true, 
			'version'            => '1.4.5', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/slider-and-carousel-plus-widget-for-instagram/', 
			'is_callable'        => '',
		),
		// Ajax Search for WooCommerce
		array(
			'name'               => 'Ajax Search for WooCommerce', 
			'slug'               => 'ajax-search-for-woocommerce', 
			'source'             => get_template_directory() . '/plugins/ajax-search-for-woocommerce.1.2.0.zip', 
			'required'           => true, 
			'version'            => '1.2.0', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/ajax-search-for-woocommerce/', 
			'is_callable'        => '',
		),
		// YITH WooCommerce Points And Rewards
		array(
			'name'               => 'YITH WooCommerce Points And Rewards', 
			'slug'               => 'yith-woocommerce-points-and-rewards-premium', 
			'source'             => get_template_directory() . '/plugins/yith-woocommerce-points-and-rewards-premium.zip', 
			'required'           => true, 
			'version'            => '1.5.7', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://yithemes.com/themes/plugins/yith-woocommerce-points-and-rewards/', 
			'is_callable'        => '',
		),
		// Woo Shipping for Nova Poshta
		array(
			'name'               => 'Woo Shipping for Nova Poshta', 
			'slug'               => 'woo-shipping-for-nova-poshta', 
			'source'             => get_template_directory() . '/plugins/woo-shipping-for-nova-poshta.2.1.5.zip', 
			'required'           => true, 
			'version'            => '2.1.5', 
			'force_activation'   => true, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/woo-shipping-for-nova-poshta/', 
			'is_callable'        => '',
		),
		// YITH WooCommerce Product Bundles
		array(
			'name'               => 'YITH WooCommerce Product Bundles', 
			'slug'               => 'yith-woocommerce-product-bundles', 
			'source'             => get_template_directory() . '/plugins/yith-woocommerce-product-bundles.zip', 
			'required'           => false, 
			'version'            => '1.1.9', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/yith-woocommerce-product-bundles/', 
			'is_callable'        => '',
		),
		// WP All Import
		array(
			'name'               => 'WP All Import', 
			'slug'               => 'wp-all-import', 
			'source'             => get_template_directory() . '/plugins/wp-all-import-pro_4.5.5.zip', 
			'required'           => false, 
			'version'            => '4.5.5', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/wp-all-import/', 
			'is_callable'        => '',
		),
		// WooCommerce Add-On – WooCommerce Add-On Unlimited License
		array(
			'name'               => 'WP All Import - WooCommerce Add-On', 
			'slug'               => 'woocommerce-xml-csv-product-import', 
			'source'             => get_template_directory() . '/plugins/wpai-woocommerce-add-on_3.0.6.zip', 
			'required'           => false, 
			'version'            => '3.0.6', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/woocommerce-xml-csv-product-import/', 
			'is_callable'        => '',
		),
		array(
			'name'               => 'PageMenu', 
			'slug'               => 'page-menu', 
			'source'             => get_template_directory() . '/plugins/page-menu.5.1.2.zip', 
			'required'           => false, 
			'version'            => '5.1.2', 
			'force_activation'   => false, 
			'force_deactivation' => false, 
			'external_url'       => 'https://ru.wordpress.org/plugins/page-menu/', 
			'is_callable'        => '',
		),
		// This is an example of how to include a plugin from an arbitrary external source in your theme.
		// array(
		// 	'name'         => 'TGM New Media Plugin', 
		// 	'slug'         => 'tgm-new-media-plugin', 
		// 	'source'       => 'https://s3.amazonaws.com/tgm/tgm-new-media-plugin.zip', 
		// 	'required'     => true, 
		// 	'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', 
		// ),

		// This is an example of how to include a plugin from a GitHub repository in your theme.
		// This presumes that the plugin code is based in the root of the GitHub repository
		// and not in a subdirectory ('/src') of the repository.
		// array(
		// 	'name'      => 'Adminbar Link Comments to Pending',
		// 	'slug'      => 'adminbar-link-comments-to-pending',
		// 	'source'    => 'https://github.com/jrfnl/WP-adminbar-comments-to-pending/archive/master.zip',
		// ),

		// This is an example of how to include a plugin from the WordPress Plugin Repository.
		array(
			'name'      => 'SVG Support',
			'slug'      => 'svg-support',
			'required'  => true,
		),
		array(
			'name'               => 'Contact Form 7', 
			'slug'               => 'contact-form-7',  
			'required'           => true,
		),
		array(
			'name'               => 'MailChimp for WordPress', 
			'slug'               => 'mailchimp-for-wp',  
			'required'           => true,
		),
		array(
			'name'               => 'Classic Editor', 
			'slug'               => 'classic-editor',  
			'required'           => true,
		),

		// This is an example of the use of 'is_callable' functionality. A user could - for instance -
		// have WPSEO installed *or* WPSEO Premium. The slug would in that last case be different, i.e.
		// 'wordpress-seo-premium'.
		// By setting 'is_callable' to either a function from that plugin or a class method
		// `array( 'class', 'method' )` similar to how you hook in to actions and filters, TGMPA can still
		// recognize the plugin as being installed.
		// array(
		// 	'name'        => 'WordPress SEO by Yoast',
		// 	'slug'        => 'wordpress-seo',
		// 	'is_callable' => 'wpseo_init',
		// ),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'tesoro-shop',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		/*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'tesoro-shop' ),
			'menu_title'                      => __( 'Install Plugins', 'tesoro-shop' ),
			/* translators: %s: plugin name. * /
			'installing'                      => __( 'Installing Plugin: %s', 'tesoro-shop' ),
			/* translators: %s: plugin name. * /
			'updating'                        => __( 'Updating Plugin: %s', 'tesoro-shop' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'tesoro-shop' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'tesoro-shop'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'tesoro-shop'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'tesoro-shop'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). * /
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'tesoro-shop'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'tesoro-shop'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'tesoro-shop'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'tesoro-shop'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'tesoro-shop'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'tesoro-shop'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'tesoro-shop' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'tesoro-shop' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'tesoro-shop' ),
			/* translators: 1: plugin name. * /
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'tesoro-shop' ),
			/* translators: 1: plugin name. * /
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'tesoro-shop' ),
			/* translators: 1: dashboard link. * /
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'tesoro-shop' ),
			'dismiss'                         => __( 'Dismiss this notice', 'tesoro-shop' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'tesoro-shop' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'tesoro-shop' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		*/
	);

	tgmpa( $plugins, $config );
}
