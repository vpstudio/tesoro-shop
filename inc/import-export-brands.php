<?php

/**
 * Add the custom column to the exporter and the exporter column menu.
 *
 * @param array $columns
 * @return array $columns
 */
function add_export_column( $columns ) {

	// column slug => column name
	$columns['brand'] = __( 'Brands', 'tesoro-shop' );

	return $columns;
}
add_filter( 'woocommerce_product_export_column_names', 'add_export_column' );
add_filter( 'woocommerce_product_export_product_default_columns', 'add_export_column' );

/**
 * Provide the data to be exported for one item in the column.
 *
 * @param mixed $value (default: '')
 * @param WC_Product $product
 * @return mixed $value - Should be in a format that can be output into a text file (string, numeric, etc).
 */
function add_export_data( $value, $product ) {

	$terms = wp_get_post_terms( $product->get_id(), 'brand' );

	if ( ! is_wp_error( $terms ) && !empty($terms) ) {
		$data = array();
		foreach ( (array) $terms as $term ) {
			$data[] = $term->name;
		}
		$value = str_replace(array('["','"]'), '', json_encode( $data ) );
	}

	return $value;
}
// Filter you want to hook into will be: 'woocommerce_product_export_product_column_{$column_slug}'.
add_filter( 'woocommerce_product_export_product_column_brand', 'add_export_data', 10, 2 );



/**
 * Register the 'Custom Column' column in the importer.
 *
 * @param array $options
 * @return array $options
 */
function add_column_to_importer( $options ) {

	// column slug => column name
	$options['brand'] = __( 'Brands', 'tesoro-shop' );

	return $options;
}
add_filter( 'woocommerce_csv_product_import_mapping_options', 'add_column_to_importer' );


/**
 * Add automatic mapping support for 'Custom Column'. 
 * This will automatically select the correct mapping for columns named 'Custom Column' or 'custom column'.
 *
 * @param array $columns
 * @return array $columns
 */
function add_column_to_mapping_screen( $columns ) {
	
	// potential column name => column slug
	$columns[__( 'Brands', 'tesoro-shop' )] = 'brand';

	$columns['Brands'] = 'brand';

	return $columns;
}
add_filter( 'woocommerce_csv_product_import_mapping_default_columns', 'add_column_to_mapping_screen' );

/**
 * Process the data read from the CSV file.
 * This just saves the value in meta data, but you can do anything you want here with the data.
 *
 * @param WC_Product $object - Product being imported or updated.
 * @param array $data - CSV data read for the product.
 * @return WC_Product $object
 */
function set_taxonomy( $object, $data ) {

    if ( ! empty( $data['brand'] ) ) {
        $brands = explode( ',', $data[ 'brand' ] );
        wp_set_object_terms( $object->get_id(), $brands, 'brand' );
    }
	return $object;
}
add_filter( 'woocommerce_product_import_pre_insert_product_object', 'set_taxonomy', 10, 2 );
