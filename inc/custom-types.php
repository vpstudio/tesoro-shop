<?php

function ts_custom_taxonomy()  {

$labels = array(
    'name'                       => __('Brands', 'tesoro-shop'),
    'singular_name'              => __('Brand', 'tesoro-shop'),
    'menu_name'                  => __('Brands', 'tesoro-shop'),
    'all_items'                  => __('All Brands', 'tesoro-shop'),
    'new_item_name'              => __('New Brand', 'tesoro-shop'),
    'add_new_item'               => __('Add new Brand', 'tesoro-shop'),
    'edit_item'                  => __('Edit new Brand', 'tesoro-shop'),
    'update_item'                => __('Update new Brand', 'tesoro-shop'),
    'search_items'               => __('Find Brand', 'tesoro-shop'),
    'add_or_remove_items'        => 'Add or remove Brands',
);
$args = array(
    'labels'                     => $labels,
    // 'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
);
register_taxonomy( 'brand', 'product', $args );
register_taxonomy_for_object_type( 'brand', 'product' );

}

add_action( 'init', 'ts_custom_taxonomy', 0 );