<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package tesoro-shop
 */

$soc_instagram_link =  !empty(fw_get_db_settings_option('soc_instagram')) ? basename(fw_get_db_settings_option('soc_instagram')) : null;

get_header();
?>
<div class="container">
	<div class="row">
		<div class="col"><?php woocommerce_breadcrumb(); ?></div>
	</div>
	<div class="row">
		<div class="col col-xl-9">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">

				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', get_post_type() );

				endwhile; // End of the loop.
				?>

				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
		<div class="col-3 d-none d-xl-block">
			<?php get_sidebar(); ?>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>
		</div>
	</div>
</div>
	

<?php
	if (!empty($soc_instagram_link)) { ?>
		<!-- instagram -->
		<section class="section section--instagram">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2 class="section__title">Instagram</h2> <a href="https://www.instagram.com/<?php echo $soc_instagram_link; ?>" rel="noopener noreferrer" target="_blank" class="instagram-username">@<?php echo $soc_instagram_link; ?></a>
						<span class="section__subtitle"><?php _e('Подписывайся на наш профиль в Instagram!', 'tesoro-shop'); ?></span>
					</div>
				</div>
			</div> 
			<?php echo do_shortcode('[iscwp-slider slidestoshow="5" show_comments_count="false" show_likes_count="false" popup="false" dots="false" autoplay="false" username="'. $soc_instagram_link .'"]'); ?>
		</section>
		<!-- #instagram -->
	<?php } 

get_footer();
