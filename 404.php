<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package tesoro-shop
 */

$custom_logo_id =  !empty(fw_get_db_customizer_option('second_logo')) ? fw_get_db_customizer_option('second_logo')['attachment_id'] : '';
$custom_logo_url = !empty(fw_get_db_customizer_option('second_logo')) ? fw_get_db_customizer_option('second_logo')['url'] : '';
$custom_logo_alt = !empty(fw_get_db_customizer_option('second_logo')) ? get_post_meta($custom_logo_id, '_wp_attachment_image_alt', true) : '';
$soc_facebook_link = fw_get_db_settings_option('soc_facebook');
$soc_instagram_link = fw_get_db_settings_option('soc_instagram');
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div id="page" class="site">

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<header class="page-header">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="not-found__logo">
                        <img src="<?php echo $custom_logo_url; ?>" class="img-responsive" alt="<?php echo $custom_logo_alt; ?>">
                    </a>
				</header><!-- .page-header -->

				<div class="page-content">
					<span class="not-found__title">404</span>
					<p class="not-found__text"><?php _e( 'Извините, кажется, такой страницы не существует. Попробуйте снова!', 'tesoro-shop' ); ?></p>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="button"><?php _e( 'Перейти на главную', 'tesoro-shop' ); ?></a>

				</div><!-- .page-content -->
				<footer class="page-footer">
					<ul class="not-found__soc">
						<li><a href="<?php echo $soc_facebook_link; ?>" rel="noopener noreferrer nofollow" target="_blank" title="<?php echo $soc_facebook_link; ?>">
                            <i class="fa fa-facebook"></i>
                        </a></li>
                        <li><a href="<?php echo $soc_instagram_link; ?>" rel="noopener noreferrer nofollow" target="_blank" title="<?php echo $soc_instagram_link; ?>">
                            <i class="fa fa-instagram"></i>
                        </a></li>
                        <li><a href="mailto:<?php echo get_option('admin_email', ''); ?>" rel="noopener noreferrer nofollow" title="<?php echo get_option('admin_email', ''); ?>">
                            <i class="fa fa-envelope"></i>
                        </a></li>
					</ul>
					<p>Designed by VP Production</p>
				</footer>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

	</div><!-- #page -->
	<?php wp_footer(); ?>

</body>
</html>
