<?php
/**
 * tesoro-shop functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package tesoro-shop
 */

if ( ! function_exists( 'tesoro-shop_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tesoro_shop_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on tesoro-shop, use a find and replace
		 * to change 'tesoro-shop' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'tesoro-shop', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'menu-images-28x28', 28, 28, false );

		add_image_size( 'tesoro-shop-product-box', 135, 135, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Top line menu', 'tesoro-shop' ),
			'menu-2' => esc_html__( 'Primary menu', 'tesoro-shop' ),
			'menu-3' => esc_html__( 'Overlay menu', 'tesoro-shop' ),
			'menu-4' => esc_html__( 'Footer menu', 'tesoro-shop' ),
			'menu-5' => esc_html__( 'Second Footer menu', 'tesoro-shop' ),
			'menu-6' => esc_html__( 'Langs menu', 'tesoro-shop' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
		add_theme_support( 'woocommerce' );
	}
endif;

add_action( 'after_setup_theme', 'tesoro_shop_setup' );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function tesoro_shop_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Shop filters bar', 'tesoro-shop' ),
		'id'            => 'filterbar',
		'description'   => esc_html__( 'Add widgets here.', 'tesoro-shop' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name'          => esc_html__( 'Shop nav-filters bar', 'tesoro-shop' ),
		'id'            => 'nav-filterbar',
		'description'   => esc_html__( 'Add widgets here.', 'tesoro-shop' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar( array(
		'name'          => esc_html__( 'News sidebar', 'tesoro-shop' ),
		'id'            => 'newsbar',
		'description'   => esc_html__( 'Add widgets here.', 'tesoro-shop' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'tesoro_shop_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tesoro_shop_scripts() {
	wp_enqueue_style( 'tesoro-shop-style', get_template_directory_uri() . '/dist/css/styles.css', array(), wp_get_theme()->get( 'Version' ) );
	wp_register_script( 'tesoro-shop-scripts', get_template_directory_uri() . '/dist/js/bundle.js', array(), wp_get_theme()->get( 'Version' ), true );
	wp_localize_script( 'tesoro-shop-scripts', 'tsajax', 
		array(
			'url' => admin_url('admin-ajax.php')
		)
	); 
	wp_enqueue_script( 'tesoro-shop-scripts' );
}
add_action( 'wp_enqueue_scripts', 'tesoro_shop_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Plugins activation.
 */
require get_template_directory() . '/inc/plugin-activation.php';

/**
 * Custom types include.
 */
require get_template_directory() . '/inc/custom-types.php';

/**
 * Custom widget include.
 */
require get_template_directory() . '/inc/class-ts-widget-recent-posts.php';

/**
 * Custom types include.
 */
require get_template_directory() . '/inc/import-export-brands.php';

/**
 * Iclude ajax loading posts.
 */
require get_template_directory() . '/inc/ajax-loading-posts.php';

/**
 * Iclude ajax loading posts.
 */
require get_template_directory() . '/inc/woocommerce-customization.php';


add_filter('wp_get_attachment_image_attributes', 'change_attachement_image_attributes', 20, 2);
function change_attachement_image_attributes( $attr, $attachment ){
    // Get post parent
    $parent = get_post_field( 'post_parent', $attachment);

    // Get post type to check if it's product
    $type = get_post_field( 'post_type', $parent);
    if( $type == 'product' || $type == 'post' ){
		/// Get title
		$title = get_post_field( 'post_title', $parent);

		$attr['alt'] = $title;
		$attr['title'] = $title;
        return $attr;
    }
    
    return $attr;
}

function remove_recent_comments_style() {  
	global $wp_widget_factory;  
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );  

	register_widget('TS_Widget_Recent_Posts');
}  
add_action( 'widgets_init', 'remove_recent_comments_style' );

function admin_style() {
    wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');


  
function ts_analytics() { 
	?>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
	   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
	   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
	   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	   ym(48480617, "init", {
			id:48480617,
			clickmap:true,
			trackLinks:true,
			accurateTrackBounce:true,
			webvisor:true
	   });
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/48480617" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
	<?php
}
	
// add_action( 'wp_head', 'ts_analytics', 10 );


add_filter( 'wp_get_nav_menu_items', 'nav_remove_empty_category_menu_item', 10, 3 );
function nav_remove_empty_category_menu_item ( $items, $menu, $args ) {
    global $wpdb;
    $nopost = $wpdb->get_col( "SELECT term_taxonomy_id FROM $wpdb->term_taxonomy WHERE count = 0" );
    foreach ( $items as $key => $item ) {
        if ( ( 'taxonomy' == $item->type ) && ( in_array( $item->object_id, $nopost ) ) ) {
            unset( $items[$key] );
        }
	}

    return $items;
}


add_action( 'wp_footer', 'add_get_call_binotel' );
function add_get_call_binotel() {
	if ( !is_archive() ) {
    ?>
        <script type="text/javascript">
		(function(d, w, s) {
			var widgetHash = 'hbd1pl333drygcsuw0ip', gcw = d.createElement(s); gcw.type = 'text/javascript'; gcw.async = true;
			gcw.src = '//widgets.binotel.com/getcall/widgets/'+ widgetHash +'.js';
			var sn = d.getElementsByTagName(s)[0]; sn.parentNode.insertBefore(gcw, sn);
		})(document, window, 'script');
		</script> 
	<?php }
}

/**
 * Remove excess head tags
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_resource_hints', 2);
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );