<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tesoro-shop
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h2 class="comments-title"><?php _e('Комментарии', 'tesoro-shop'); ?></h2><!-- .comments-title -->


		<ol class="commentlist">
			<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
		</ol>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
			$cpage = get_query_var('cpage') ? get_query_var('cpage') : 1;
			if( $cpage > 1 ) {
				echo '<div data-id='. get_the_ID() .' data-page='. $cpage .' class="comment_loadmore">'. __('Load More', 'tesoro-shop') .'</div>';
			}
		endif; ?>

		<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'tesoro-shop' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments(). ?>

		<div id="review_form_wrapper">
			<div id="review_form">
				<?php
					$commenter = wp_get_current_commenter();

					$comment_form = array(
						'title_reply'          => have_comments() ? __( 'Add a comment', 'woocommerce' ) : sprintf( __( 'Be the first to comment &ldquo;%s&rdquo;', 'woocommerce' ), get_the_title() ),
						'title_reply_to'       => __( 'Leave a Reply to %s', 'woocommerce' ),
						'title_reply_before'   => '<span id="reply-title" class="comment-reply-title">',
						'title_reply_after'    => '</span>',
						'comment_notes_after'  => '',
						'fields'               => array(
							'author' => '<p class="comment-form-author">' .
										'<input id="author" placeholder="&#xf007; ' . esc_html__( 'Ваше имя *', 'tesoro-shop' ) . '" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" required /></p>',
							'email'  => '<p class="comment-form-email">' .
										'<input id="email" placeholder="&#xf0e0; ' . esc_html__( 'Ваш email *', 'tesoro-shop' ) . '" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" required /></p>',
						),
						'label_submit'  => __( 'Submit', 'woocommerce' ),
						'logged_in_as'  => '',
						'comment_field' => '',
					);

					if ( $account_page_url = wc_get_page_permalink( 'myaccount' ) ) {
						$comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', 'woocommerce' ), esc_url( $account_page_url ) ) . '</p>';
					}
					
					$comment_form['comment_field'] = '<p class="comment-form-comment"><textarea placeholder="&#xf005; ' . esc_html__( 'Ваш отзыв *', 'tesoro-shop' ) . '" id="comment" name="comment" cols="45" rows="8" required></textarea></p>';
					
					comment_form( $comment_form );
				?>
			</div>
		</div>

</div><!-- #comments -->
