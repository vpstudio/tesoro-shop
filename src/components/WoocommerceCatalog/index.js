import './woocommerce-catalog.scss';
const optionsPanel = document.querySelector('.catalog__otions');

if (optionsPanel) {
  const showPanel = () => {
    if (window.pageYOffset > 200 && window.innerWidth <= 991) {
      optionsPanel.classList.add('fixed');
    } else {
      optionsPanel.classList.remove('fixed');
    }

    requestAnimationFrame(showPanel);
  };
  requestAnimationFrame(showPanel);
}
