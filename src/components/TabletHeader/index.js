import './tablet-header.scss';

if ($('.tablet-search-handler').length > 0) {
  $('.tablet-search-handler').click(function(e) {
    e.preventDefault();
    $('.tablet-search-bar').fadeToggle('fast');
  });
}
