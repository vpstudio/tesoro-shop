import './callback-block.scss';
import 'magnific-popup/dist/jquery.magnific-popup';
import 'magnific-popup/dist/magnific-popup.css';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

$('.callback-block').hover(
  e => {
    $('.callback-block-dropdown').slideDown('fast');
  },
  e => {
    $('.callback-block-dropdown').slideUp('fast');
  }
);

$('.callback').magnificPopup({
  type: 'inline',
  preloader: false,
  focus: '#number',
  closeOnBgClick: true,
  fixedContentPos: false,
  removalDelay: 300,
  mainClass: 'mfp-fade',
  callbacks: {
    beforeOpen: function() {
      if ($(window).width() < 700) {
        this.st.focus = false;
      } else {
        this.st.focus = '#number';
      }
    },
    open: () => {
      disableBodyScroll(document.querySelector('.callback'));
    },
    close: () => {
      enableBodyScroll(document.querySelector('.callback'));
    }
  }
});
