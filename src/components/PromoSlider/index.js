import './promo-slider.scss';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

import imagesLoaded from 'imagesLoaded';

if ($('.promo-slider').length > 0) {
	imagesLoaded(document.querySelector('.owl-carousel.promo-slider'), () => {
		$('.promo-slider').owlCarousel({
			items: 1,
			loop: true,
			autoplay: true,
			smartSpeed: 1500,
			dots: true,
			autoplayHoverPause: true
		});
	});
}