import './product-slider.scss';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

const productSliderNav = $('.product-slider-swicher').children();
const slider = document.querySelector('.product-slider');
const loadProducts = sliderType => {
  const container = document.querySelector('.product-slider__storage');
  const storage = container.querySelector(`.${sliderType}`);
  if (storage.innerHTML !== '') {
    $(slider)
      .trigger('replace.owl.carousel', storage.innerHTML)
      .trigger('refresh.owl.carousel');
  }
  return storage.innerHTML;
};

if ($(slider).length > 0) {
  const defaultSliderType = slider.dataset.slider;
  $(slider).owlCarousel({
    nav: true,
    loop: true,
    navText: [
      '<span class="fa fa-chevron-left"></span>',
      '<span class="fa fa-chevron-right"></span>'
    ],
    checkVisibility: true,
    responsive: {
      0: {
        items: 2,
        dots: true,
        nav: false,
        margin: 19
      },
      768: {
        items: 3,
        dots: true,
        nav: false,
        margin: 24
      },
      992: {
        items: 4,
        dots: true,
        nav: false,
        margin: 24
      },
      1260: {
        items: 5,
        dots: false,
        nav: true,
        margin: 30
        // stagePadding: 30
      }
    }
  });
  loadProducts(defaultSliderType);
}

if (productSliderNav.length > 0) {
  productSliderNav.each((i, val) => {
    let sliderType = val.dataset.slider;
    val.onclick = () => {
      slider.dataset.slider = sliderType;
      productSliderNav.removeClass('active');
      productSliderNav.each((i, val) => {
        if (val.dataset.slider === sliderType) {
          $(val).addClass('active');
        }
      });
      loadProducts(sliderType);
    };
  });
}
