import './news.scss';


$('#loadmore').click(function () {
    $(this).text('Загружаю...'); // изменяем текст кнопки, вы также можете добавить прелоадер
    var data = {
        'action': 'loadmore',
        'query': ts_posts,
        'page': current_page
    };
    $.ajax({
        url: tsajax.url,
        data: data,
        type: 'POST',
        success: function (data) {
            if (data) {
                $('#loadmore').text('Загрузить ещё')
                $('#posts-loop').append(data); // вставляем новые посты
                current_page++; // увеличиваем номер страницы на единицу
                if (current_page == max_pages) $("#loadmore").remove(); // если последняя страница, удаляем кнопку
            } else {
                $('#loadmore').remove(); // если мы дошли до последней страницы постов, скроем кнопку
            }
        }
    });
});