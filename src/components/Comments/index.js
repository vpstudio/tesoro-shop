import './comments.scss';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

if ($('.comments-slider').length > 0) {
	$('.comments-slider').owlCarousel({
		items: 1,
		loop: true,
		nav: false,
		responsive: {
			0: {
				dots: true,
				margin: 15
			},
			768: {
				dots: true,
				margin: 80
			},
			1440: {
				dots: true,
				margin: 210
			}
		}
	});
}