import './gallerySlider.scss';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

import imagesLoaded from 'imagesLoaded';

if ($('.gallery-slider').length > 0) {
  imagesLoaded(document.querySelector('.gallery-slider'), () => {
    $('.gallery-slider').owlCarousel({
      items: 1,
      loop: true,
      dots: true,
      autoplay: true,
      autoplayTimeout: 3000,
      autoplayHoverPause: true,
    });
  });
}
