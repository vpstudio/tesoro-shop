import './sticky-nav.scss';
import { closeMenu } from '../OverlayMenu';
const header = document.querySelector('.sticky-nav');
const menu = document.querySelector('.overlay-menu');
const openBtn = document.querySelectorAll('.overlay-menu-openbtn');

const showMenu = () => {
  if (window.innerWidth < 1260) return;
  if (window.pageYOffset > 300) {
    menu.dataset.show === 'true' || header.classList.add('sticky');
  } else {
    menu.dataset.show === 'true' || header.classList.remove('sticky');
    $.each(openBtn, function() {
      closeMenu(this.querySelector('i'));
    });
  }
  requestAnimationFrame(showMenu);
};

requestAnimationFrame(showMenu);
