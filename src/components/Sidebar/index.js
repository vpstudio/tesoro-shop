import './sidebar.scss';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

const sidebarMobile = document.getElementById('mobile-sidebar');

function addApplyFilterButton() {
  const resetButton = sidebarMobile.querySelector(
    '.berocket_aapf_reset_button'
  );
  const applyButton = document.createElement('a');
  applyButton.classList = 'widget_update_button';
  applyButton.setAttribute('href', '#');
  applyButton.setAttribute('rel', 'noopener noreferrer');
  applyButton.innerText = 'Показать';
  resetButton.parentNode.insertBefore(applyButton, resetButton);
}
if (sidebarMobile) {
  const sidebarContent = sidebarMobile.querySelector('.sidebar-content');
  const sidebarContainer = sidebarContent.querySelector('.sidebar-container');
  // const scroll = new BScroll(sidebarContainer, {
  //   scrollY: true,
  //   click: true,
  //   startY: -10
  // });
  addApplyFilterButton();

  if ($('.filters').length > 0) {
    $('.filters').on('click', function(e) {
      e.preventDefault();

      disableBodyScroll(sidebarContainer);
      $('.sidebar--mobile').addClass('visible');
    });
  }

  $(document).on('click', '.widget_update_button', function(e) {
    e.preventDefault();

    enableBodyScroll(sidebarContainer);
    $('.sidebar').removeClass('visible');
  });
  $(document).on('click', '.sidebar__close-btn', function(e) {
    e.preventDefault();

    enableBodyScroll(sidebarContainer);
    $('.sidebar').removeClass('visible');
  });

  $(window).on('click', function() {
    if (event.target == sidebarMobile) {
      enableBodyScroll(sidebarContainer);
      $('.sidebar').removeClass('visible');
    }
  });
}
