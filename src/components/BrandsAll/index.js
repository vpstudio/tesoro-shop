import './brandsAll.scss';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

import imagesLoaded from 'imagesLoaded';

const brandsProductSlider = document.querySelector('.brands-product-slider');

if ($('.brands-all--mobile').length > 0) {
  const brandItem = document.querySelectorAll('.brand-item');

  for (let i = 0; i < brandItem.length; i++) {
    if (i % 4 === 0) {
      const brandItemBox = document.createElement('div');
      brandItemBox.classList.add('brands-slider-col');

      if (brandItem[i]) brandItemBox.appendChild(brandItem[i]);
      if (brandItem[i + 1]) brandItemBox.appendChild(brandItem[i + 1]);
      if (brandItem[i + 2]) brandItemBox.appendChild(brandItem[i + 2]);
      if (brandItem[i + 3]) brandItemBox.appendChild(brandItem[i + 3]);
      if (brandItem[i + 4]) brandItemBox.appendChild(brandItem[i + 4]);

      document
        .querySelector('.brands-all--mobile.owl-carousel.owl-theme')
        .appendChild(brandItemBox);
    }
  }

  $('.brands-all--mobile.owl-carousel.owl-theme').owlCarousel({
    dots: true,
    nav: false,
    items: 2,
    slideBy: 2
  });

  const toogleBrandsSlider = () => {
    if ($(window).width() > 768) {
      $('.brands-all--mobile').css('display', 'none');
      $('.brands-all--desktop').html('');
      $('.owl-item:not(.cloned)  .brands-slider-col .brand-item')
        .clone()
        .appendTo('.brands-all--desktop');
      $('.brands-all--desktop').addClass('visible');
    } else {
      $('.brands-all--desktop').removeClass('visible');
      $('.brands-all--mobile').css('display', 'block');
    }
  };

  toogleBrandsSlider();

  $(window).resize(toogleBrandsSlider);
}

if ($(brandsProductSlider).length > 0) {
  $(brandsProductSlider).owlCarousel({
    loop: true,
    nav: true,
    navText: [
      '<span class="fa fa-chevron-left"></span>',
      '<span class="fa fa-chevron-right"></span>'
    ],
    checkVisibility: true,
    responsive: {
      0: {
        items: 2,
        dots: true,
        nav: false,
        margin: 19
      },
      768: {
        items: 3,
        dots: true,
        nav: false,
        margin: 24
      },
      992: {
        items: 4,
        dots: true,
        nav: false,
        margin: 24
      },
      1400: {
        items: 5,
        dots: false,
        nav: true,
        margin: 30
        // stagePadding: 30
      }
    }
  });
}
