import './search-form.scss';
import 'magnific-popup/dist/jquery.magnific-popup';
import 'magnific-popup/dist/magnific-popup.css';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

$('.search-products').magnificPopup({
  type: 'inline',
  preloader: false,
  focus: '.aws-search-field',
  closeOnBgClick: true,
  fixedContentPos: true,
  removalDelay: 300,
  mainClass: 'mfp-fade',
  // When elemened is focused, some mobile browsers in some cases zoom in
  // It looks not nice, so we disable it:
  callbacks: {
    beforeOpen: function() {
      if ($(window).width() < 700) {
        this.st.focus = false;
      } else {
        this.st.focus = '.aws-search-field';
      }
    },
    open: () => {
      disableBodyScroll(document.querySelector('.search-products'));
    },
    close: () => {
      enableBodyScroll(document.querySelector('.search-products'));
    }
  }
});
