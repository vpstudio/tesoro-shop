import './mobile-header.scss';

if ($('.mobile-search-handler').length > 0) {
  $('.mobile-search-handler').click(function(e) {
    e.preventDefault();
    $('.mobile-search-bar').fadeToggle('fast');
  });
}
