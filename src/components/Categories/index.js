import './categories.scss';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

const buildSlider = () => {
	$('.section-categories .categories').owlCarousel({
		items: 1,
		dots: true,
		loop: true
	});
};

const destroySlider = () => {
	var owl = $('.section-categories .categories');
	owl.trigger('destroy.owl.carousel');
	owl.addClass('off');
};

if ($('.section-categories .categories').length > 0) {
	if ($(window).width() < 480) {
		buildSlider();
	} else {
		$('.section-categories .categories').addClass('off');
	}
}

$(window).resize(function () {
	if ($(window).width() < 480) {
		buildSlider();
	} else {
		destroySlider();
	}
});