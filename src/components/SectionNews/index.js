import './section-news.scss';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

const owl = $('.news');

const buildSlider = () => {
	owl.owlCarousel({
		items: 1,
		dots: true,
		loop: true
	});
};

const destroySlider = () => {
	owl.trigger('destroy.owl.carousel');
	owl.addClass('off');
};

if (owl.length > 0) {
	if ($(window).width() < 768) {
		buildSlider();
	} else {
		owl.addClass('off');
	}
}

$(window).resize(function () {
	if ($(window).width() < 768) {
		buildSlider();
	} else {
		destroySlider();
	}
});