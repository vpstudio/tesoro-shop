import './woocommerce-product-boxes.scss';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

const owl = $('.woocommerce-product-boxes .row');

const buildSlider = () => {
    owl.owlCarousel({
        loop: true,
        stagePadding: 5,
        responsive: {
            0: {
                items: 1,
                dots: true,
            },
            768: {
                items: 2,
                dots: true,
            }
        }
    });
};

const destroySlider = () => {
    owl.trigger('destroy.owl.carousel');
    owl.addClass('off');
};

if (owl.length > 0) {
    if ($(window).width() < 990) {
        buildSlider();
    } else {
        owl.addClass('off');
    }
}

$(window).resize(function () {
    if ($(window).width() < 990) {
        buildSlider();
    } else {
        destroySlider();
    }
});