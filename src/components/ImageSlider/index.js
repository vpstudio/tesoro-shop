import './image-slider.scss';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

import imagesLoaded from 'imagesLoaded';

if ($('.image-slider').length > 0) {
  imagesLoaded(document.querySelector('.owl-carousel.image-slider'), () => {
    $('.image-slider').owlCarousel({
      items: 1,
      loop: true,
      autoplay: true,
      smartSpeed: 1500,
      autoplayHoverPause: true,
      dots: true,
      nav: true,
      navText: [
        '<span class="fa fa-chevron-left"></span>',
        '<span class="fa fa-chevron-right"></span>'
      ]
    });
  });
}
