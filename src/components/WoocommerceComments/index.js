import './woocommerce-comments.scss';

// load more button click event
$('.comment_loadmore').click(function () {
    var button = $(this);
    var page = parseInt(button.data('page'));

    page--;
    $.ajax({
        url: tsajax.url,
        data: {
            'action': 'cloadmore',
            'post_id': button.data('id'),
            'cpage': page,
        },
        type: 'POST',
        beforeSend: function (xhr) {
            button.addClass('loading'); // preloader here
            button.prop('disabled', true);
        },
        success: function (data) {
            if (data) {
                $('ol.commentlist').append(data);
                button.removeClass('loading');
                button.prop('disabled', false);
                button.data('page', page);
                // if the last page, remove the button
                if (page == 1)
                    button.remove();
            } else {
                button.remove();
            }
        }
    });
    return false;
});