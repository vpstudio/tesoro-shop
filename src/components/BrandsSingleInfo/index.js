import './brandsSingleInfo.scss';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

const slider = document.querySelector('.brand-slider');

if ($(slider).length > 0) {
  $(slider).owlCarousel({
    loop: true,
    nav: true,
    navText: [
      '<span class="fa fa-chevron-left"></span>',
      '<span class="fa fa-chevron-right"></span>'
    ],
    checkVisibility: true,
    responsive: {
      0: {
        items: 2,
        dots: true,
        nav: false,
        margin: 19
      },
      768: {
        items: 3,
        dots: true,
        nav: false,
        margin: 24
      },
      992: {
        items: 4,
        dots: true,
        nav: false,
        margin: 24
      },
      1400: {
        items: 5,
        dots: false,
        nav: true,
        margin: 30
        // stagePadding: 30
      }
    }
  });
}
