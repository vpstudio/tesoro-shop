import './rbGrid.scss';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

const owl = $('.rb-grid--mobile');

const buildSlider = () => {
  owl.owlCarousel({
    dots: true,
    items: 1,
    loop: true
  });
};

const destroySlider = () => {
  owl.trigger('destroy.owl.carousel');
  owl.addClass('off');
};

if (owl.length > 0) {
  if ($(window).width() < 768) {
    buildSlider();
  } else {
    owl.addClass('off');
  }
}

$(window).resize(function() {
  if ($(window).width() < 768) {
    buildSlider();
  } else {
    destroySlider();
  }
});
