import './woocommerce-product.scss';

function toogleProductInfo($products) {

    $products.each(function () {

        $(this).hover(
            function (e) {
                $(this).children('.woocommerce-loop-product-hover')
                    .css("display", "flex")
                    // .hide()
                    .fadeIn(250);

            },
            function (e) {
                $(this).children('.woocommerce-loop-product-hover')
                    .fadeOut(150);

            }
        )
    })
}