import './brands.scss';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'owl.carousel';

if ($('.brands').length > 0) {
  $('.brands').owlCarousel({
    dots: false,
    nav: true,
    margin: 15,
    loop: true,
    navText: [
      '<span class="fa fa-chevron-left"></span>',
      '<span class="fa fa-chevron-right"></span>'
    ],
    responsive: {
      0: {
        items: 1
      },
      445: {
        items: 2
      },
      767: {
        items: 4
      },
      992: {
        stagePadding: 60,
        items: 5
      },
      1108: {
        stagePadding: 60,
        items: 5
      }
    }
  });
}
