import './video.scss';
import 'magnific-popup/dist/jquery.magnific-popup';
import 'magnific-popup/dist/magnific-popup.css';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

export default (() => {
  if ($('.popup-youtube').length > 0) {
    $(document).ready(function() {
      $('.popup-youtube').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 250,
        preloader: true,
        fixedContentPos: false,
        callbacks: {
          open: function() {
            disableBodyScroll(document.querySelector('.popup-youtube'));
          },
          close: function() {
            enableBodyScroll(document.querySelector('.popup-youtube'));
          }
        }
      });
    });
  }
})();
