import './overlay-menu.scss';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

const menu = document.querySelector('.overlay-menu');
const menuBG = document.querySelector('.overlay-menu-bg');
const openBtn = document.querySelectorAll('.overlay-menu-openbtn');
const nestedMenuItems = menu.querySelectorAll(
  '.overlay-menu__nav .menu-item-has-children>a'
);

export const openMenu = target => {
  target.className = 'fa fa-times';
  menu.dataset.show = 'true';
  $(menuBG).fadeIn();
  $(window).width() >= 1107 || disableBodyScroll(menu);
  menuBG.addEventListener('click', e => {
    closeMenu(target);
  });
};

export const closeMenu = target => {
  target.className = 'fa fa-bars';
  menu.dataset.show = 'false';
  $(menuBG).fadeOut();
  enableBodyScroll(menu);
  menuBG.removeEventListener('click', e => {
    closeMenu(target);
  });
};

if ($('.overlay-menu').length > 0) {
  $.each(nestedMenuItems, function() {
    this.onclick = e => {
      const submenu = $(this).next('ul');

      e.preventDefault();
      $(this).toggleClass('rotate');
      submenu.toggleClass('show');
    };
  });

  $.each(openBtn, function() {
    this.onclick = e => {
      if (menu.dataset.show === 'true') {
        closeMenu(this.querySelector('i'));
      } else {
        openMenu(this.querySelector('i'));
      }
    };
  });
  $(window).resize(() => {
    $.each(openBtn, function() {
      closeMenu(this.querySelector('i'));
    });
  });
}
