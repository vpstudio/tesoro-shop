import './woocommerce-single.scss';
import 'jquery-zoom/jquery.zoom';

const shop_attributes_table =
  document.querySelector('.shop_attributes') || false;
const link =
  document.querySelector('.woocommerce-single-product-hover__link') || false;

const price =
  document.querySelector(
    '.woocommerce div.product .ts-single-wrapper .price'
  ) || false;

if (price) {
  const oldPrice =
    price.querySelector('del .woocommerce-Price-amount') || false;
  const salePrice =
    price.querySelector('ins .woocommerce-Price-amount') || false;

  if (salePrice) {
    let oldPriceVal = oldPrice.childNodes[0].nodeValue.replace(/[.,\s]/g, '');
    oldPriceVal = parseInt(oldPriceVal);
    let salePriceVal = salePrice.childNodes[0].nodeValue.replace(/[.,\s]/g, '');
    salePriceVal = parseInt(salePriceVal);
    const difValue = oldPriceVal - salePriceVal;
    const saleValue = (difValue / oldPriceVal) * 100;
    const saleRate = document.createElement('span');
    saleRate.classList.add('sale-rate');
    saleRate.innerText = '-' + Math.round(saleValue) + '%';
    price.prepend(saleRate);

    const saleDelta = document.createElement('span');
    saleDelta.classList.add('sale-delta');
    saleDelta.innerText = 'Экономия ' + Math.round(difValue) + ' грн';
    price.append(saleDelta);
  }
}

const insertAfter = (newNode, referenceNode) => {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
};

const moveProductNodesOnMobileScreen = () => {
  const productTitle =
    document.querySelector('.single.woocommerce .product_title.entry-title') ||
    false;
  const productAttrs =
    document.querySelector(
      '.single.woocommerce .woocommerce-single-product-hover__attributes'
    ) || false;
  if (!productTitle) return;
  const productWrapper = document.querySelector('.single.woocommerce .product');
  const productSummary = document.querySelector(
    '.single.woocommerce .product .summary.entry-summary'
  );

  if (window.innerWidth <= 768) {
    productTitle.remove();
    productWrapper.prepend(productTitle);
    productAttrs.remove();
    productSummary.append(productAttrs);
  } else {
    document
      .querySelector('.single.woocommerce .product .product_title.entry-title')
      .remove();
    productSummary.prepend(productTitle);
    insertAfter(productAttrs, productTitle);
  }
};

moveProductNodesOnMobileScreen();

const moveAddToWishlistAfterToCartButton = () => {
  const wishlist = document.querySelector(
    '.single.woocommerce div.product form.cart .tinv-wraper.woocommerce.tinv-wishlist'
  );
  const addToCartWrapper = document.querySelector(
    '.single.woocommerce div.product form.cart'
  );
  if (!wishlist) return;

  wishlist.remove();
  $(addToCartWrapper).after(wishlist);
};
moveAddToWishlistAfterToCartButton();

var resizeId;
$(window).resize(function() {
  clearTimeout(resizeId);
  resizeId = setTimeout(moveProductNodesOnMobileScreen, 500);
});

const showAttributes = () => {
  const attr_titles = document.querySelectorAll('.tabs.wc-tabs li');
  const attr_setions = document.querySelectorAll('.woocommerce-Tabs-panel');

  $(attr_titles).each(function(i, val) {
    $(this).removeClass('active');
    if ($(this).hasClass('additional_information_tab')) {
      $(this).addClass('active');
    }
  });
  $(attr_setions).each(function(i, val) {
    $(this).css('display', 'none');
    if ($(this).hasClass('woocommerce-Tabs-panel--additional_information')) {
      $(this).css('display', 'block');
    }
  });
};
if (link) {
  link.addEventListener('click', e => {
    e.preventDefault();
    showAttributes();
    $('html, body').animate(
      {
        scrollTop: $('#tab-additional_information').offset().top - 100
      },
      1000
    );
  });
}

if (shop_attributes_table) {
  const shop_attributes = shop_attributes_table.querySelectorAll('tr');
  if (shop_attributes.length > 3) {
    link.style.display = 'block';
  }
}

$('.woocommerce-product-gallery__image a').zoom();

if (document.querySelector('.get-price-button')) {
  $('.get-price-button').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#get-pirce-email',
    closeOnBgClick: true,
    fixedContentPos: false,
    removalDelay: 300,
    mainClass: 'mfp-fade',
    callbacks: {
      beforeOpen: function() {
        if ($(window).width() < 700) {
          this.st.focus = false;
        } else {
          this.st.focus = '#get-pirce-email';
        }
      }
    }
  });

  $('#get-price-submit').click(function() {
    var data = {
      action: 'get_price',
      email: $('#get-pirce-email').val(),
      product_type: $('#ts-product-type').val(),
      product_id:
        $('input[name="variation_id"]').val() || $('#ts-product-id').val()
    };
    $.ajax({
      url: tsajax.url,
      type: 'POST',
      data: data,
      beforeSend: xhr => {
        $('#get-price-submit').val('Отправка...');
      },
      success: function(data) {
        $('#get-price-submit').val('Отправить');
        alert(data);
        $('#get-price-form form').trigger('reset');
        $('#get-price-form').magnificPopup('close');
      }
    });
    return false;
  });
}
