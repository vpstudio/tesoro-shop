import './index.scss';
import './components/fw-typography.scss';
import './components/inputs.scss';
import './components/tables.scss';
import './components/page.scss';
import './components/header.scss';
import './components/popup.scss';
import './components/notice.scss';
import './components/links.scss';
import './components/section.scss';
import './components/button.scss';
import './components/slider.scss';
import './components/rbPages.scss';
import './components/MobileHeader';
import './components/TabletHeader';
import './components/DesktopHeader';
import './components/NavVList';
import './components/TopLine';
import './components/CatalogNav';
import './components/StickyNav';
import './components/OverlayMenu';
import './components/Mailing';
// import './components/SearchForm';
import './components/Footer';

import './components/WoocommerceCart';
import './components/CallbackBlock';
import './components/ProductItem';
import './components/BrandsAll';
import './components/2colInfoDark';
import './components/BrandsSingleInfo';
import './components/GallerySlider';
import './components/ImageSlider';
import './components/Video';
import './components/TextImageRight';
import './components/TextImageLeft';

import './components/RBCollections';
import './components/RBGrid';
// import './components/UnysonAccordion';

/**
 * Main page modules
 */

import './components/PromoSlider';
import './components/ProductSlider';
import './components/Categories';
import './components/Brands';
import './components/About';
import './components/FeachersBlock';
import './components/FeachersBlockLine';
import './components/SectionNews';

import './components/Comments';
import './components/Instagram';

/**
 * Catalog page modules
 */

import './components/Sidebar';
import './components/Breadcrumbs';
import './components/WoocommerceCatalog';

import './components/WoocommerceProduct';
import './components/WoocommerceSingle';
import './components/WoocommerceProductBoxes';
import './components/WoocommerceBundle';
import './components/WoocommerceComments';
import './components/WoocommerceWishlist';
import './components/WoocommerceAccount';
import './components/WoocommerceCheckout';

import './components/News';
import './components/SingleNew';
import './components/NewsComments';
import './components/ContactForm';
import './components/Map';

import './components/PageNotFound';
