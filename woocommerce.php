<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package customify
 */

get_header(); 
    
    if ( is_single() ) { ?>
        <div class="container">
            <div class="content-inner">
                <div class="row">
                    <div class="col"><?php woocommerce_breadcrumb(); ?></div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="catalog">
                        <?php woocommerce_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #.content-inner -->
    <?php } elseif ( is_tax('brand') ) {
        get_template_part( 'page-brands-single' );
    } else { ?>
        <div class="container">
            <div class="content-inner">
                <div class="row">
                    <div class="col"><?php woocommerce_breadcrumb(); ?></div>
                </div>
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <h1 class="section__title"><?php echo woocommerce_page_title();?></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 sidebar-col">
                        <div class="sidebar">
                            <div class="sidebar__title"><?php _e('Фильтры товаров', 'tesoro-shop'); ?></div>
                            <?php get_template_part( 'sidebar', 'shop' ); ?>
                            <div class="sidebar-nav">
                            <?php get_template_part( 'sidebar', 'nav' ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div id="mobile-sidebar" class="sidebar sidebar--mobile">
                            <div class="sidebar-content">
                                <div class="sidebar__title"><?php _e('Фильтры товаров', 'tesoro-shop'); ?></div>
                                <a class="sidebar__close-btn" rel="noopener noreferrer" href="#"><i class="fa fa-times"></i></a>
                                <div class="sidebar-container">
                                    <?php get_template_part( 'sidebar', 'shop' ); ?>
                                </div>
                                <div class="sidebar-nav">
                                <?php get_template_part( 'sidebar', 'nav' ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="catalog">
                            <?php woocommerce_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #.content-inner -->
    <?php }

get_footer();
