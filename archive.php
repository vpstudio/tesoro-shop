<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tesoro-shop
 */

$soc_instagram_link =  !empty(fw_get_db_settings_option('soc_instagram')) ? basename(fw_get_db_settings_option('soc_instagram')) : null;
get_header();
?>
<div class="container">
	<div class="row">
		<div class="col"><?php woocommerce_breadcrumb(); ?></div>
	</div>
	<div class="row">
		<div class="col d-flex justify-content-center">
			<h1 class="section__title"><?php wp_title("");?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col col-xl-9">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
					<div id="posts-loop" class="row">

						<?php while ( have_posts() ) : the_post(); 
						$categories = get_the_category($post->ID); ?>
						<div class="col-12 col-lg-6 col-xl-4">
							<article id="post-<?php the_ID(); ?>" class="post">
								<a href="<?php echo get_permalink($post->ID); ?>" class="post__image-wrapper">
									<div class="post__image" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'medium'); ?>);"></div>
								</a>
								<a href="<?php echo get_permalink($post->ID); ?>"><h3 class="post__title"><?php echo $post->post_title; ?></h3></a>
								<div class="post__data">
									<time date="<?php echo $post->post_date; ?>" class="post__date"><?php echo get_the_date('d M Y', $post->ID); ?></time>
									<?php if (!empty($categories)) { echo "|";
									foreach ($categories as $category ) { ?>
										<a class="post__cat" href="<?php echo get_category_link( $category->term_id ); ?>" ><?php echo $category->name; ?></a>
									<?php } 
									}?>
								</div>
								<div class="post__excerpt">
									<?php echo wp_trim_words( get_the_content(), 25, '...' ); ?>
								</div>
							</article >
						</div>
						<?php endwhile; // End of the loop. ?>
					</div>
					<div class="row">
						<div class="col d-flex justify-content-center">
						<?php if (  $wp_query->max_num_pages > 1 ) : ?>
							<script>
								var ts_posts = '<?php echo serialize($wp_query->query_vars); ?>';
								var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
								var max_pages = '<?php echo $wp_query->max_num_pages; ?>';
							</script>
							<a href="javascript:void(0)" id="loadmore" class="button alt"><?php _e('Показать больше', 'tesoro-shop'); ?></a>
						<?php endif; ?>
						</div>
					</div>
				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
		<div class="col-3 d-none d-xl-block"><?php get_sidebar(); ?></div>
	</div>
</div>

<?php
	if (!empty($soc_instagram_link)) { ?>
		<!-- instagram -->
		<section class="section section--instagram">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2 class="section__title">Instagram</h2> <a href="https://www.instagram.com/<?php echo $soc_instagram_link; ?>" target="_blank" class="instagram-username">@<?php echo $soc_instagram_link; ?></a>
						<span class="section__subtitle"><?php _e('Подписывайся на наш канал в Instagram!', 'tesoro-shop'); ?></span>
					</div>
				</div>
			</div> 
			<?php echo do_shortcode('[iscwp-slider slidestoshow="5" show_comments_count="false" show_likes_count="false" popup="false" dots="false" autoplay="false" username="'. $soc_instagram_link .'"]'); ?>
		</section>
		<!-- #instagram -->
	<?php } 

get_footer();
