<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['grid.columns'] = array(
	'1_6' => array(
		'title'          => '1/6',
		'backend_class'  => 'col-sm-2',
		'frontend_class' => 'col-xs-12 col-sm-2',
	),
	'1_5' => array(
		'title'          => '1/5',
		'backend_class'  => 'col-sm-15',
		'frontend_class' => 'col-xs-12 col-sm-15',
	),
	'1_4' => array(
		'title'          => '1/4',
		'backend_class'  => 'col-sm-3',
		'frontend_class' => 'col-xs-12 col-sm-3',
	),
	'1_3' => array(
		'title'          => '1/3',
		'backend_class'  => 'col-sm-4',
		'frontend_class' => 'col-xs-12 col-sm-4',
	),
	'1_2' => array(
		'title'          => '1/2',
		'backend_class'  => 'col-sm-6',
		'frontend_class' => 'col-xs-12 col-sm-6',
	),
	'2_3' => array(
		'title'          => '2/3',
		'backend_class'  => 'col-sm-8',
		'frontend_class' => 'col-xs-12 col-sm-8',
	),
	'3_4' => array(
		'title'          => '3/4',
		'backend_class'  => 'col-sm-9',
		'frontend_class' => 'col-xs-12 col-sm-9',
	),
	'1_1' => array(
		'title'          => '1/1',
		'backend_class'  => 'col-sm-12',
		'frontend_class' => 'col-12',
	),
);

/**
 * @since 1.2.0
 */
$cfg['grid.row.class'] = 'row';

$cfg['grid.container.class'] = 'container';

$cfg['default_item_widths'] = array(
    /**
     * Copy/Paste here default columns https://github.com/ThemeFuse/Unyson-Builder-Extension/blob/master/config.php
     * and add, remove or change them
     */

);