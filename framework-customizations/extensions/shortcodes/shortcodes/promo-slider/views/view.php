<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

$promo = $atts['promo'];

if ( empty( $promo ) ) {
	return;
} ?>

<!-- section-promo -->
<section class="section section--promo">
	<!-- promo-slider -->
	<div class="promo-slider owl-carousel owl-theme">
		<?php foreach ($promo as $item) : ?>
			<?php
				$title = $item['promo_title'];
				$link = !empty($item['promo_link']) ? 'href="'. $item['promo_link'] .'"' : '';
				$image_fallback = !empty($item['promo_image_fallback']) ? $item['promo_image_fallback']['url'] : '';
				$image_super_high = !empty($item['promo_image_super_high']) ? $item['promo_image_super_high']['url'] : '';
				$image_high = !empty($item['promo_image_high']) ? $item['promo_image_high']['url'] : '';
				$image_medium = !empty($item['promo_image_medium']) ? $item['promo_image_medium']['url'] : '';
			?>
			
			<a <?php echo $link; ?> title="<?php echo $item['promo_title']; ?>" class="promo-slider__slide">
				<picture>
					<source media="(min-width: 1366px)" srcset="<?php echo $image_super_high; ?>">
					<source media="(min-width: 768px)" srcset="<?php echo $image_high; ?>">
					<source media="(min-width: 445px)" srcset="<?php echo $image_medium; ?>">
					<img src="<?php echo $image_fallback; ?>" alt="<?php echo $item['promo_title']; ?>">
				</picture>
			</a>
		
		<?php endforeach; ?>
	</div>
	<!-- #promo-slider -->
</section>
<!-- #section-promo -->

