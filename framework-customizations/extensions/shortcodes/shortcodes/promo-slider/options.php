<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'promo' => array(
		'type'  => 'addable-popup',
		'label' => __('Слайды', 'tesoro-shop'),
		'popup-title' => __('Новый слайд', 'tesoro-shop'),
		'size' => 'large', // small, medium, large
		'limit' => 7, // limit the number of popup`s that can be added
		'add-button-text' => __('Добавить', 'tesoro-shop'),
		'sortable' => true,
		'template' => '{{- promo_title }}',
		'popup-options' => array(
			'promo_title' => array(
				'label' => __('Заголовок', 'tesoro-shop'),
				'type' => 'text',
			),
			'promo_link' => array(
				'label' => __('Ссылка', 'tesoro-shop'),
				'desc'  => __('Не обязательно', 'tesoro-shop'),
				'type' => 'text',
			),
			'promo_image_fallback' => array(
				'label' => __('Изображение по-умолчанию', 'tesoro-shop'),
				'type' => 'upload',
				'images_only' => true,
				'files_ext' => array('png', 'jpg', 'jpeg'),
			),
			'promo_image_super_high' => array(
				'label' => __('Изображение очень большое', 'tesoro-shop'),
				'type' => 'upload',
				'images_only' => true,
				'files_ext' => array('png', 'jpg', 'jpeg'),
			),
			'promo_image_high' => array(
				'label' => __('Изображение большое', 'tesoro-shop'),
				'type' => 'upload',
				'images_only' => true,
				'files_ext' => array('png', 'jpg', 'jpeg'),
			),
			'promo_image_medium' => array(
				'label' => __('Изображение среднее', 'tesoro-shop'),
				'type' => 'upload',
				'images_only' => true,
				'files_ext' => array('png', 'jpg', 'jpeg'),
			),			
		),
	),
);

