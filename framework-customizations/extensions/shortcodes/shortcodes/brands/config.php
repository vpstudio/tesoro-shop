<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Brands', 'fw'),
	'description'   => __('Add a Brands', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
	'popup_size'    => 'large',
);