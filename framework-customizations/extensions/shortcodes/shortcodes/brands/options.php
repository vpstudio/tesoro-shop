<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'brands_title' => array(
		'type' => 'text',
		'label' => __('Заголовок', 'tesoro-shop'),
	),
	'brands_text' => array(
		'type' => 'wp-editor',
		'size' => 'large', // small, 
		'label' => __('Текст', 'tesoro-shop'),
	),
	'brands_link' => array(
		'type' => 'text',
		'label' => __('Ссылка на страницу', 'tesoro-shop'),
	),
);

