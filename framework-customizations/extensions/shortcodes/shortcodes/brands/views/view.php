<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

$args = array(
    'taxonomy'   => 'brand',
    'hide_empty' => false,
);

$brands_list = get_terms( $args );

if ( empty( $brands_list ) ) {
	return;
}

$brands_title = $atts['brands_title'];
$brands_link = $atts['brands_link'];
$brands_text = $atts['brands_text'];
?>

<!-- brands -->
<section class="section section-brands">
    <div class="section__title-wrapepr text-center">
        <h2 class="section__title"><?php echo $brands_title; ?></h2>
    </div>
    <div class="section__text"><?php echo $brands_text; ?></div>
    <div class="brands owl-carousel owl-theme">
        
        <?php foreach ( $brands_list as $brand ) { 
            $brand_vis = fw_get_db_term_option($brand->term_id, 'brand', 'hide_brand');
            if ( $brand_vis ) continue;
            $image = fw_get_db_term_option($brand->term_id, 'brand', 'brand_image');
            if ( $image != null ) {
                $image_url = $image['url'];
            } else {
                $image_url = '';
            }
        ?>
            <!-- brand -->
            <a href="<?php echo get_term_link($brand->term_id, 'brand'); ?>" rel="details" title="<?php echo $brand->name; ?>" class="brand">
                <img class="img-responsive" src="<?php echo $image_url; ?>" alt="<?php echo $brand->name; ?>" title="<?php echo $brand->name; ?>">
            </a>
            <!-- #brands -->
        <?php } ?>
        
    </div>
    <?php if ( !empty($brands_link) ) {
        echo '<div class="section__button-wrapper"><a href="'. $brands_link .'" class="button section__button">'. __('СМОТРЕТЬ ВСЕ БРЕНДЫ', 'tesoro-shop') .'</a></div>';
    }?>
</section>
<!-- #brands -->