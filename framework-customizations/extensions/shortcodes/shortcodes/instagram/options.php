<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'inst_username' => array(
		'type'  => 'text',
		'label' => __('Instagram username', 'tesoro-shop'),
	),
);

