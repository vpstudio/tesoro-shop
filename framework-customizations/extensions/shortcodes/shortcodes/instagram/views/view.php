<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['inst_username'] ) ) {
	return;
}

$inst_username = $atts['inst_username'];

?>

<!-- instagram -->
<section class="section section--instagram">
  <div class="container">
    <div class="row">
        <div class="col-12">
          <h2 class="section__title">Instagram </h2> <a href="https://www.instagram.com/<?php echo $inst_username; ?>" rel="noopener noreferrer" target="_blank" class="instagram-username">  @<?php echo $inst_username; ?></a>
          <span class="section__subtitle"><?php _e('Подписывайся на наш аккаунт в Instagram!', 'tesoro-shop'); ?></span>
        </div>
    </div>
  </div> 
  <?php echo do_shortcode('[iscwp-slider slidestoshow="5" show_comments_count="false" show_likes_count="false" popup="false" dots="false" autoplay="true" username="'. $inst_username .'"]'); ?>
</section>
<!-- #instagram -->