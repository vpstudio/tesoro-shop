<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
      'text_with_img_switch' => array(
        'type'  => 'switch',
        'value' => 'left',
        'label' => __('Изображение', 'tesoro-shop'),
        'left-choice' => array(
          'value' => 'left',
          'label' => __('Слева', 'tesoro-shop'),
        ),
        'right-choice' => array(
          'value' => 'right',
          'label' => __('Справа', 'tesoro-shop'),
        ),
      ),
      'text_with_img_title' => array(
        'type'  => 'text',
        'value' => 'default value',
        'label' => __('Заголовок', 'tesoro-shop'),
      ),
      'text_with_img_text' => array(
        'type'  => 'wp-editor',
        'label' => __('Текст', 'tesoro-shop'),
        'size' => 'small',
        'editor_height' => 400,
        'wpautop' => true,
        'editor_type' => 'tinymce',
        'shortcodes' => false,
      ),
      'text_with_img_image' => array(
        'type'  => 'upload',
        'value' => array(
            'attachment_id' => null,
            'url' => 'https://images.unsplash.com/photo-1540167828867-7e5020e0bc8e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6b75c91f2c2c552d78c93be1f87d6fc2&auto=format&fit=crop&w=500&q=60'
        ),
        'label' => __('Изображение', 'tesoro-shop'),
        'images_only' => true,
      ),
);

