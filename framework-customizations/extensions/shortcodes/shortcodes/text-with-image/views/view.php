<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['text_with_img_image'] ) ) {
	return;
}

$text_with_img_switch = $atts['text_with_img_switch'];
$text_with_img_title = $atts['text_with_img_title'];
$text_with_img_text = $atts['text_with_img_text'];
$text_with_img_image_id = !empty($atts['text_with_img_image']) ? $atts['text_with_img_image']['attachment_id'] : '';
$text_with_img_image_alt = get_post_meta($text_with_img_image_id, '_wp_attachment_image_alt', true);
$text_with_img_image_title = get_the_title($text_with_img_image_id);
$text_with_img_image = wp_get_attachment_image_url($text_with_img_image_id, 'full');

?>

<!-- text-image <?php echo $text_with_img_switch; ?>-->
<section class="section <?php echo 'section--text-image-' . $text_with_img_switch; ?>">
    <div class="container <?php echo 'text-image-' . $text_with_img_switch; ?>">
        <div class="row align-items-center">
            <div class="<?php if ($text_with_img_switch == 'right') echo 'order-md-2'; ?> col-md-6">
                <div class="<?php echo 'text-image-' . $text_with_img_switch . '__image'; ?>">
                <img class="img-responsive" src="<?php echo $text_with_img_image; ?>" title="<?php echo $text_with_img_image_title; ?>" alt="<?php echo $text_with_img_image_alt; ?>"></div>
            </div>
            <div class="col-md-6 <?php if ($text_with_img_switch == 'right') echo 'order-md-1'; ?>">
                <h6 class="<?php echo 'text-image-' . $text_with_img_switch . '__ttl'; ?>"><?php echo $text_with_img_title; ?></h6>
                <div class="<?php echo 'text-image-' . $text_with_img_switch . '__txt'; ?>"><?php echo $text_with_img_text; ?></div>
            </div>
        </div>
    </div>
</section>
<!-- #text-image <?php echo $text_with_img_switch; ?>-->