<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Text with Image', 'fw'),
	'description'   => __('Add an Text with Image', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
);