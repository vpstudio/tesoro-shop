<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Social Icons', 'fw'),
	'description'   => __('Add an Social Icons', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
);