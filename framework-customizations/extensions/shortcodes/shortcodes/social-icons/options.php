<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'social_icons' => array(
		'type'  => 'addable-box',
		'label' => __('Социальные ссылки', 'tesoro-shop'),
		'box-options' => array(
			'type' => array(
				'type'  => 'select',
				'label' => __('Тип', 'tesoro-shop'),
				'choices' => array(
					'link' => __('Ссылка', 'tesoro-shop'),
					'mail' => __('Почта', 'tesoro-shop'),
					'phone' => __('Телефон', 'tesoro-shop'),
				),
			),
			'title' => array( 'type' => 'text' ),
			'url' => array( 'type' => 'text' ),
			'icon' => array( 
				'type' => 'icon-v2',
				'preview_size' => 'small',
				'modal_size' => 'small',
			),
		),
		'template' => '{{- title }}', // box title
		'limit' => 4, // limit the number of boxes that can be added
		'add-button-text' => __('Добавить', 'tesoro-shop'),
		'sortable' => true,
	),

);