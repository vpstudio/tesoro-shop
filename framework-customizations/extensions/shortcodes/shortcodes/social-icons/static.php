<?php if (!defined('FW')) die('Forbidden');

wp_enqueue_style(
	'fw-shortcode-soc-icons',
	get_template_directory_uri() . '/framework-customizations/extensions/shortcodes/shortcodes/social-icons/static/css/styles.css'
);

