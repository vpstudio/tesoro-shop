<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$social_icons = $atts['social_icons'];
if ( empty( $social_icons ) ) {
  return;
} 
fw()->backend->option_type('icon-v2')->packs_loader->enqueue_frontend_css();
?>

<!-- social_icons -->
<div class="social-icons">
  <?php foreach ($social_icons as $social_icon ) { 
    switch ($social_icon['type']) {
      case 'phone':
        echo '<div><a href="tel:'. str_replace([' ', '(', ')', '+', '-'], '', $social_icon['url']) .'" title="'. $social_icon['title'] .'" rel="noopener noreferrer"><i class="'. $social_icon['icon']['icon-class'] .'"></i> '. $social_icon['title'] .'</a></div>';
        break;
      case 'mail':
        echo '<div><a href="mailto:'. $social_icon['url'] .'" title="'. $social_icon['title'] .'" rel="noopener noreferrer"><i class="'. $social_icon['icon']['icon-class'] .'"></i> '. $social_icon['title'] .'</a></div>';
        break;
      default:
        echo '<div><a href="'. $social_icon['url'] .'" title="'. $social_icon['title'] .'" target="_blank" rel="noopener noreferrer"><i class="'. $social_icon['icon']['icon-class'] .'"></i> '. $social_icon['title'] .'</a></div>';
        break;
    }
  } ?>
</div>
<!-- #social_icons -->