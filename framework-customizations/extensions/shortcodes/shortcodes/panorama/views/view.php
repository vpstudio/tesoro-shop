<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$shortcodes_extension = fw_ext('shortcodes');

?>
<style type="text/css">
	@-ms-viewport { width: device-width; }
	@media only screen and (min-device-width: 800px) { html { } }
	* { padding: 0; margin: 0; }
	html { height: 100%; }
	body { height: 100%; }
	div#container { height: 522px; width: 100%; margin: 0 auto; overflow:hidden; border: 2px solid #CCA300; padding: 15px; }
	@media (max-width: 768px) {
		div#container { height: 319px; padding: 5px; }
	}
	div#tourDIV {
		height:100%;
		position:relative;
		overflow:hidden;
	}
	div#panoDIV {
		height:100%;
		position:relative;
		overflow:hidden;
		-webkit-user-select: none;
		-khtml-user-select: none;
		-moz-user-select: none;
		-o-user-select: none;
		user-select: none;
	}
</style>
<style type="text/css">
	div#panoDIV.cursorMoveMode {
		cursor: move;
		cursor: url(<?php echo $shortcodes_extension->locate_URI('/shortcodes/panorama/static/Roberto_Bravodata/graphics/cursors_move_html5.cur'); ?>), move;
	}

	div#panoDIV.cursorDragMode {
		cursor: grab;
		cursor: -moz-grab;
		cursor: -webkit-grab;
		cursor: url(<?php echo $shortcodes_extension->locate_URI('/shortcodes/panorama/static/Roberto_Bravodata/graphics/cursors_drag_html5.cur'); ?>), default;
	}
</style>
<script type="text/javascript">
	function webglAvailable() {
		try {
			var canvas = document.createElement("canvas");
			return !!window.WebGLRenderingContext && (canvas.getContext("webgl") || canvas.getContext("experimental-webgl"));
		} catch (e) {
			return false;
		}
	}

	function getWmodeValue() {
		var webglTest = webglAvailable();
		if (webglTest) {
			return 'direct';
		}
		return 'opaque';
	}

	function readDeviceOrientation() {
		// window.innerHeight is not supported by IE
		var winH = window.innerHeight ? window.innerHeight : jQuery(window).height();
		var winW = window.innerWidth ? window.innerWidth : jQuery(window).width();
		//force height for iframe usage
		if (!winH || winH == 0) {
			winH = '100%';
		}
		// set the height of the document
		jQuery('html').css('height', winH);
		// scroll to top
		window.scrollTo(0, 0);
	}
	jQuery(document).ready(function () {
		if (
			/(iphone|ipod|ipad|android|iemobile|webos|fennec|blackberry|kindle|series60|playbook|opera\smini|opera\smobi|opera\stablet|symbianos|palmsource|palmos|blazer|windows\sce|windows\sphone|wp7|bolt|doris|dorothy|gobrowser|iris|maemo|minimo|netfront|semc-browser|skyfire|teashark|teleca|uzardweb|avantgo|docomo|kddi|ddipocket|polaris|eudoraweb|opwv|plink|plucker|pie|xiino|benq|playbook|bb|cricket|dell|bb10|nintendo|up.browser|playstation|tear|mib|obigo|midp|mobile|tablet)/
			.test(navigator.userAgent.toLowerCase())) {
			if (/iphone/.test(navigator.userAgent.toLowerCase()) && window.self === window.top) {
				jQuery('body').css('height', '100.18%');
			}
			// add event listener on resize event (for orientation change)
			if (window.addEventListener) {
				window.addEventListener("load", readDeviceOrientation);
				window.addEventListener("resize", readDeviceOrientation);
				window.addEventListener("orientationchange", readDeviceOrientation);
			}
			//initial execution
			setTimeout(function () {
				readDeviceOrientation();
			}, 10);
		}
	});

	function accessWebVr() {
		unloadPlayer();

		eventUnloadPlugins();

		setTimeout(function () {
			loadPlayer(true);
		}, 100);
	}

	function accessStdVr() {
		unloadPlayer();

		resetValuesForPlugins();

		setTimeout(function () {
			loadPlayer(false);
		}, 100);
	}

	function loadPlayer(isWebVr) {
		if (isWebVr) {
			embedpano({
				id: "krpanoSWFObject",
				xml: "<?php echo $shortcodes_extension->locate_URI('/shortcodes/panorama/static/Roberto_Bravodata/Roberto_Bravo_vr.xml'); ?>",
				target: "panoDIV",
				passQueryParameters: true,
				bgcolor: "#000000",
				html5: "only+webgl",
				vars: {
					skipintro: true,
					norotation: true
				}
			});
		} else {
			embedpano({
				id: "krpanoSWFObject"

					,
				swf: "<?php echo $shortcodes_extension->locate_URI('/shortcodes/panorama/static/Roberto_Bravodata/Roberto_Bravo.swf'); ?>"

					,
				target: "panoDIV",
				passQueryParameters: true,
				bgcolor: "#000000"

					,
				html5: "prefer"


					,
				wmode: getWmodeValue()

			});
		}
		//apply focus on the visit if not embedded into an iframe
		if (top.location === self.location) {
			kpanotour.Focus.applyFocus();
		}
	}

	function unloadPlayer() {
		if (jQuery('#krpanoSWFObject')) {
			removepano('krpanoSWFObject');
		}
	}

	function isVRModeRequested() {
		var querystr = window.location.search.substring(1);
		var params = querystr.split('&');
		for (var i = 0; i < params.length; i++) {
			if (params[i].toLowerCase() == "vr") {
				return true;
			}
		}
		return false;
	}
</script>

<section class="section section--panorama">
	<div class="row">
		<div class="col">
			<div id="container">
				<div id="tourDIV">
					<div id="panoDIV">
						<noscript>

							<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="100%" height="100%" id="Roberto_Bravodata/Roberto_Bravo">
								<param name="movie" value="<?php echo $shortcodes_extension->locate_URI('/shortcodes/panorama/static/Roberto_Bravodata/Roberto_Bravo.swf'); ?>" />
								<param name="allowFullScreen" value="true" />
								<!--[if !IE]>-->
								<object type="application/x-shockwave-flash" data="Roberto_Bravodata/Roberto_Bravo.swf" width="100%" height="100%">
									<param name="movie" value="<?php echo $shortcodes_extension->locate_URI('/shortcodes/panorama/static/Roberto_Bravodata/Roberto_Bravo.swf'); ?>" />
									<param name="allowFullScreen" value="true" />
									<!--<![endif]-->
									<a href="http://www.adobe.com/go/getflash">
										<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player to visualize the Virtual Tour : Roberto_Bravo (Virtual tour generated by Panotour)" />
									</a>
									<!--[if !IE]>-->
								</object>
								<!--<![endif]-->
							</object>

						</noscript>
					</div>

					<script type="text/javascript" src="<?php echo $shortcodes_extension->locate_URI('/shortcodes/panorama/static/Roberto_Bravodata/Roberto_Bravo.js'); ?>"></script>
					<script type="text/javascript">
						if (isVRModeRequested()) {
							accessWebVr();
						} else {
							accessStdVr();
						}
					</script>
				</div>
			</div>
		</div>
	</div>
</section>
