<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Panorama', 'fw' ),
	'description' => __( 'Add a Panorama Box', 'fw' ),
	'tab'         => __( 'Tesoro', 'fw' ),
);