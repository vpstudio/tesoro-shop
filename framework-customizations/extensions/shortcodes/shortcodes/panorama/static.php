<?php if (!defined('FW')) die('Forbidden');

$shortcodes_extension = fw_ext('shortcodes');
wp_enqueue_style(
	'fw-shortcode-panorama-jquery-ui-style',
	$shortcodes_extension->locate_URI( '/shortcodes/panorama/static/Roberto_Bravodata/lib/jquery-ui-1.11.1/jquery-ui.min.css' )
);
wp_enqueue_script(
	'fw-shortcode-panorama-jquery-ui-srcipts',
	$shortcodes_extension->locate_URI('/shortcodes/panorama/static/Roberto_Bravodata/lib/jquery-ui-1.11.1/jquery-ui.min.js'),
	array('jquery'),
	fw()->manifest->get_version(),
	false
);
wp_enqueue_script(
	'fw-shortcode-panorama-touch-punch',
	$shortcodes_extension->locate_URI('/shortcodes/panorama/static/Roberto_Bravodata/lib/jquery.ui.touch-punch.min.js'),
	array('jquery','fw-shortcode-panorama-jquery-ui-srcipts'),
	fw()->manifest->get_version(),
	false
);
wp_enqueue_script(
	'fw-shortcode-panorama-kolor',
	$shortcodes_extension->locate_URI('/shortcodes/panorama/static/Roberto_Bravodata/lib/Kolor/KolorTools.min.js'),
	array('jquery','fw-shortcode-panorama-jquery-ui-srcipts','fw-shortcode-panorama-touch-punch'),
	fw()->manifest->get_version(),
	false
);
wp_enqueue_script(
	'fw-shortcode-panorama-kolor-bootstrap',
	$shortcodes_extension->locate_URI('/shortcodes/panorama/static/Roberto_Bravodata/graphics/KolorBootstrap.js'),
	array('jquery','fw-shortcode-panorama-jquery-ui-srcipts','fw-shortcode-panorama-touch-punch','fw-shortcode-panorama-kolor'),
	fw()->manifest->get_version(),
	false
);
