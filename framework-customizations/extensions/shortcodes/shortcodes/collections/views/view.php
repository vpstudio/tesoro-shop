<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$collections = $atts['collections'];
$collection_title = $atts['collections_title'];
if ( empty($collections)) {
    return;
} ?>

<section class="section section--rb-collections">

    <?php if (!empty($collection_title)) : ?>
    <div class="col d-flex justify-content-center">
        <h1 class="section__title"><?php echo $collection_title; ?></h1>
    </div>
    <?php endif; ?>
    <div class="rb-collections">
    <?php foreach( $collections as $collection ) : 
        $collection_image = $collection['image'];
    
    ?>
      <a href="<?php echo $collection['url']; ?>" class="rb-collections__item">
        <div class="rb-collections__image">
          <img src="<?php echo !empty($collection_image) ? $collection_image['url'] : ''; ?>" alt="<?php echo $collection['title']; ?>">
          <div class="rb-collections__overlay"></div>
        </div>
        <h6><?php echo $collection['title']; ?></h6>
      </a>
    <?php endforeach; ?>
    </div>

</section>