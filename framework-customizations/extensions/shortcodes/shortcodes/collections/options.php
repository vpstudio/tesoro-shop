<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'collections_title' => array(
		'type' => 'text',
		'label' => __('Заголовок', 'tesoro-shop'),
	),
	'collections' => array(
		'type'  => 'addable-box',
		'label' => __('Коллекции', 'tesoro-shop'),
		'box-options' => array(
			'title' => array( 'type' => 'text' ),
			'url' => array( 'type' => 'text' ),
			'image' => array( 'type' => 'upload' ),
		),
		'template' => '{{- title }}', // box title
		'add-button-text' => __('Добавить', 'tesoro-shop'),
		'sortable' => true,
	)
);

