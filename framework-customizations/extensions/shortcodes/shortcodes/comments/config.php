<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Comments', 'fw'),
	'description'   => __('Add a comments', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
);