<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['comments_list'] ) ) {
	return;
}

$comments_title = $atts['comments_title'];
$comments_list = $atts['comments_list'];

?>
<!-- section-comments -->
<section class="section section-comments owl-lazy" >
	<div class="container">
	<div class="row">
		<div class="col">
			<div class="section__title-wrapper text-center">
				<h2 class="section__title"><?php echo $comments_title; ?></h2>
			</div>
		</div>
	</div>
	<div class="row">
		<!-- comments-slider -->
		<div class="comments-slider owl-carousel owl-theme">

			<?php foreach ($comments_list as $comment) { 
				$product_image = !empty($comment['product_image']) ? wp_get_attachment_image_url($comment['product_image']['attachment_id'], 'thumbnail'): '';
				$product_image_alt = !empty($comment['product_image']) ? get_post_meta($comment['product_image']['attachment_id'], '_wp_attachment_image_alt', true): '';
				$product_image_title = !empty($comment['product_image']) ? get_the_title($comment['product_image']['attachment_id']): '';
				$count = $comment['product_rating'];
				$show_raiting = $count > 0 ? 'visible' : 'hidden';
				$rating = ($count / 5) * 100;
			?>
			<!-- comments-slide -->
			<div class="comments-slider__slide">
				<div class="comments-slider__product-data">
					<a href="<?php echo $comment['product_link']; ?>"><img src="<?php echo $product_image; ?>" title="<?php echo $product_image_title; ?>" alt="<?php echo $product_image_alt; ?>" class="comments-slider__product-image"> <?php echo $comment['product_title']; ?> </a>
					<div class="comments-slider__product-raiting-wrapper">
						<div class="comments-slider__product-raiting" style="visibility: <?php echo $show_raiting; ?>">
								<span style="width: <?php echo $rating; ?>%;"></span>
							</div>
						</div>
					<span class="comments-slider__autor"><?php echo $comment['comment_autor']; ?></span>
				</div>
				<div class="comments-slider__comment-text">
					<p><?php echo $comment['comment_text']; ?></p>
				</div>
			</div>
			<!-- #comments-slide -->
			<?php } ?>
			
		</div>
		<!-- #comments-slider -->
	</div>
	</div>
</section>
<!-- #section-comments -->
