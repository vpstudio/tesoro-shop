<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'comments_title' => array(
		'label' => __('Заголовок', 'tesoro-shop'),
		'desc' => __('Заголовок секции', 'tesoro-shop'),
		'type' => 'text',
	),
	'comments_list' => array(
		'type' => 'addable-popup',
			'label' => __('Выбранные комментарии', 'tesoro-shop'),
			'desc' => __('Список комментариев выводимых в секции', 'tesoro-shop'),
		'template' => '{{- comment_autor }}',
		'popup-title' => __('Комментарий', 'tesoro-shop'),
		'size' => 'small', // small, medium, large
		'limit' => 10, // limit the number of popup`s that can be added
		'add-button-text' => __('Добавить', 'tesoro-shop'),
		'sortable' => true,
		'popup-options' => array(
			'comment_autor' => array(
				'label' => __('Имя автора комментария', 'tesoro-shop'),
				'type' => 'text',
			),
			'comment_text' => array(
				'label' => __('Текст комментария', 'tesoro-shop'),
				'type' => 'textarea',
					),
					'product_title' => array(
						'label' => __('Название товара', 'tesoro-shop'),
						'type' => 'text',
					),
					'product_image' => array(
						'label' => __('Изображения товара', 'tesoro-shop'),
						'type' => 'upload',
						'images_only' => true,
						'files_ext' => array( 'png', 'jpg', 'jpeg' ),
					),
					'product_rating' => array(
						'label' => __('Рейтинг товара', 'tesoro-shop'),
						'desc' => __('Рейтинг товара оставленный автором комментария', 'tesoro-shop'),
						'type' => 'slider',
						'properties' => array(
							'min' => 0,
							'max' => 5,
							'step' => 1,
						),
					),
					'product_link' => array(
						'label' => __('Ссылка на товар', 'tesoro-shop'),
						'type' => 'text',
					),
    	),
	),
);

