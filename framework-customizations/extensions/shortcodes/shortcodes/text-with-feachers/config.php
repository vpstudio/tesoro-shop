<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Text with feachers', 'fw'),
	'description'   => __('Add an Text with feachers', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
	'popup_size'    => 'large',
);