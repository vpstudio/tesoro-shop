<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'feachers_block_title' => array(
		'type'  => 'text',
		'label' => __('Заголовок', 'tesoro-shop'),
	),
	'feachers_block_text' => array(
		'type'  => 'wp-editor',
		'size' => 'large', // small, 
		'label' => __('Текст', 'tesoro-shop'),
	),
	'feachers_block_feachers' => array(
    'type'  => 'addable-box',
    'label' => __('Особенности', 'tesoro-shop'),
    'box-options' => array(
        'feachers_title' => array( 
					'type' => 'text',
					'label' => __('Заголовок', 'tesoro-shop'),
				 ),
        'feachers_text' => array( 
					'type' => 'textarea',
					'label' => __('Текст', 'tesoro-shop'),
				 ),
        'feachers_icon' => array( 
					'type' => 'upload',
					'label' => __('Иконка', 'tesoro-shop'),
					'files_ext' => array( 'png', 'jpg', 'jpeg', 'svg' ),
				 ),
    ),
    'template' => '{{- feachers_title }}',
    'limit' => 6,
    'add-button-text' => __('Add', 'tesoro-shop'),
    'sortable' => true,
	),
);

