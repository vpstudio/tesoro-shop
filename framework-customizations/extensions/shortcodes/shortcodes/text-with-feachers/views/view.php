<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['feachers_block_text'] ) ) {
	return;
}

$feachers_block_title = $atts['feachers_block_title'];
$feachers_block_text = $atts['feachers_block_text'];
$feachers_block_feachers = $atts['feachers_block_feachers'];

?>

<!-- feachers_block -->
<section class="section section--feachers-block">
	<div class="row">
		<div class="col">
			<h2 class="section__title" ><?php echo $feachers_block_title; ?></h2>
			<div class="section__text"><?php echo $feachers_block_text; ?></div>
			<div class="feachers-list">
				<?php foreach ( $feachers_block_feachers as $feacher ) { 
					$feachers_title = $feacher['feachers_title'];
					$feachers_text = $feacher['feachers_text'];
					$feacher_image = $feacher['feachers_icon'];
					if ($feacher_image != null) {
						$feacher_image_url = $feacher_image['url'];
					} else {
						$feacher_image_url = '';
					}
				?>
				<div class="feachers-list__item">
					<div class="feachers-list__icon-wrapper">
						<img class="feachers-list__icon" src="<?php echo $feacher_image_url; ?>" title="<?php echo $feachers_title; ?>" alt="<?php echo $feachers_title; ?>">
					</div>
					<h3 class="feachers-list__title"><?php echo $feachers_title; ?></h3>
					<p class="feachers-list__text"><?php echo $feachers_text; ?></p>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<!-- #feachers_block -->