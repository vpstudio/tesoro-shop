<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'new_products' => array(
		'type'  => 'addable-option',
		'label' => __('Новинки', 'tesoro-shop'),
		'desc'  => __('Скоприруйте SLUG товара (указан в разделе "Постоянная ссылка" на странице товара сразу после product/)', 'tesoro-shop'),
		'option' => array( 'type' => 'text' ),
		'add-button-text' => __('Добавить', 'tesoro-shop'),
		'sortable' => true,
	),
	'sale_products' => array(
		'type'  => 'addable-option',
		'label' => __('Скидки', 'tesoro-shop'),
		'desc'  => __('Скоприруйте SLUG товара (указан в разделе "Постоянная ссылка" на странице товара сразу после product/)', 'tesoro-shop'),
		'option' => array( 'type' => 'text' ),
		'add-button-text' => __('Добавить', 'tesoro-shop'),
		'sortable' => true,
	),
	'popular_products' => array(
		'type'  => 'addable-option',
		'label' => __('Хиты', 'tesoro-shop'),
		'desc'  => __('Скоприруйте SLUG товара (указан в разделе "Постоянная ссылка" на странице товара сразу после product/)', 'tesoro-shop'),
		'option' => array( 'type' => 'text' ),
		'add-button-text' => __('Добавить', 'tesoro-shop'),
		'sortable' => true,
	),
	'catalog_link' => array(
		'type'  => 'text',
		'label' => __('URL кнопки', 'tesoro-shop'),
	),
);

