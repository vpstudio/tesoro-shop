<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Products slider', 'fw'),
	'description'   => __('Add a Products slider', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
	'popup_size'    => 'large',
);