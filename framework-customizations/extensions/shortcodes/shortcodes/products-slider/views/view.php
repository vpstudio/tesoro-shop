<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$recent_products_ids = array();
$popular_products_ids = array();
$sale_products_ids = array();
$recent_products = array();
$popular_products = array();
$sale_products = array();

$glob_sale_products_titles = ts_remove_empty(fw_get_db_settings_option('glob_sale_products'));
$sale_products_titles = ts_remove_empty($atts['sale_products']);

$glob_new_products_titles = ts_remove_empty(fw_get_db_settings_option('glob_new_products'));
$new_products_titles = ts_remove_empty($atts['new_products']);

$glob_popular_products_titles = ts_remove_empty(fw_get_db_settings_option('glob_popular_products'));
$popular_products_titles = ts_remove_empty($atts['popular_products']);

if ( !empty($sale_products_titles) ) {
    $sale_products_ids = get_wc_products_ids_by_titles($atts['sale_products']);
    $sale_products = wc_get_products( array(
        'status'   => 'publish',
        'order'	   => 'DESC',
        'include' => $sale_products_ids,
    ));
} else {
    if ( !empty(fw_get_db_settings_option('glob_sale_products')) ) {
        $sale_products_ids = get_wc_products_ids_by_titles($glob_sale_products_titles);
        $sale_products = wc_get_products( array(
            'status'   => 'publish',
            'order'	   => 'DESC',
            'include' => $sale_products_ids,
        ));
    }
}
if ( !empty($new_products_titles) ) {
    $recent_products_ids = get_wc_products_ids_by_titles($atts['new_products']);
    $recent_products = wc_get_products( array(
        'status'   => 'publish',
        'order'	   => 'DESC',
        'include' => $recent_products_ids,
    ));
} else {
    if ( !empty(fw_get_db_settings_option('glob_new_products')) ) {
        $recent_products_ids = get_wc_products_ids_by_titles($glob_new_products_titles);
        $recent_products = wc_get_products( array(
            'status'   => 'publish',
            'order'	   => 'DESC',
            'include' => $recent_products_ids,
        ));
    }
}
if ( !empty($popular_products_titles) ) {
    $popular_products_ids = get_wc_products_ids_by_titles($atts['popular_products']);
    $popular_products = wc_get_products( array(
        'status'   => 'publish',
        'order'	   => 'DESC',
        'orderby'  => 'meta_value_num',
        'meta_key' => '_wc_average_rating',
        'include' => $popular_products_ids,
    ));
} else {
    if ( !empty(fw_get_db_settings_option('glob_popular_products')) ) {
        $popular_products_ids = get_wc_products_ids_by_titles($glob_popular_products_titles);
        $popular_products = wc_get_products( array(
            'status'   => 'publish',
            'order'	   => 'DESC',
            'include' => $popular_products_ids,
        ));
    }
}

if ( empty($recent_products) && empty($popular_products) && empty($sale_products) ) {
    return;
}

$defaultSliderType = '';
$defaultSliderProducts = '';
$catalog_link = $atts['catalog_link'];

if (!empty($recent_products)) {
    $defaultSliderType = 'new';
    $defaultSliderProducts = $recent_products;
} elseif (!empty($popular_products)) {
    $defaultSliderType = 'popular';
    $defaultSliderProducts = $popular_products;
} elseif (!empty($sale_products)) {
    $defaultSliderType = 'sale';
    $defaultSliderProducts = $sale_products;
}
?>

<!-- product-slider -->
<section id="product-slider" class="section section--product-slider">
    <div class="row">
        <div class="col">
            <div class="product-slider-swicher">
                <?php if (!empty($sale_products)) { ?>
                <button class="product-slider-swicher__item" data-slider="sale"><?php _e( 'Скидки', 'tesoro-shop' ); ?></button>
                <?php } ?>
                <?php if (!empty($recent_products)) { ?>
                <button class="product-slider-swicher__item active" data-slider="new"><?php _e( 'Новинки', 'tesoro-shop' ); ?></button>
                <?php } ?>
                <?php if (!empty($popular_products)) { ?>
                <button class="product-slider-swicher__item" data-slider="popular"><?php _e( 'Хиты', 'tesoro-shop' ); ?></button>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div data-slider="<?php echo $defaultSliderType; ?>" class="product-slider owl-carousel owl-theme">
                <?php echo ts_build_products( $defaultSliderProducts, $defaultSliderType ); ?>
            </div>
            <div class="product-slider__storage">
                <div class="sale">
                    <?php echo ts_build_products( $sale_products, 'sale' ); ?>
                </div>
                <div class="new">
                    <?php echo ts_build_products( $recent_products, 'new' ); ?>
                </div>
                <div class="popular">
                    <?php echo ts_build_products( $popular_products, 'popular' ); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="section__button-wrapper">
                <?php if (empty($catalog_link)) : ?>
                <a href="<?php echo get_post_type_archive_link( 'product' ); ?>" class="button section__button"><?php _e( 'В КАТАЛОГ', 'tesoro-shop' ); ?></a>
                <?php else : ?>
                <a href="<?php echo $catalog_link; ?>" class="button section__button"><?php _e( 'В КАТАЛОГ', 'tesoro-shop' ); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- #product-slider -->