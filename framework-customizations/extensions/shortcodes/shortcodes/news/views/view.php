<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$term_ids = $atts['news_cats'];

if ( empty($term_ids) ) {
    $args = array(
        'post_type'   => 'post',
        'post_status' => 'publish',
        'numberposts' => '3',
        'orderby'     => 'date',
        'order'       => 'DESC',
    );
} else {
    $args = array(
        'post_type'   => 'post',
        'post_status' => 'publish',
        'numberposts' => '3',
        'orderby'     => 'date',
        'order'       => 'DESC',
        'tax_query'      => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'id',
                'terms'    => $term_ids
            )
        )
    );
}


$news = get_posts( $args );

if ( empty( $news ) ) {
	return;
}

/**
 * @var array $atts
 */

$news_title = $atts['news_title'];
$news_text = $atts['news_text'];

?>

<!-- news -->
<section class="section section-news">
    <div class="section__title-wrapepr text-center">
        <h2 class="section__title"><?php echo $news_title; ?></h2>
    </div>
    <div class="section__text text-center"><?php echo $news_text; ?></div>
    <div class="news owl-carousel owl-theme">
        
        <?php foreach ( $news as $new_item ) { 
            setup_postdata($new_item);
            $categories = get_the_category($new_item->ID);
        ?>
            <article id="post-<?php the_ID(); ?>" class="post">
                <a href="<?php echo get_permalink($new_item->ID); ?>" class="post__image-wrapper">
                    <div class="post__image" style="background-image: url(<?php echo get_the_post_thumbnail_url($new_item->ID, 'full'); ?>);"></div>
                </a>
                <a href="<?php echo get_permalink($new_item->ID); ?>"><h3 class="post__title"><?php echo $new_item->post_title; ?></h3></a>
                <div class="post__data">
                    <time date="<?php echo $new_item->post_date; ?>" class="post__date"><?php echo get_the_date('d M Y', $new_item->ID); ?></time> |
                    <?php foreach ($categories as $category ) { ?>
                        <a class="post__cat" href="<?php echo get_category_link( $category->term_id ); ?>" ><?php echo $category->name; ?></a>
                    <?php } ?>
                </div>
                <div class="post__excerpt">
                    <?php echo wp_trim_words( $new_item->post_excerpt, 25, '...' ); ?>
                </div>
            </article >
        <?php } 
            wp_reset_postdata();
        ?>
    </div>
    <div class="section__button-wrapper">
        <a href="<?php echo get_post_type_archive_link( 'post' ); ?>" class="button section__button"><?php _e('СМОТРЕТЬ ВСЕ НОВОСТИ', 'tesoro-shop'); ?></a>
    </div>
</section>
<!-- #news -->