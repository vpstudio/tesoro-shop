<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('News', 'fw'),
	'description'   => __('Add a News', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
);