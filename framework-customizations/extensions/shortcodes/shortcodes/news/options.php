<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'news_title' => array(
		'type' => 'text',
		'label' => __('Заголовок', 'tesoro-shop'),
	),
	'news_cats' => array(
		'type'  => 'multi-select',
		'label' => __('Категории', 'tesoro-shop'),
		'desc'  => __('Укажите категории новостей', 'tesoro-shop'),
		// 'choices' => fw_get_category_term_list(),
		'population' => 'taxonomy',
		'source' => 'category',
		'prepopulate' => 10,
		'limit' => 10,
	),
	'news_text' => array(
		'type' => 'wp-editor',
		'label' => __('Текст', 'tesoro-shop'),
	),	
);

