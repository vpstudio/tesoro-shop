<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['feachers_line_block_text'] ) ) {
	return;
}

$feachers_line_block_title = $atts['feachers_line_block_title'];
$feachers_line_block_text = $atts['feachers_line_block_text'];
$feachers_line_block_feachers = $atts['feachers_line_block_feachers'];
$feachers_line_block_button_title = $atts['feachers_line_block_button_title'];
$feachers_line_block_button_link = $atts['feachers_line_block_button_link'];

?>

<!-- feachers_line_block -->
<section class="section section--feachers-line-block">
	<div class="row">
		<div class="col">
			<h2 class="section__title" ><?php echo $feachers_line_block_title; ?></h2>
			<div class="section__text"><?php echo $feachers_line_block_text; ?></div>
			<div class="feachers-line-list">
				<?php foreach ( $feachers_line_block_feachers as $feacher ) { 
					$feacher_icon = !empty($feacher['feachers_line_icon']) ? $feacher['feachers_line_icon']['icon-class'] : '';
				?>
				<div class="feachers-line-list__item">
					<i class="<?php echo $feacher_icon; ?>"></i>
					<h3 class="feachers-line-list__title"><?php echo $feacher['feachers_line_title']; ?></h3>
					<span></span>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php if (!empty($feachers_line_block_button_link)) { ?>
	<div class="row">
		<div class="col">
			<a href="<?php echo $feachers_line_block_button_link; ?>" class="button section__button"><?php echo $feachers_line_block_button_title; ?></a>
		</div>
	</div>
	<?php } ?>
</section>
<!-- #feachers_line_block -->