<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'feachers_line_block_title' => array(
		'type'  => 'text',
		'label' => __('Заголовок', 'tesoro-shop'),
	),
	'feachers_line_block_text' => array(
		'type'  => 'wp-editor',
		'label' => __('Текст', 'tesoro-shop'),
	),
	'feachers_line_block_feachers' => array(
    'type'  => 'addable-box',
    'label' => __('Особенности', 'tesoro-shop'),
    'box-options' => array(
        'feachers_line_title' => array( 
					'type' => 'text',
					'label' => __('Заголовок', 'tesoro-shop'),
				 ),
        'feachers_line_icon' => array(
				'label' => __('Иконка', 'tesoro-shop'),
				'type' => 'icon-v2',
				'preview_size' => 'small',
				'modal_size' => 'small',
				),
    ),
    'template' => '{{- feachers_line_title }}',
    'limit' => 4,
    'add-button-text' => __('Add', 'tesoro-shop'),
    'sortable' => true,
	),
	'feachers_line_block_button_title' => array(
		'type'  => 'text',
		'label' => __('Название кнопки', 'tesoro-shop'),
	),
	'feachers_line_block_button_link' => array(
		'type'  => 'text',
		'label' => __('URL кнопки', 'tesoro-shop'),
	),
);

