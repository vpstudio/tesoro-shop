<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'col2_title_l' => array(
		'type'  => 'text',
		'label' => __('Заголовок слева', 'tesoro-shop'),
	),
	'col2_text_l' => array(
		'type'  => 'wp-editor',
		'label' => __('Текст слева', 'tesoro-shop'),
	),
	'col2_title_r' => array(
		'type'  => 'text',
		'label' => __('Заголовок справа', 'tesoro-shop'),
	),
	'col2_text_r' => array(
		'type'  => 'wp-editor',
		'label' => __('Текст слева', 'tesoro-shop'),
	),
	'col2_image' => array(
		'type'  => 'upload',
		'label' => __('Фоновое изображение', 'tesoro-shop'),
		'images_only' => true,
	),
);

