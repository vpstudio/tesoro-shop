<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['col2_image'] ) ) {
	return;
}

$col2_title_l = $atts['col2_title_l'];
$col2_title_r = $atts['col2_title_r'];
$col2_text_l = $atts['col2_text_l'];
$col2_text_r = $atts['col2_text_r'];

$col2_image_id = !empty($atts['col2_image']) ? $atts['col2_image']['attachment_id'] : '';

$col2_image = wp_get_attachment_image_url($col2_image_id, 'full');

?>

<!-- col2-info-dark -->
<section class="section section--col2-info-dark" style="background-image: url(<?php echo $col2_image; ?>)">
  <div class="col2-info-dark__overlay"></div>
  <div class="col2-info-dark">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-6">
          <h4><?php echo $col2_title_l; ?></h4>
          <?php echo $col2_text_l; ?>
        </div>
        <div class="col-md-12 col-lg-6">
		  <h4><?php echo $col2_title_r; ?></h4>
          <?php echo $col2_text_r; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- #col2-info-dark -->