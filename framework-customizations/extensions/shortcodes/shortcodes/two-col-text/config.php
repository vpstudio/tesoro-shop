<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Two col text', 'fw'),
	'description'   => __('Add an two col text', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
);