<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

if ( empty( $atts['stores_list'] ) ) {
	return;
}

$stores = $atts['stores_list'];


/**
 * @var array $atts
 */
?>

<!-- stores-list -->
<section class="section section--stores-list">
    <div class="row">
        <div class="col">
            <div class="stores-location owl-carousel owl-theme">
                <?php foreach ($stores as $store) { ?>
                <div class="store">
                    <h4 class="store__name">
                        <a title="<?php _e('Открыть на карте', 'tesoro-shop'); ?>" rel="map" target="_black" href="https://maps.google.com/?q=<?php echo $store['stores_list_address']; ?>"><?php echo $store['stores_list_name']; ?></a>
                    </h4>
                    <a class="store__address" title="<?php _e('Открыть на карте', 'tesoro-shop'); ?>" rel="map" target="_black" href="https://maps.google.com/?q=<?php echo $store['stores_list_address']; ?>"><?php echo $store['stores_list_address']; ?></a>
                    <div class="store__contacts">
                        <a href="tell:<?php echo str_replace([' ', '(', ')', '+', '-'], '', $store['stores_list_phone']); ?>" rel="noopener noreferrer" class="store__phone"><?php echo $store['stores_list_phone']; ?></a>
                        <a href="mailto:<?php echo $store['stores_list_email']; ?>" rel="noopener noreferrer" class="store__email"><?php echo $store['stores_list_email']; ?></a>
                    </div>
                    <div class="store__work-time"><?php echo $store['stores_list_work_time']; ?></div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<!-- #stores-list -->