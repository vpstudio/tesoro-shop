<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Stores List', 'fw'),
	'description'   => __('Add a Stores List', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
);