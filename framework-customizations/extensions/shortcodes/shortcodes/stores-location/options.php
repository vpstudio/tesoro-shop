<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'stores_list' => array(
		'type'  => 'addable-box',
		'value' => array(
			array(
				'stores_list_name' => __('Mono бутик «Roberto Bravo»', 'tesoro-shop'),
				'stores_list_address' => __('Одесса, ул. Генуэзская 24Б, ТЦ Kadorr City Mall, 1й этаж', 'tesoro-shop'),
				'stores_list_phone' => '+38 063 063-19-99',
				'stores_list_email' => 'RobertoBravo.ua@gmail.com',
				'stores_list_work_time' => __('с 10:00 до 21:00 без выходных', 'tesoro-shop'),
			),
			array(
				'stores_list_name' => __('Tesoro Jewelry', 'tesoro-shop'),
				'stores_list_address' => __('Одесса, Семафорный переулок 4, ТРЦ Среднефонтанский, 2-й этаж'),
				'stores_list_phone' => '+38 093 370-42-21',
				'stores_list_email' => 'tesoro.jew@gmail.com',
				'stores_list_work_time' => __('с 10:00 до 22:00 без выходных', 'tesoro-shop'),
			),
			array(
				'stores_list_name' => __('Tesoro Bags', 'tesoro-shop'),
				'stores_list_address' => __('Одесса, Семафорный переулок 4, ТРЦ Среднефонтанский, 1-й этаж', 'tesoro-shop'),
				'stores_list_phone' => '+38 093 672-69-14',
				'stores_list_email' => 'tesorobags@gmail.com',
				'stores_list_work_time' => __('с 10:00 до 22:00 без выходных', 'tesoro-shop'),
			),
			array(
				'stores_list_name' => __('Tioro', 'tesoro-shop'),
				'stores_list_address' => __('Киев, пл.Институтская 2,ТЦ «Глобус»', 'tesoro-shop'),
				'stores_list_phone' => '+38 050 152-01-98',
				'stores_list_email' => 'tioro.jewels@gmail.com',
				'stores_list_work_time' => __('с 10:00 до 22:00 без выходных', 'tesoro-shop'),
			),
			array(
				'stores_list_name' => __('St.Diamond', 'tesoro-shop'),
				'stores_list_address' => __('Львов, пр.Шевченка, 26', 'tesoro-shop'),
				'stores_list_phone' => '+38 067 327-08-88',
				'stores_list_email' => 'lvivdiamond@gmail.com',
				'stores_list_work_time' => __('с 10:00 до 22:00 без выходных', 'tesoro-shop'),
			),
			// ...
		),
		'label' => __('Список магазинов', 'tesoro-shop'),
		'box-options' => array(
			'stores_list_name' => array( 'type' => 'text', 'value' => __('Название', 'tesoro-shop'), ),
			'stores_list_address' => array( 'type' => 'text', 'value' => __('Адресс', 'tesoro-shop'), ),
			'stores_list_phone' => array( 'type' => 'text', 'value' => __('Телефон', 'tesoro-shop'), ),
			'stores_list_email' => array( 'type' => 'text', 'value' => __('Email', 'tesoro-shop'), ),
			'stores_list_work_time' => array( 'type' => 'text', 'value' => __('Время работы', 'tesoro-shop'), ),
		),
		'template' => 'Магазин {{- stores_list_name }}',
		'limit' => 10,
		'add-button-text' => __('Add', 'tesoro-shop'),
		'sortable' => true,
	)
);

