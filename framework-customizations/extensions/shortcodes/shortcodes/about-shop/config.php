<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('About Shop', 'fw'),
	'description'   => __('Add an About Shop', 'fw'),
	'popup_size'    => 'large',
	'tab'           => __('Tesoro', 'fw'),
);