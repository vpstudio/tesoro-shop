<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['about_text'] ) ) {
	return;
}

$about_title = $atts['about_title'];
$about_text = $atts['about_text'];
$about_button = $atts['about_button'];
$about_button_link = $atts['about_button_link'];
$about_image_id = !empty($atts['about_image']) ? $atts['about_image']['attachment_id'] : '';

$about_image = wp_get_attachment_image_url($about_image_id, 'full');
$about_type = $atts['about_type'] ? ' col-xl-6' : '';

?>

<!-- about -->
<section class="section section-about" style="background-image: url(<?php echo $about_image; ?>);">
	<div class="row">
		<div class="col-12<?php echo $about_type; ?>">
			<h2 class="section__title" ><?php echo $about_title; ?></h2>
			<div class="section__text"><?php echo $about_text; ?></div>
			<?php if ( $about_button ) {
				echo '<div class="section__button-wrapper"><a href="'. $about_button_link .'" class="button section__button">'. __('Читать о нас', 'tesoro-shop') .'</a></div>';
			} ?>
		</div>
	</div>
</section>
<!-- #about -->