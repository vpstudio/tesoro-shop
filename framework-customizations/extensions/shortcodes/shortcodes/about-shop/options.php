<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'about_type' => array(
		'type'  => 'switch',
		'value' => true,
		'label' => __('На половину экрана?', 'tesoro-shop'),
		'left-choice' => array(
			'value' => true,
			'label' => __('Да', 'tesoro-shop'),
		),
		'right-choice' => array(
			'value' => false,
			'label' => __('Нет', 'tesoro-shop'),
		),
	),
	'about_title' => array(
		'type'  => 'text',
		'label' => __('Заголовок', 'tesoro-shop'),
	),
	'about_text' => array(
		'type'  => 'wp-editor',
		'size' => 'large', // small, 
		'label' => __('Текст', 'tesoro-shop'),
	),
	'about_button' => array(
		'type'  => 'switch',
		'value' => true,
		'label' => __('Показывать кнопку', 'tesoro-shop'),
		'left-choice' => array(
			'value' => true,
			'label' => __('Показывать', 'tesoro-shop'),
		),
		'right-choice' => array(
			'value' => false,
			'label' => __('Скрыть', 'tesoro-shop'),
		),
	),
	'about_button_link' => array(
		'type'  => 'text',
		'label' => __('URL кнопки', 'tesoro-shop'),
	),
);

