<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Contact form', 'fw'),
	'description'   => __('Add an Contact form', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
);