<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['contact_shortcode'] ) ) {
	return;
}

$contact_shortcode = $atts['contact_shortcode'];
$contact_switch = $atts['contact_switch'];
$contact_soc_links = $atts['contact_soc_links'];
$contact_title = $atts['contact_title'];
$contact_subtitle = $atts['contact_subtitle'];

fw()->backend->option_type('icon-v2')->packs_loader->enqueue_frontend_css();
?>

<!-- contact-from -->
<section class="section section--contact-from">
  <div class="row">
    <div class="com-md-12 order-12 order-lg-1 col-lg-6">
      <div class="contact-form">
        <?php echo do_shortcode($contact_shortcode); ?>
      </div>
    </div>
    <?php if ( $contact_switch == 'show' ) { ?>
    <div class="com-md-12 order-1 order-lg-12 col-lg-5 offset-lg-1">
      <div class="contact-form-wrapper">
        <h2 class="contact-form-title"><?php echo $contact_title; ?></h2>
        <div class="contact-form-substitle"><?php echo $contact_subtitle; ?></div>
        <ul class="contact-from-soc-links">
          <?php foreach ($contact_soc_links as $soc_link ) { 
            switch ($soc_link['type']) {
              case 'phone':
                echo '<li><a href="tel:'. str_replace([' ', '(', ')', '+', '-'], '', $soc_link['url']) .'" title="'. $soc_link['title'] .'" rel="noopener noreferrer"><i class="'. $soc_link['icon']['icon-class'] .'"></i> '. $soc_link['title'] .'</a></li>';
                break;
              case 'mail':
                echo '<li><a href="mailto:'. $soc_link['url'] .'" title="'. $soc_link['title'] .'" rel="noopener noreferrer"><i class="'. $soc_link['icon']['icon-class'] .'"></i> '. $soc_link['title'] .'</a></li>';
                break;
              default:
                echo '<li><a href="'. $soc_link['url'] .'" title="'. $soc_link['title'] .'" target="_blank" rel="noopener noreferrer"><i class="'. $soc_link['icon']['icon-class'] .'"></i> '. $soc_link['title'] .'</a></li>';
                break;
            }
          } ?>
        </ul>
      </div>
    </div>
    <?php } ?>
  </div>
</section>
<!-- #contact-from -->