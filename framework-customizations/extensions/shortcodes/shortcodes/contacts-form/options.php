<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'contact_shortcode' => array(
		'type'  => 'text',
		'label' => __('Contact form shortcode', 'tesoro-shop'),
	),
	'contact_switch' => array(
		'type'  => 'switch',
		'value' => 'show',
		'label' => __('Показывать список социальных ссылок', 'tesoro-shop'),
		'left-choice' => array(
			'value' => 'show',
			'label' => __('Показывать', 'tesoro-shop'),
		),
		'right-choice' => array(
			'value' => 'hide',
			'label' => __('Скрыть', 'tesoro-shop'),
		),
	),
	'contact_title' => array(
		'type'  => 'text',
		'label' => __('Contact form title', 'tesoro-shop'),
	),
	'contact_subtitle' => array(
		'type'  => 'textarea',
		'label' => __('Contact form subtitle', 'tesoro-shop'),
	),
	'contact_soc_links' => array(
		'type'  => 'addable-box',
		'label' => __('Социальные ссылки', 'tesoro-shop'),
		'box-options' => array(
			'type' => array(
				'type'  => 'select',
				'label' => __('Тип', 'tesoro-shop'),
				'choices' => array(
					'link' => __('Ссылка', 'tesoro-shop'),
					'mail' => __('Почта', 'tesoro-shop'),
					'phone' => __('Телефон', 'tesoro-shop'),
				),
			),
			'title' => array( 'type' => 'text' ),
			'url' => array( 'type' => 'text' ),
			'icon' => array( 
				'type' => 'icon-v2',
				'preview_size' => 'small',
				'modal_size' => 'small',
			),
		),
		'template' => '{{- title }}', // box title
		'limit' => 4, // limit the number of boxes that can be added
		'add-button-text' => __('Add', 'tesoro-shop'),
		'sortable' => true,
	),
);

