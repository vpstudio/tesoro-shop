<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['video_brand_link'] ) ) {
	return;
}

$video_brand_link = $atts['video_brand_link'];
$video_brand_title = $atts['video_brand_title'];
$video_brand_image = !empty($atts['video_brand_image']) ? $atts['video_brand_image']['url'] : '';

?>

<!-- video -->
<section class="section section--video">
  <div class="video">
      <div class="video__bg" style="background-image: url(<?php echo $video_brand_image; ?>)"></div>
      <div class="video__wrapper">
              <a href="<?php echo $video_brand_link; ?>" rel="noopener noreferrer" title="<?php echo $video_brand_title; ?>" class="popup-youtube video__content">
                  <svg width="144" height="144" viewBox="0 0 144 144" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M66.1607 27.3281L27.625 48.7433C27.1801 48.9948 26.7932 49.0238 26.4643 48.8304C26.1548 48.6369 26 48.2887 26 47.7857V5.07143C26 4.56845 26.1548 4.22024 26.4643 4.02679C26.7932 3.83333 27.1801 3.86235 27.625 4.11384L66.1607 25.529C66.6057 25.7805 66.8281 26.0804 66.8281 26.4286C66.8281 26.7768 66.6057 27.0766 66.1607 27.3281Z" transform="translate(32 46)" fill="#FFF200"></path>
                      <circle cx="72" cy="72" r="67" stroke="#FFF200" stroke-width="10"></circle>
                  </svg>
              </a>
      </div> 
  </div>
</section>
<!-- #video -->