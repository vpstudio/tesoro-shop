<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
      'video_brand_link' => array(
        'type'  => 'text',
        'value' => 'https://www.youtube.com/watch?v=HLhJ798sRtU',
        'label' => __('Ссылка на Youtube Видео бренда', 'tesoro-shop'),
      ),
      'video_brand_title' => array(
        'type'  => 'text',
        'value' => 'default value',
        'label' => __('Заголовок Видео бренда', 'tesoro-shop'),
      ),
      'video_brand_image' => array(
        'type'  => 'upload',
        'value' => array(
            'attachment_id' => null,
            'url' => 'https://images.unsplash.com/photo-1540167828867-7e5020e0bc8e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6b75c91f2c2c552d78c93be1f87d6fc2&auto=format&fit=crop&w=500&q=60'
        ),
        'label' => __('Фоновое изображение Видео бренда', 'tesoro-shop'),
        'images_only' => true,
      ),
);

