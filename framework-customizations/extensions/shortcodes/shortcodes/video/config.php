<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Video', 'fw'),
	'description'   => __('Add an Video', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
);