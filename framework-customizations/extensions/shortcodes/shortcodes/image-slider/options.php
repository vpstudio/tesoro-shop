<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'images_list' => array(
		'type' => 'addable-popup',
			'label' => __('Слайды', 'tesoro-shop'),
		'popup-title' => __('Новый слайд', 'tesoro-shop'),
		'size' => 'small', // small, medium, large
		'limit' => 7, // limit the number of popup`s that can be added
		'add-button-text' => __('Добавить', 'tesoro-shop'),
		'sortable' => true,
		'popup-options' => array(
			'image' => array(
				'label' => __('Изображение', 'tesoro-shop'),
				'type' => 'upload',
				'images_only' => true,
				'files_ext' => array('png', 'jpg', 'jpeg'),
			),
    	),
	),
);

