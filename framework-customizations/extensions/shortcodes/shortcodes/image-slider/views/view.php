<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['images_list'] ) ) {
	return;
}

$images_list = $atts['images_list'];

?>

<!-- section-image-slider -->
<section class="section section--image-slider">
	<!-- image-slider -->
	<div class="image-slider owl-carousel owl-theme">
		<?php foreach ($images_list as $item) { 
			$image = wp_get_attachment_image_url($item['image']['attachment_id'], 'full');
			echo '<div class="image-slider__slide" style="background-image: url('. $image .')"></div>';	
		} ?>
	</div>
	<!-- #image-slider -->
</section>
<!-- #section-image-slider -->

