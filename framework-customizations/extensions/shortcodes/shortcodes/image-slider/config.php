<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Image slider', 'fw'),
	'description'   => __('Add a Image slider', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
);