<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}



if ( empty( $atts['map_iframe'] ) ) {
	return;
}

$stores_switch = $atts['stores_switch'];
$stores = $atts['stores'];
$map_iframe = $atts['map_iframe'];
$map_height = $atts['map_height'];

/**
 * @var array $atts
 */
?>

<!-- stores-location -->
<section class="section section--stores-location">
        <?php if ( $stores_switch == 'show' ) { ?>
        <div class="row">
            <div class="col">
                <div class="stores-location owl-carousel owl-theme">
                    <?php foreach ($stores as $store) {
                        $name = isset($store['name']) ? $store['name'] : '';
                        $address = isset($store['address']) ? $store['address'] : '';
                        $phone = isset($store['phone']) ? $store['phone'] : '';
                        $email = isset($store['email']) ? $store['email'] : '';
                        $work_time = isset($store['work_time']) ? $store['work_time'] : '';
                    ?>
                    <div class="store">
                        <h4 class="store__name"><a title="<?php _e('Открыть на карте', 'tesoro-shop'); ?>" rel="map" target="_black" href="https://maps.google.com/?q=<?php echo $address; ?>"><?php echo $name; ?></a></h4>
                        <a class="store__address" title="<?php _e('Открыть на карте', 'tesoro-shop'); ?>" rel="map" target="_black" href="https://maps.google.com/?q=<?php echo $address; ?>"><?php echo $address; ?></a>
                        <div class="store__contacts">
                            <a href="tell:<?php echo str_replace([' ', '(', ')', '+', '-'], '', $phone); ?>" rel="noopener noreferrer" class="store__phone"><?php echo $phone; ?></a>
                            <a href="mailto:<?php echo $email; ?>" rel="noopener noreferrer" class="store__email"><?php echo $email; ?></a>
                        </div>
                        <div class="store__work-time"><?php echo $work_time; ?></div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col">
                <!-- map -->
                <div id="stores-location-map" class="<?php if ( $stores_switch == 'show' ) echo 'bordered'; ?>" style="height: <?php echo $map_height . 'px'; ?>;"><iframe src="<?php echo $map_iframe; ?>" height="100%" width="100%" frameborder="0"></iframe></div>
                <!-- #map -->
            </div>
        </div>
        <?php if ( $stores_switch != 'show' ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__button-wrapper">
                    <a href="<?php echo esc_url( home_url( '/our-stores' ) ); ?>" class="button section__button"><?php _e( 'Больше о наших магазинах', 'tesoro-shop' ); ?></a>
                </div>
            </div>
        </div>
        <?php } ?>
    
</section>
<!-- #stores-location -->