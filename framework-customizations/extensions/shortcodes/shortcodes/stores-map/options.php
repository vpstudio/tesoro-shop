<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'stores_switch' => array(
		'type'  => 'switch',
		'value' => 'show',
		'label' => __('Показывать список магазинов', 'tesoro-shop'),
		'left-choice' => array(
			'value' => 'show',
			'label' => __('Показывать', 'tesoro-shop'),
		),
		'right-choice' => array(
			'value' => 'hide',
			'label' => __('Скрыть', 'tesoro-shop'),
		),
	),
	'stores' => array(
		'type'  => 'addable-box',
		'value' => array(
			array(
				'name' => __('Mono бутик «Roberto Bravo»', 'tesoro-shop'),
				'address' => __('Одесса, ул. Генуэзская 24Б, ТЦ Kadorr City Mall, 1й этаж', 'tesoro-shop'),
				'phone' => '+38 063 063-19-99',
				'email' => 'RobertoBravo.ua@gmail.com',
				'work_time' => __('с 10:00 до 21:00 без выходных', 'tesoro-shop'),
			),
			array(
				'name' => __('Tesoro Jewelry', 'tesoro-shop'),
				'address' => __('Одесса, Семафорный переулок 4, ТРЦ Среднефонтанский, 2-й этаж'),
				'phone' => '+38 093 370-42-21',
				'email' => 'tesoro.jew@gmail.com',
				'work_time' => __('с 10:00 до 22:00 без выходных', 'tesoro-shop'),
			),
			array(
				'name' => __('Tesoro Bags', 'tesoro-shop'),
				'address' => __('Одесса, Семафорный переулок 4, ТРЦ Среднефонтанский, 1-й этаж', 'tesoro-shop'),
				'phone' => '+38 093 672-69-14',
				'email' => 'tesorobags@gmail.com',
				'work_time' => __('с 10:00 до 22:00 без выходных', 'tesoro-shop'),
			),
			array(
				'name' => __('Tioro', 'tesoro-shop'),
				'address' => __('Киев, пл.Институтская 2,ТЦ «Глобус»', 'tesoro-shop'),
				'phone' => '+38 050 152-01-98',
				'email' => 'tioro.jewels@gmail.com',
				'work_time' => __('с 10:00 до 22:00 без выходных', 'tesoro-shop'),
			),
			array(
				'name' => __('St.Diamond', 'tesoro-shop'),
				'address' => __('Львов, пр.Шевченка, 26', 'tesoro-shop'),
				'phone' => '+38 067 327-08-88',
				'email' => 'lvivdiamond@gmail.com',
				'work_time' => __('с 10:00 до 22:00 без выходных', 'tesoro-shop'),
			),
			// ...
		),
		'label' => __('Список магазинов', 'tesoro-shop'),
		'box-options' => array(
			'name' => array( 'type' => 'text', 'value' => __('Название', 'tesoro-shop'), ),
			'address' => array( 'type' => 'text', 'value' => __('Адресс', 'tesoro-shop'), ),
			'phone' => array( 'type' => 'text', 'value' => __('Телефон', 'tesoro-shop'), ),
			'email' => array( 'type' => 'text', 'value' => __('Email', 'tesoro-shop'), ),
			'work_time' => array( 'type' => 'text', 'value' => __('Время работы', 'tesoro-shop'), ),
		),
		'template' => 'Магазин {{- name }}',
		'limit' => 10,
		'add-button-text' => __('Add', 'tesoro-shop'),
		'sortable' => true,
	),
	'map_iframe' => array(
		'type' => 'text',
		'value' => 'https://www.google.com/maps/d/u/0/embed?mid=1ifZDLHoN7p7ihUm-J1HjEZ6Cp6S8IWcG',
		'label' => __('Iframe url', 'tesoro-shop'),
		'desc'  => __('Paste here url from google maps iframe', 'tesoro-shop'),
	),
	'map_height' => array(
		'type' => 'text',
		'value' => '522',
		'label' => __('Высота карты', 'tesoro-shop'),
	),
);

