<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Stores Map', 'fw'),
	'description'   => __('Add a Stores Map', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
);