<?php if (!defined('FW')) die( 'Forbidden' );
$color_class = !empty($atts['color']) ? "button--custom--{$atts['color']}" : ''; 
$align_class = !empty($atts['align']) ? $atts['align'] : 'center'; 
$icon_class= !empty($atts['icon']) ? $atts['icon']['icon-class'] : ''; ?>
<div style="text-align:<?php echo esc_attr($align_class); ?>;" class="button--custom-wrapper">
<a href="<?php echo esc_attr($atts['link']) ?>" target="<?php echo esc_attr($atts['target']) ?>" class="button--custom <?php echo esc_attr($color_class); ?>">
<?php if (!empty($icon_class)){ ?><i class="<?php echo $icon_class; ?>"></i><?php } ?><?php echo "<span>".$atts['label']."</span>"; ?></a>
</div>
