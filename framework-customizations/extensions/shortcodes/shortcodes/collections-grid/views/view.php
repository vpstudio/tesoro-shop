<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$collections = $atts['collections'];
if ( empty($collections)) {
    return;
} 
$catalog_link = $atts['catalog_link'];
?>

<section class="section section--rb-grid">
  <div class="rb-grid">
    <div class="row">
      <?php foreach( $collections as $collection ) :
            if (empty($collection['url']) || $collection['url'] == '#') : continue; endif;
            $collection_image = $collection['image']; ?>
        <div class="col-md-3">
          <a href="<?php echo $collection['url']; ?>" class="rb-grid__item">
            <div class="rb-grid__image">
              <img src="<?php echo !empty($collection_image) ? $collection_image['url'] : ''; ?>" alt="<?php echo $collection['title']; ?>">
              <div class="rb-grid__overlay"></div>
            </div>
            <h6><?php echo $collection['title']; ?></h6>
            <p>COLLECTION</p>
          </a>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
  <!-- owl-carousel owl-theme -->
  <div class="container rb-grid rb-grid--mobile owl-carousel owl-theme">
    <?php
    $count = 1;
    foreach( $collections as $collection ) {
      $collection_image = $collection['image'];
      $collection_image_url = !empty($collection_image) ? $collection_image['url'] : "#";
      if ($count%4 == 1) {
        echo '<div class=" rb-grid__row">';
      }
      echo '<a href="'. $collection['title'] .'" class="rb-grid__item">';
      echo  '<div class="rb-grid__image"><img src="'. $collection_image_url .'" alt="'. $collection['title'] .'"><div class="rb-grid__overlay"></div></div>';
      echo   '<h6>'. $collection['title'] .'</h6>';
      echo   '<p>COLLECTION</p>';
      echo '</a>';
      if ($count%4 == 0) {
        echo '</div>';
      }
      $count++;
    }
    if ($count%4 != 1) echo "</div>";
    ?>
  </div>
  <div class="row">
    <div class="col">
      <div class="section__button-wrapper">
          <?php if (empty($catalog_link)) : ?>
          <a href="<?php echo get_post_type_archive_link( 'product' ); ?>" class="button section__button"><?php _e( 'ПЕРЕЙТИ В КАТАЛОГ', 'tesoro-shop' ); ?></a>
          <?php else : ?>
          <a href="<?php echo $catalog_link; ?>" class="button section__button"><?php _e( 'ПЕРЕЙТИ В КАТАЛОГ', 'tesoro-shop' ); ?></a>
          <?php endif; ?>
      </div>
    </div>
  </div>
</section>