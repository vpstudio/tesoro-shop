<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
  'categories_links_list' => array(
    'type'  => 'addable-box',
    'value' => array(
      array(
          'cat_title' => __('Золотые украшения', 'tesoro-shop'),
          'cat_link' => '#',
      ),
      array(
        'cat_title' => __('Серебряные украшения', 'tesoro-shop'),
        'cat_link' => '#',
      ),
      array(
        'cat_title' => __('Браслеты', 'tesoro-shop'),
        'cat_link' => '#',
      ),
      array(
        'cat_title' => __('Звенья Nomination', 'tesoro-shop'),
        'cat_link' => '#',
      ),
      array(
        'cat_title' => __('Кольца', 'tesoro-shop'),
        'cat_link' => '#',
      ),
      array(
        'cat_title' => __('Серьги', 'tesoro-shop'),
        'cat_link' => '#',
      ),
      array(
        'cat_title' => __('Мужские украшения', 'tesoro-shop'),
        'cat_link' => '#',
      ),
      array(
        'cat_title' => __('Сумки', 'tesoro-shop'),
        'cat_link' => '#',
      ),
      array(
        'cat_title' => __('Часы', 'tesoro-shop'),
        'cat_link' => '#',
      ),
      // ...
    ),
    'label' => __('Категории', 'tesoro-shop'),
    'box-options' => array(
        'cat_title' => array( 'type' => 'text', 'label' => __('Заголовок', 'tesoro-shop') ),
        'cat_link' => array( 'type' => 'text', 'label' => __('URL', 'tesoro-shop') ),
        'cat_image' => array( 'type' => 'upload', 'label' => __('Изображение', 'tesoro-shop' ) ),
    ),
    'template' => '{{- cat_title }}', // box title
    'limit' => 15, // limit the number of boxes that can be added
    'add-button-text' => __('Добавить', 'tesoro-shop'),
    'sortable' => true,
  ),
);

