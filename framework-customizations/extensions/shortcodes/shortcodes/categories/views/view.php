<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$categories_list = $atts['categories_links_list'];

if ( empty($categories_list) ) {
	return;
} ?>

<!-- categories -->
<section class="section section-categories">
    <div class="categories owl-carousel owl-theme">
        
        <?php foreach ( $categories_list as $category ) {
            $title = $category['cat_title'];
            $link = $category['cat_link'];
            $image = $category['cat_image'];
        ?>
            <a href="<?php echo $link; ?>"  class="categories__item">
                <div class="categories__image">
                    <?php if (!empty($image) ) { ?> 
                    <img class="img-responsive" src="<?php echo $image['url']; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>">
                    <?php } ?>
                </div>
                <h6 class="categories__name"><?php echo $title; ?></h6>
            </a>
        <?php } ?>
        
    </div>
</section>
<!-- #categories -->