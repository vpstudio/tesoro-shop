<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Categories', 'fw'),
	'description'   => __('Add a Categories', 'fw'),
	'tab'           => __('Tesoro', 'fw'),
);