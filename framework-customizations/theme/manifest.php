<?php if (!defined('FW')) die('Forbidden');

$manifest['id'] = get_option( 'stylesheet' );

$manifest['supported_extensions'] = array(
  'translate-press' => array()
);

$manifest['requirements'] = array(
  'wordpress' => array(
      'min_version' => '4.9.7'
  ),
  'framework' => array(
      'min_version' => '2.7.19'
  )
);