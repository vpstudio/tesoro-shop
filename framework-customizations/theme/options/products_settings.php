<?php if (!defined( 'FW' )) die('Forbidden');

$options = array(
  'products_settings' => array(
      'type' => 'tab',
      'options' => array(

        'product_sorting_box' => array(
          'type'    => 'box',
          'title'   => 'Сортировка товаров',
          'options' => array(
            'brands_sort_list' => array(
              'type'  => 'multi-select',
              'label' => __('Порядок сортировки', 'tesoro-shop'),
              'desc'  => __('Укаржите бренды в правильном порядке', 'tesoro-shop'),
              'population' => 'taxonomy',
              'source' => 'brand',
              'prepopulate' => 999,
            ),
          ),
        ),
        
      ),
      'title' => __('Настройки товаров', 'tesoro-shop'),
  ),
);