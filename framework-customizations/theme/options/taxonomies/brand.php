<?php if (!defined( 'FW' )) die('Forbidden');

$options = array(
  'brand_image' => array(
    'type' => 'upload',
    'label' => __('Полное изображение бренда', 'tesoro-shop'),
    'fw-storage' => array(
      'type' => 'post-meta',
      'post-meta' => 'demo_post_meta',
    ),
  ),
  'brand_text' => array(
    'type'  => 'wp-editor',
    'label' => __('Описание бренда', 'tesoro-shop'),
    'size' => 'large',
    'editor_height' => 400,
    'wpautop' => true,
    'editor_type' => 'tinymce',
    'shortcodes' => false 
  ),
  'product_іs_hide_price' => array(
    'type'  => 'checkbox',
    'label' => __('Скрыть цену товара?', 'tesoro-shop'),
    'text'  => __('Скрыть', 'tesoro-shop'),
    'fw-storage' => array(
        'type' => 'post-meta',
        'post-meta' => 'demo_post_meta',
    ),
  ),
  'brand_thumbnail' => array(
    'type' => 'upload',
    'label' => __('Миниатюра бренда', 'tesoro-shop'),
  ),
  'left_soc' => array(
		'type'  => 'addable-box',
		'label' => __('Социальные ссылки слева', 'tesoro-shop'),
		'box-options' => array(
			'link_type' => array(
				'type'  => 'select',
				'label' => __('Тип', 'tesoro-shop'),
				'choices' => array(
					'link' => __('Ссылка', 'tesoro-shop'),
					'email' => __('Почта', 'tesoro-shop'),
					'phone' => __('Телефон', 'tesoro-shop'),
				),
			),
			'title' => array( 'type' => 'text' ),
			'link' => array( 'type' => 'text' ),
			'icon' => array( 
				'type' => 'icon-v2',
				'preview_size' => 'small',
				'modal_size' => 'small',
			),
		),
		'template' => '{{- title }}', // box title
		'limit' => 2, // limit the number of boxes that can be added
		'add-button-text' => __('Add', 'tesoro-shop'),
		'sortable' => true,
  ),
  'right_soc' => array(
		'type'  => 'addable-box',
		'label' => __('Социальные ссылки справа', 'tesoro-shop'),
		'box-options' => array(
			'link_type' => array(
				'type'  => 'select',
				'label' => __('Тип', 'tesoro-shop'),
				'choices' => array(
					'link' => __('Ссылка', 'tesoro-shop'),
					'email' => __('Почта', 'tesoro-shop'),
					'phone' => __('Телефон', 'tesoro-shop'),
				),
			),
			'title' => array( 'type' => 'text' ),
			'link' => array( 'type' => 'text' ),
			'icon' => array( 
				'type' => 'icon-v2',
				'preview_size' => 'small',
				'modal_size' => 'small',
			),
		),
		'template' => '{{- title }}', // box title
		'limit' => 2, // limit the number of boxes that can be added
		'add-button-text' => __('Add', 'tesoro-shop'),
		'sortable' => true,
  ),
  'brand_slider' => array(
    'type'  => 'addable-box',
    'label' => __('Слайдер бренда', 'tesoro-shop'),
    'box-options' => array(
        'image' => array( 'type' => 'upload', 'images_only' => true ),
        'title' => array( 'type' => 'text' ),
    ),
    'template' => '{{- title }}',
    'limit' => 5,
    'add-button-text' => __('Добавить', 'tesoro-shop'),
    'sortable' => true,
  ),
  'video_box' => array(
    'type'  => 'box',
    'options' => array(
      'video_visible' => array(
        'type'  => 'switch',
        'value' => 'show',
        'label' => __('Показывать Видео бренда', 'tesoro-shop'),
        'left-choice' => array(
            'value' => 'show',
            'label' => __('Показывать', 'tesoro-shop'),
        ),
        'right-choice' => array(
            'value' => 'hide',
            'label' => __('Скрыть', 'tesoro-shop'),
        ),
      ),
      'video_brand_link' => array(
        'type'  => 'text',
        'value' => 'https://www.youtube.com/watch?v=HLhJ798sRtU',
        'label' => __('Ссылка на Youtube Видео бренда', 'tesoro-shop'),
      ),
      'video_brand_title' => array(
        'type'  => 'text',
        'value' => 'default value',
        'label' => __('Заголовок Видео бренда', 'tesoro-shop'),
      ),
      'video_brand_image' => array(
        'type'  => 'upload',
        'value' => array(
            'attachment_id' => null,
            'url' => '//tesoro-jewelry.com.ua/wp-content/uploads/Group-2-1.jpg'
        ),
        'label' => __('Фоновое изображение Видео бренда', 'tesoro-shop'),
        'images_only' => true,
      ),
    ),
    'title' => __('Видео бренда', 'tesoro-shop'),
  ),
  'brand_cats' => array(
    'type'  => 'multi-picker',
    'label' => false,
    'desc'  => false,
    'value' => array(
        'gadget' => 'hide',
    ),
    'picker' => array(
        'gadget' => array(
            'label'   => __('Показывать категории бренда', 'tesoro-shop'),
            'type'    => 'select',
            'choices' => array(
                'show'  => __('Показывать', 'tesoro-shop'),
                'hide' => __('Не показывать', 'tesoro-shop')
            ),
        )
    ),
    'choices' => array(
        'show' => array(
            'brand_cat_list' => array(
              'type'  => 'addable-popup',
              'label' => __('Категория бренда', 'tesoro-shop'),
              'popup-options' => array(
                'brand_cat_title' => array(
                  'type'  => 'text',
                  'label' => __('Название категории бренда', 'tesoro-shop'),
                ),
                'brand_cat_link' => array(
                    'type'  => 'text',
                    'label' => __('Ссылка на категорию бренда', 'tesoro-shop'),
                ),
                'brand_cat_image' => array(
                  'type' => 'upload',
                  'label' => __('Изображение категории бренда', 'tesoro-shop'),
                ),
              ),
              'template' => '{{- brand_cat_title }}', // box title
              'limit' => 4,
              'add-button-text' => __('Добавить', 'tesoro-shop'),
              'sortable' => true,
            ),
        ),
        'hide' => array(),
    ),
    'show_borders' => false,
  ),
  'hide_brand' => array(
    'type'  => 'checkbox',
    'label' => __('Скрыть Бренд?', 'tesoro-shop'),
    'text'  => __('Скрыть', 'tesoro-shop'),
    'fw-storage' => array(
      'type' => 'post-meta',
      'post-meta' => 'fw_brand_hidden',
    ),
  ),
);