<?php if (!defined( 'FW' )) die('Forbidden');

$options = array(
    'second_logo' => array(
        'type' => 'upload',
        'label' => __('Альтернативный логотип', 'tesoro-shop'),
        'desc' => __('Логотип для фиксированного меню сайта', 'tesoro-shop'),
    ),
);