<?php if (!defined( 'FW' )) die('Forbidden');

if (get_page_template_slug() != 'page-landing.php') {
    $options = array(
        'show_page_title_box' => array(
            'type' => 'box',
            'options' => array(
                'show_page_title' => array(
                    'type'  => 'checkbox',
                    'value' => true,
                    'label' => __('Показывать заголовок: ', 'tesoro-shop'),
                    'text'  => __('Да', 'tesoro-shop'),
                ),
            ),
            'title' => null,
        ),
    );
} else {
    $options = array(
        'page_logo_box' => array(
            'type' => 'box',
            'options' => array(
                'page_dark_logo' => array(
                    'type'  => 'upload',
                    'label' => __('Изображение для светлого фона', 'tesoro-shop'),
                    'images_only' => true,
                ),
                'page_light_logo' => array(
                    'type'  => 'upload',
                    'label' => __('Изображение для темного фона', 'tesoro-shop'),
                    'images_only' => true,
                ),
                'page_logo_alt'=> array(
                    'type' => 'text',
                    'label' => __('Alt текст логотипа', 'tesoro-shop'),
                    'value' => 'Roberto Bravo',
                ),
                'page_logo_link'=> array(
                    'type' => 'text',
                    'label' => __('Ссылка логотипа', 'tesoro-shop'),
                ),
            ),
            'title' => __('Логотип станицы', 'tesoro-shop'),
        ),
    );
}