<?php if (!defined( 'FW' )) die('Forbidden');

$options = array(
    'badge_box' => array(
        'type' => 'box',
        'options' => array(
            'is_new' => array(
                'type'  => 'checkbox',
                'value' => false,
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'ts_is_new',
                ),
                'label' => __('Новика', 'tesoro-shop'),
                'desc'  => __('Добавиляет товар в сладер <b>новинок</b> на главной странице', 'tesoro-shop'),
            ),
            'is_popular' => array(
                'type'  => 'checkbox',
                'value' => false,
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'ts_is_popular',
                ),
                'label' => __('Хит', 'tesoro-shop'),
                'desc'  => __('Добавиляет товар в сладер <b>хитов</b> на главной странице', 'tesoro-shop'),
            ),
            'is_sale' => array(
                'type'  => 'checkbox',
                'value' => false,
                'fw-storage' => array(
                    'type' => 'post-meta',
                    'post-meta' => 'ts_is_sale',
                ),
                'label' => __('Скидка', 'tesoro-shop'),
                'desc'  => __('Добавиляет товар в сладер <b>скидок</b> на главной странице', 'tesoro-shop'),
            ),
            'gift_box' => array(
                'type'  => 'multi-picker',
                'label' => false,
                'desc'  => false,
                'value' => array(
                    'gadget' => 'yes',
                ),
                'picker' => array(
                    'gadget' => array(
                        'label'   => __('Показывать подарочную упаковку?', 'tesoro-shop'),
                        'type'    => 'select',
                        'choices' => array(
                            'yes'  => __('Показывать', 'tesoro-shop'),
                            'no' => __('Скрыть', 'tesoro-shop')
                        )
                    )
                ),
                'choices' => array(
                    'no' => array(),
                    'yes' => array(
                        'gift_box_img' => array(
                            'type'  => 'upload',
                            'label' => __('Изображение подарочной упаковки', 'tesoro-shop'),
                        )
                    ),
                ),
                'show_borders' => false,
            ),
            'bundle_id' => array(
                'type' => 'text',
                'label'=> __('Bundle product ID', 'tesoro-shop'),
                'desc'=> __('Введите ID budle товара', 'tesoro-shop'),
            )
        ),
        'title' => null,
    ),
);