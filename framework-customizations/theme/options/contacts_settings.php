<?php if (!defined( 'FW' )) die('Forbidden');

$options = array(
  'main_page_settings' => array(
      'type' => 'tab',
      'options' => array(
        'phone_box' => array(
          'type'    => 'box',
          'title'   => 'Телефон',
          'options' => array(
            'phone' => array(
              'type'  => 'text',
              'value' => '+38 (063) 234 59 99',
              'label' => __('Телефон', 'tesoro-shop'),
            ),
          ),
        ),
        'product_slider_box' => array(
          'type'    => 'box',
          'title'   => 'Слайдер товаров',
          'options' => array(
            'glob_new_products' => array(
              'type'  => 'addable-option',
              'label' => __('Новинки', 'tesoro-shop'),
              'desc'  => __('Скоприруйте SLUG товара (указан в разделе "Постоянная ссылка" на странице товара сразу после product/)', 'tesoro-shop'),
              'option' => array( 'type' => 'text' ),
              'add-button-text' => __('Добавить', 'tesoro-shop'),
              'sortable' => true,
            ),
            'glob_sale_products' => array(
              'type'  => 'addable-option',
              'label' => __('Скидки', 'tesoro-shop'),
              'desc'  => __('Скоприруйте SLUG товара (указан в разделе "Постоянная ссылка" на странице товара сразу после product/)', 'tesoro-shop'),
              'option' => array( 'type' => 'text' ),
              'add-button-text' => __('Добавить', 'tesoro-shop'),
              'sortable' => true,
            ),
            'glob_popular_products' => array(
              'type'  => 'addable-option',
              'label' => __('Хиты', 'tesoro-shop'),
              'desc'  => __('Скоприруйте SLUG товара (указан в разделе "Постоянная ссылка" на странице товара сразу после product/)', 'tesoro-shop'),
              'option' => array( 'type' => 'text' ),
              'add-button-text' => __('Добавить', 'tesoro-shop'),
              'sortable' => true,
            )
          ),
        ),
        'social_links' => array(
          'type'    => 'box',
          'title'   => 'Социальные ссылки',
          'options' => array(
            // 'soc_vk' => array(
            //   'type' => 'text',
            //   'label' => __('VK', 'tesoro-shop'),
            //   'value' => '//vk.com'
            // ),
            'soc_facebook' => array(
              'type' => 'text',
              'label' => __('Facebook', 'tesoro-shop'),
              'value' => '//facebook.com'
            ),
            // 'soc_twitter' => array(
            //   'type' => 'text',
            //   'label' => __('Twitter', 'tesoro-shop'),
            //   'value' => '//twitter.com'
            // ),
            'soc_instagram' => array(
              'type' => 'text',
              'label' => __('Instagram', 'tesoro-shop'),
              'value' => '//instagram.com'
            ),
          ),
        ),
        'work_time' => array(
          'type' => 'text',
          'label'=> __('Время работы', 'tesoro-shop'),
          'value' => 'пн-пт: 10:00-18:00',
        ),
        'callback_shortcode' => array(
          'type' => 'text',
          'label'=> __('Шорткод формы обратного звонка', 'tesoro-shop'),
          'desc' => __('Требуется наличие плагина contact form 7', 'tesoro-shop'),
        ),
        'exchange_page_link' => array(
          'type' => 'text',
          'label' => __('Ссылка на страницу обмена', 'tesoro-shop'),
        ),
        'shipping_payment_page_link' => array(
          'type' => 'text',
          'label' => __('Ссылка а страницу Доставка и Оплата', 'tesoro-shop'),
        ),
        'sub_shortcode' => array(
          'type' => 'text',
          'label'=> __('Шорткод формы подписки', 'tesoro-shop'),
          'desc' => __('Требуется наличие плагина MailChimp for WordPress', 'tesoro-shop'),
        ),
        
      ),
      'title' => __('Настройки темы', 'tesoro-shop'),
  ),
);