<?php
/**
 * Template Name: Landing page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tesoro-shop
 */

get_header('landing');
?>

	<div id="primary" class="content-area">



					<?php
					while ( have_posts() ) : the_post();

						the_content();

					endwhile; // End of the loop.
					?>


	</div><!-- #primary -->

<?php
get_footer();
