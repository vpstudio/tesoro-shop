<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tesoro-shop
 */

if ( defined( 'FW' ) ) {
$soc_facebook_link = fw_get_db_settings_option('soc_facebook');
$soc_instagram_link = fw_get_db_settings_option('soc_instagram');
$phone = fw_get_db_settings_option('phone');
$work_time = fw_get_db_settings_option('work_time');
$sub_form = fw_get_db_settings_option('sub_shortcode');
}
$custom_logo_id = get_theme_mod( 'custom_logo' );
$custom_logo_url = !empty(wp_get_attachment_image_src($custom_logo_id, 'full', false)) ? wp_get_attachment_image_src($custom_logo_id, 'full', false)[0] : '';

?>

</div><!-- #content -->	
</div><!-- #page -->
	<div class="mailing">
        <div class="container">
            <div class="row">

                <!-- Mailing -->
                <div class="col-12 col-md-6 col-xl-3">
                    <div class="mailing__text">
                        <h4 class="mailing__title">
                            <?php _e('Дарим*', 'tesoro-shop'); ?>
                            <span><?php _e('-15%', 'tesoro-shop'); ?></span>
                            <br><?php _e(' на первую покупку', 'tesoro-shop'); ?></h4>
                        <p class="mailing__subtitle"><?php _e('за подписку на наши новости', 'tesoro-shop'); ?></p>
                    </div>
                </div>
        
                <div class="col-12 col-md-6 col-xl-3">
                    <div class="mailing__text-wrapper">
                        <div class="mailing__img">
                            <img src="<?php echo get_template_directory_uri() . '/dist/images/ring.png'; ?>" class="img-responsive" title="" alt="">
                        </div>
                    </div>
                </div>
        
                <div class="col-12 col-md-12 col-xl-5">
                    <div class="mailing__form">
                        <?php echo do_shortcode($sub_form); ?>
                    </div>
                </div>
        
                <div class="col-12 col-md-12 col-xl-1">
                    <div class="mailing__soc-icons">
                        <a href="<?php echo $soc_facebook_link; ?>" rel="noopener noreferrer nofollow" target="_blank" title="<?php echo $soc_facebook_link; ?>" >
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="mailto:<?php echo get_option('admin_email', ''); ?>" rel="noopener noreferrer nofollow" title="<?php echo get_option('admin_email', ''); ?>">
                            <i class="fa fa-envelope"></i>
                        </a>
                        <a href="<?php echo $soc_instagram_link; ?>" rel="noopener noreferrer nofollow" target="_blank" title="<?php echo $soc_instagram_link; ?>" >
                            <i class="fa fa-instagram"></i>
                        </a>
                    </div>
                </div>
                <!-- #Mailing -->
            </div>
        </div>
	</div>
	
    <footer class="footer">
        <div class="container">
            <div class="row">

                <div class="col-12 col-xl-2">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="footer__logo">
                        <img src="<?php echo $custom_logo_url; ?>" rel="noopener noreferrer nofollow" class="img-responsive" title="<?php echo get_bloginfo('description'); ?>" alt="<?php echo get_bloginfo('description'); ?>">
                    </a>
                    <div class="footer__soc-icons">
                        <a href="<?php echo $soc_facebook_link; ?>"  rel="noopener noreferrer nofollow" target="_blank" title="<?php echo $soc_facebook_link; ?>">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="<?php echo $soc_instagram_link; ?>" rel="noopener noreferrer nofollow" target="_blank" title="<?php echo $soc_instagram_link; ?>">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a href="mailto:<?php echo get_option('admin_email', ''); ?>" rel="noopener noreferrer nofollow" title="<?php echo get_option('admin_email', ''); ?>">
                            <i class="fa fa-envelope"></i>
                        </a>
                    </div>
                </div>
                            
                <div class="col-12 order-2 col-xl-8 order-xl-1">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'menu-4',
                            'menu_id'        => 'footer-menu',
                            'container'			 => false,
                            'menu_class'		 => 'footer__nav'
                        ) );
                    ?>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'menu-5',
                            'menu_id'        => 'catalog-footer-menu',
                            'container'			 => false,
                            'menu_class'		 => 'footer__cat-nav'
                        ) );
                    ?>
                    <!--div class="payment-list">
                        <div class="payment-list__item">
                            <img src="<?php //echo get_template_directory_uri() . '/dist/images/visa.svg'; ?>" alt="" title="">
                        </div>
                        <div class="payment-list__item">
                            <img src="<?php //echo get_template_directory_uri() . '/dist/images/mastercard.svg'; ?>" alt="" title="">
                        </div>
                        <div class="payment-list__item">
                            <img src="<?php //echo get_template_directory_uri() . '/dist/images/PayPal.svg'; ?>" alt="" title="">
                        </div>
                        <div class="payment-list__item">
                            <img src="<?php //echo get_template_directory_uri() . '/dist/images/postepay.svg'; ?>" alt="" title="">
                        </div>
                        <div class="payment-list__item">
                            <img src="<?php //echo get_template_directory_uri() . '/dist/images/american-express.svg'; ?>" alt="" title="">
                        </div>
                    </div-->
                    <div class="copyright">
                        Copyright © <?php echo date("Y"); ?> TESORO. All rights reserved. Designed by
                        <a href="https://vp-production.com/"  rel="nofollow" target="_blank" >VP Production</a>
                    </div>
                </div>
                            
                <div class="col-12 order-1 col-xl-2 order-xl-2">
                    <div class="footer__contacts">
                        <a href="tel:<?php echo str_replace([' ', '(', ')', '+', '-'], '', $phone); ?>" rel="noopener noreferrer nofollow" class="footer__phone">
                            <i class="fa fa-phone"></i>  <?php echo $phone; ?>
                        </a>
                        <a href="<?php echo esc_url( home_url( '/our-stores' ) ); ?>" class="footer__location">
                            <span>
                                <i class="fa fa-map-marker"></i>
                            </span> <?php _e('Наши магазины', 'tesoro-shop'); ?>
                        </a>
                        <p class="footer__worktime">
                            <?php echo $work_time; ?>
                        </p>
                    </div>
                </div>
                            
            </div>
        </div>
    </footer>

<?php wp_footer(); ?>

</body>
</html>
