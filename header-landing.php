<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tesoro-shop
 *
 *
 * Detect plugin. For use on Front End only.
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

global $woocommerce;
if ( defined( 'FW' ) ) {	
if (get_page_template_slug() == 'page-landing.php') {
$custom_logo_url_d = fw_get_db_post_option( get_the_ID(), 'page_dark_logo');
$custom_logo_url_l = fw_get_db_post_option( get_the_ID(), 'page_light_logo');
$custom_logo_alt = fw_get_db_post_option( get_the_ID(), 'page_logo_alt');
$custom_logo_link = fw_get_db_post_option( get_the_ID(), 'page_logo_link');
}
$soc_facebook_link = fw_get_db_settings_option('soc_facebook');
$soc_instagram_link = fw_get_db_settings_option('soc_instagram');
$phone = fw_get_db_settings_option('phone');
$work_time = fw_get_db_settings_option('work_time');
$callback_shortcode = fw_get_db_settings_option('callback_shortcode');
}
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#000">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#000">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div id="page" class="site">
        <header class="landing-page-header">
            <div class="mobile-header">
                <div class="mobile-header__wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-4 d-flex align-items-center justify-content-between">
                                <a href="javascript:void(0)" rel="noopener noreferrer nofollow" class="mobile-header__nav-button overlay-menu-openbtn">
                                    <span>
                                        <i class="fa fa-bars"></i>
                                    </span>
                                </a>
                                <a href="javascript:void(0)" rel="noopener noreferrer nofollow" class="mobile-header__search-button search-products mobile-search-handler">
                                    <span>
                                        <i class="fa fa-search"></i>
                                    </span>
                                </a>
                            </div>
                            <div class="col-4 d-flex align-items-center justify-content-between">
                                <a href="<?php echo $custom_logo_link; ?>" title="<?php echo $custom_logo_alt; ?>" class="mobile-header__logo">
                                    <img src="<?php echo $custom_logo_url_l['url']; ?>" class="img-responsive" title="<?php echo $custom_logo_alt; ?>" alt="<?php echo $custom_logo_alt; ?>">
                                </a>
                            </div>
                            <div class="col-4 d-flex align-items-center justify-content-between">
                                <a href="tel:<?php echo str_replace([' ', '(', ')', '+', '-'], '', $phone); ?>" rel="noopener noreferrer nofollow" class="mobile-header__phone-button">
                                    <span>
                                        <i class="fa fa-phone "></i>
                                    </span>
                                </a>
                                <a href="<?php echo wc_get_cart_url() ?>" rel="noopener noreferrer nofollow" class="mobile-header__card-button">
                                    <div class="header__card-counter">
                                        <i class="fa fa-shopping-cart"></i> <?php echo '<span class="card-count">'. sprintf($woocommerce->cart->cart_contents_count) .'</span>'; ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="search-bar mobile-search-bar">
                    <div class="container">
                        <div class="row">
                            <div class="search-form">
                            <?php if( is_plugin_active( 'ajax-search-for-woocommerce/ajax-search-for-woocommerce.php' ) ) { echo do_shortcode('[wcas-search-form]'); } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tablet-header">
                <div class="tablet-header__wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col d-flex align-items-center justify-content-around">
                                <a href="javascript:void(0)" rel="noopener noreferrer nofollow" class="tablet-header__nav-button overlay-menu-openbtn">
                                    <span>
                                        <i class="fa fa-bars"></i>
                                    </span>
                                </a>
                                <a href="<?php echo esc_url( home_url( '/our-stores' ) ); ?>" class="tablet-header__map-marker-alt-button">
                                    <span>
                                        <i class="fa fa-map-marker"></i>
                                    </span> <?php _e('Наши магазины', 'tesoro-shop'); ?>
                                </a>
                                <a href="javascript:void(0)" rel="noopener noreferrer nofollow" class="tablet-header__search-button search-products tablet-search-handler">
                                    <span>
                                        <i class="fa fa-search"></i>
                                    </span> <?php _e('Поиск товара', 'tesoro-shop'); ?>
                                </a>
                            </div>
                            <div class="col d-flex align-items-center justify-content-center">
                                <a href="<?php echo $custom_logo_link; ?>" title="<?php echo $custom_logo_alt; ?>" class="tablet-header__logo">
                                    <img src="<?php echo $custom_logo_url_l['url']; ?>" class="img-responsive" title="<?php echo $custom_logo_alt; ?>" alt="<?php echo $custom_logo_alt; ?>">
                                </a>
                            </div>
                            <div class="col d-flex align-items-center justify-content-around">
                                <a href="tel:<?php echo str_replace([' ', '(', ')', '+', '-'], '', $phone); ?>" rel="noopener noreferrer nofollow" class="tablet-header__phone-button">
                                    <span>
                                        <i class="fa fa-phone "></i>
                                    </span> <?php echo $phone; ?>
                                </a>
                                <a href="<?php echo wc_get_cart_url() ?>" rel="noopener noreferrer nofollow"  class="tablet-header__card-button">
                                    <div class="header__card-counter">
                                        <span><i class="fa fa-shopping-cart"></i></span> <?php _e('Корзина', 'tesoro-shop'); ?> <?php echo '<span class="card-count">'. sprintf($woocommerce->cart->cart_contents_count) .'</span>'; ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="search-bar tablet-search-bar">
                    <div class="container">
                        <div class="row">
                            <div class="search-form">
                            <?php if( is_plugin_active( 'ajax-search-for-woocommerce/ajax-search-for-woocommerce.php' ) ) { echo do_shortcode('[wcas-search-form]'); } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="desktop-header">
                <div class="top-line">
                    <div class="container">
                        <div class="row">
                            <div class="col-2">
                                <div class="top-line__phone callback-block">
                                    <a rel="noopener noreferrer nofollow" href="tel:<?php echo str_replace([' ', '(', ')', '+', '-'], '', $phone); ?>"><i class="fa fa-phone "></i>  <?php echo $phone; ?> <i class="fa fa-angle-down"></i></a>
                                    <div class="callback-block-dropdown">
                                        <p class="work-time"><?php echo $work_time; ?></p>
                                        <a class="callback" rel="noopener noreferrer nofollow" href="#callback-form"><?php _e('Обратный звонок', 'tesoro-shop'); ?></a>
                                        <div id="callback-form" class="callback-form white-popup-block mfp-hide">
                                            <?php echo !empty($callback_shortcode) ? do_shortcode($callback_shortcode, true) : 'paste form shortcode here'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8">
                                <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'menu-1',
                                        'menu_id'        => 'top-line-menu',
                                        'container'		 => false,
                                        'menu_class'	 => 'top-line__nav'
                                    ) );
                                ?>
                            </div>
                            <div class="col-2 d-flex align-items-center justify-content-between">
                                <ul class="top-line__soc-icons">
                                    <li>
                                        <a rel="noopener noreferrer nofollow" href="<?php echo $soc_instagram_link; ?>" target="_blank" title="<?php echo $soc_instagram_link; ?>">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="noopener noreferrer nofollow" href="<?php echo $soc_facebook_link; ?>" target="_blank" title="<?php echo $soc_facebook_link; ?>">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                </ul>
                                <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'menu-6',
                                        'menu_id'        => 'top-line-langs',
                                        'container'		 => false,
                                        'menu_class'	 => 'top-line__langs'
                                    ) );
                                ?>
                                <?php
                                    if ( is_user_logged_in() ) {
                                        echo '<a class="top-line__account" rel="noopener noreferrer nofollow" href="'. get_permalink( get_option('woocommerce_myaccount_page_id') ) .'" title="'. __('Кабинет', 'tesoro-shop') .'" >'. __('Кабинет', 'tesoro-shop') .'</a>';
                                    } else {
                                        echo '<a class="top-line__account" rel="noopener noreferrer nofollow" href="'. get_permalink( get_option('woocommerce_myaccount_page_id') ) .'" title="'. __('Войти', 'tesoro-shop') .'" >'. __('Войти', 'tesoro-shop') .'</a>';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="desktop-header__wrapper">
                                <div class="desktop-header__col">
                                    <a href="<?php echo $custom_logo_link; ?>" title="<?php echo $custom_logo_alt; ?>" class="desktop-header__logo">
                                        <img src="<?php echo $custom_logo_url_d['url']; ?>" class="img-responsive" title="<?php echo $custom_logo_alt; ?>" alt="<?php echo $custom_logo_alt; ?>">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="catalog-nav">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'menu-2',
                                        'menu_id'        => 'catalog-nav-menu',
                                        'container'			 => false,
                                        'menu_class'		 => 'catalog-nav__menu'
                                    ) );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="overlay-menu-bg" data-show="false"></div>
            <div class="overlay-menu" data-show="false">
                <nav class="overlay-menu__nav">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'menu-3',
                            'menu_id'        => 'catalog-nav-menu',
                            'container'	     => false,
                            'menu_class'	 => ''
                        ) );
                    ?>
                </nav>
                <div class="overlay-menu__content">
                    <?php
                        if ( is_user_logged_in() ) {
                            echo '<a class="overlay-menu__account" href="'. get_permalink( get_option('woocommerce_myaccount_page_id') ) .'" title="'. __('Кабинет', 'tesoro-shop') .'" >'. __('Кабинет', 'tesoro-shop') .'</a>';
                        } else {
                            echo '<a class="overlay-menu__account" href="'. get_permalink( get_option('woocommerce_myaccount_page_id') ) .'" title="'. __('Войти', 'tesoro-shop') .'" >'. __('Войти', 'tesoro-shop') .'</a>';
                        }
                    ?>
                    <?php if( is_plugin_active( 'ti-woocommerce-wishlist/ti-woocommerce-wishlist.php' ) ) { echo '<div class="woocommerce-wishlist">'. do_shortcode('[ti_wishlist_products_counter]', true) .'</div>'; } ?>
                    <ul class="overlay-menu__soc-icons">
                        <li>
                            <a href="<?php echo $soc_instagram_link; ?>" target="_blank" title="<?php echo $soc_instagram_link; ?>">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $soc_facebook_link; ?>" target="_blank" title="<?php echo $soc_facebook_link; ?>">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:<?php echo get_option('admin_email', ''); ?>" title="<?php echo get_option('admin_email', ''); ?>">
                                <i class="fa fa-envelope"></i>
                            </a>
                        </li>
                    </ul>
                    <a href="tel:<?php echo str_replace([' ', '(', ')', '+', '-'], '', $phone); ?>"><i class="fa fa-phone "></i>  <?php echo $phone; ?></a>
                    <a href="<?php echo esc_url( home_url( '/our-stores' ) ); ?>" class="overlay-menu__map-marker-alt-button">
                        <span>
                            <i class="fa fa-map-marker"></i>
                        </span> <?php _e('Наши магазины', 'tesoro-shop'); ?>
                    </a>
                    <p class="work-time"><?php echo $work_time; ?></p>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'menu-6',
                            'menu_id'        => 'overlay-menu-langs',
                            'container'		 => false,
                            'menu_class'	 => 'overlay-menu__langs'
                        ) );
                    ?>
                    <a class="callback" href="#callback-form"><?php _e('Обратный звонок', 'tesoro-shop'); ?></a>
                    
                </div>
            </div>
            <div class="sticky-nav">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <a href="javascript:void(0)" rel="noopener noreferrer nofollow" class="mobile-header__nav-button overlay-menu-openbtn">
                                <span>
                                    <i class="fa fa-bars"></i>
                                </span>
                            </a>
                            <div class="callback-block">
                                <a href="tel:<?php echo str_replace([' ', '(', ')', '+', '-'], '', $phone); ?>" rel="noopener noreferrer nofollow" >
                                <span>
                                    <i class="fa fa-phone "></i>
                                </span> <?php echo $phone; ?>
                                <i class="fa fa-angle-down"></i></a>
                                <div class="callback-block-dropdown">
                                    <p class="work-time"><?php echo $work_time; ?></p>
                                    <a class="callback" rel="noopener noreferrer nofollow" href="#callback-form"><?php _e('Обратный звонок', 'tesoro-shop'); ?></a>
                                    <div id="callback-form" class="callback-form white-popup-block mfp-hide">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col justify-content-center">
                            <a href="<?php echo $custom_logo_link; ?>" title="<?php echo $custom_logo_alt; ?>" class="sticky-nav__logo">
                                <img src="<?php echo $custom_logo_url_d['url']; ?>" class="img-responsive" alt="<?php echo $custom_logo_alt; ?>">
                            </a>
                        </div>
                        <div class="col">
                            <?php if( is_plugin_active( 'ti-woocommerce-wishlist/ti-woocommerce-wishlist.php' ) ) { echo do_shortcode('[ti_wishlist_products_counter]', true); } ?>
                            <a href="<?php echo wc_get_cart_url() ?>" rel="noopener noreferrer nofollow" class="desktop-header__card-button">
                                <div class="header__card-counter">
                                    <span><i class="fa fa-shopping-cart"></i></span> <?php _e('Корзина', 'tesoro-shop'); ?> <span class="card-count"><?php echo sprintf($woocommerce->cart->cart_contents_count); ?></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="content" class="site-content">
