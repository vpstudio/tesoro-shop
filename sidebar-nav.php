<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tesoro-shop
 */

if ( ! is_active_sidebar( 'nav-filterbar' ) ) {
	return;
}
?>

<aside id="nav-secondary" class="widget-area">
	<?php dynamic_sidebar( 'nav-filterbar' ); ?>
</aside><!-- #nav-secondary -->