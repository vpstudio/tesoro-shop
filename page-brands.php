<?php
/**
 * Template Name: Brands
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tesoro-shop
 */
$popular_products = wc_get_products( array(
  'status'   => 'publish',
  'per_page'    => 6,
    'order'	   => 'DESC',
    'orderby'  => 'meta_value_num',
    'meta_key' => '_wc_average_rating',
));

$terms = get_terms(array(
  'taxonomy' => 'brand',
  'hide_empty' => false,
));

$brand_terms = array();
foreach ($terms as $term) {
  $brand_vis = fw_get_db_term_option($term->term_id, 'brand', 'hide_brand', '');
  if ( $brand_vis ) continue;
  $brand_image = fw_get_db_term_option($term->term_id, 'brand', 'brand_image', null);
  $brand_image_link = $brand_image != null ? $brand_image['url'] : '';
  $brand_image_id = $brand_image != null ? $brand_image['attachment_id'] : '';
  $brand_image_alt = get_post_meta( $brand_image_id, '_wp_attachment_image_alt', true);
  $brand_terms[] = array(
    'id'         => $term->term_id,
    'title'      => $term->name,
    'image_link' => $brand_image_link,
    'image_alt'  => $brand_image_alt
  );
}

get_header();
?>

<div class="container">
  <div class="row">
      <div class="col"><?php woocommerce_breadcrumb(); ?></div>
  </div>
</div>
<div id="primary" class="content-area">
    <section class="section section--brands-all">
      <h1 class="section__title"><?php _e('Наши бренды', 'tersoro-shop'); ?></h1>
      <div class="container">
        <div class="row">
          <div class="col">
            <div class="brands-all--mobile owl-carousel owl-theme">
              <?php foreach ($brand_terms as $brand_term) { ?>
                <a href="<?php echo get_term_link($brand_term['id'], 'brand'); ?>" class="brand-item" title="<?php echo $brand_term['title'] ?>">
                  <img src="<?php echo $brand_term['image_link'] ?>" class="img-responsive brands-all__image" alt="<?php echo $brand_term['image_alt'] ?>">
                  <span class="brands-all__ttl"><?php echo $brand_term['title'] ?></span>
                </a>
              <?php } ?>
            </div>
            <div class="brands-all--desktop"></div>
          </div>
        </div>
      </div>
    </section>

    <?php while ( have_posts() ) : the_post();
      the_content();
    endwhile; // End of the loop. ?>

    <!-- brands-product-slider -->
    <section id="brands-product-slider" class="section section--brands-slider">
      <div class="container">
        <div class="row">
          <div class="col d-flex justify-content-center">
            <h2 class="section__title"><?php _e('Хиты продаж', 'tersoro-shop'); ?></h2>
          </div>
        </div>
        <div class="row">
            <div class="col">
                <div data-slider="popular" class="brands-product-slider owl-carousel owl-theme">
                    <?php echo ts_build_products( $popular_products, 'popular' ); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="section__button-wrapper">
                    <a href="<?php echo get_post_type_archive_link( 'product' ); ?>" class="button section__button"><?php _e( 'ПЕРЕЙТИ В КАТАЛОГ', 'tesoro-shop' ); ?></a>
                </div>
            </div>
        </div>
      </div>
    </section>
    <!-- #brands-product-slider -->

</div><!-- #primary -->

<?php
get_footer();
