<?php
/**
 * 
 * This is the template that displays brand single pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tesoro-shop
 */
fw()->backend->option_type('icon-v2')->packs_loader->enqueue_frontend_css();
$term_id = get_queried_object_id();
$term = get_term($term_id);
$brand_image = fw_get_db_term_option($term_id, 'brand', 'brand_image', null);
$brand_text = fw_get_db_term_option($term_id, 'brand', 'brand_text', null);
$left_soc = fw_get_db_term_option($term_id, 'brand', 'left_soc', null);
$right_soc = fw_get_db_term_option($term_id, 'brand', 'right_soc', null);
$brand_slider = fw_get_db_term_option($term_id, 'brand', 'brand_slider', null);

$video_visible = fw_get_db_term_option($term_id, 'brand', 'video_visible', null);
$video_brand_link = fw_get_db_term_option($term_id, 'brand', 'video_brand_link', null);
$video_brand_title = fw_get_db_term_option($term_id, 'brand', 'video_brand_title', null);
$video_brand_image = fw_get_db_term_option($term_id, 'brand', 'video_brand_image', null) != null ? fw_get_db_term_option($term_id, 'brand', 'video_brand_image', null) : '';

$brand_cats = fw_get_db_term_option($term_id, 'brand', 'brand_cats', null);

$product_ids = get_posts(array(
  'post_type' => 'product',
  'numberposts' => -1,
  'fields' => 'ids',
  'tax_query' => array(
    array(
      'taxonomy' => 'brand',
      'field' => 'id',
      'terms' => $term_id, // Where term_id of Term 1 is "1".
      'include_children' => false
    )
  )
));

get_header();
?>
<div class="container">
  <div class="row">
      <div class="col"><?php woocommerce_breadcrumb(); ?></div>
  </div>
</div>
<div id="primary" class="content-area">
    <section class="section section--brands-single-info">
      <div class="container brands-single-info">
        <div class="row">
          <div class="col d-flex justify-content-center">
            <h1 class="section__title"><?php echo $term->name; ?></h1>
          </div>
        </div>
        <div class="row align-items-center justify-content-center">
          <?php if ( !empty($brand_image) ) { 
            $image = $brand_image['url'];
            $image_alt = get_post_meta( $brand_image['attachment_id'], '_wp_attachment_image_alt', true);
          ?>
          <div class="col-12 col-md-4 order-md-2 col-xl-4 order-xl-2 brands-single-info__logo">
            <img src="<?php echo $image; ?>" alt="<?php echo $term->name; ?>" class=" img-responsive">
          </div>
          <?php } ?>
          <div class="col-6 col-md-4 order-md-1 col-xl-4 order-xl-1 brands-single-info__link brands-single-info__link--left">
          <?php foreach ( $left_soc as $soc_link ) {
            $soc_icon = $soc_link['icon']['type'] != 'none' ? $soc_link['icon']['icon-class'] : '';

            switch ($soc_link['link_type']) {
              case 'email':
                $soc_icon = !empty($soc_icon) ? $soc_icon : 'fa fa-envelope';
                echo '<a href="mailto:'. $soc_link['link'] .'" rel="noopener noreferrer nofollow" title="'. $soc_link['title'] .'" ><span>'. $soc_link['title'] .'</span><i class="'. $soc_icon .'"></i></a>';
                break;
              case 'phone':
                $soc_icon = !empty($soc_icon) ? $soc_icon : 'fa fa-phone-volume';
                echo '<a href="tel:'. str_replace([' ', '(', ')', '+', '-'], '', $soc_link['link']) .'" rel="noopener noreferrer nofollow" title="'. $soc_link['title'] .'" ><span>'. $soc_link['title'] .'</span><i class="'. $soc_icon .'"></i></a>';  
                break;
              case 'link':
                $soc_icon = !empty($soc_icon) ? $soc_icon : 'fa fa-globe-americas';
                echo '<a href="'. $soc_link['link'] .'" rel="noopener noreferrer nofollow" title="'. $soc_link['title'] .'" ><span>'. $soc_link['title'] .'</span><i class="'. $soc_icon .'"></i></a>';  
                break;
              default:
                $soc_icon = !empty($soc_icon) ? $soc_icon : 'fa fa-globe-americas';
                echo '<a href="'. $soc_link['link'] .'" rel="noopener noreferrer nofollow" title="'. $soc_link['title'] .'" ><span>'. $soc_link['title'] .'</span><i class="'. $soc_icon .'"></i></a>';  
                break;
            }
          } ?>
          </div>
          <div class=" col-6 col-md-4 order-md-3 col-xl-4 order-xl-3  brands-single-info__link brands-single-info__link--right">
          <?php foreach ( $right_soc as $soc_link ) {
            $soc_icon = $soc_link['icon']['type'] != 'none' ? $soc_link['icon']['icon-class'] : '';

            switch ($soc_link['link_type']) {
              case 'email':
                $soc_icon = !empty($soc_icon) ? $soc_icon : 'fa fa-envelope';
                echo '<a href="mailto:'. $soc_link['link'] .'" rel="noopener noreferrer nofollow" title="'. $soc_link['title'] .'" ><span>'. $soc_link['title'] .'</span><i class="'. $soc_icon .'"></i></a>';
                break;
              case 'phone':
                $soc_icon = !empty($soc_icon) ? $soc_icon : 'fa fa-phone-volume';
                echo '<a href="tel:'. str_replace([' ', '(', ')', '+', '-'], '', $soc_link['link']) .'" rel="noopener noreferrer nofollow" title="'. $soc_link['title'] .'" ><span>'. $soc_link['title'] .'</span><i class="'. $soc_icon .'"></i></a>';  
                break;
              case 'link':
                $soc_icon = !empty($soc_icon) ? $soc_icon : 'fa fa-globe-americas';
                echo '<a href="'. $soc_link['link'] .'" rel="noopener noreferrer nofollow" target="_blank" title="'. $soc_link['title'] .'" ><span>'. $soc_link['title'] .'</span><i class="'. $soc_icon .'"></i></a>';  
                break;
                default:
                $soc_icon = !empty($soc_icon) ? $soc_icon : 'fa fa-globe-americas';
                echo '<a href="'. $soc_link['link'] .'" rel="noopener noreferrer nofollow" target="_blank" title="'. $soc_link['title'] .'" ><span>'. $soc_link['title'] .'</span><i class="'. $soc_icon .'"></i></a>';
                break;
            }
          } ?>
          </div>
        </div>

        <div class="row" style="padding-top: 50px;">
          <div class="col">
            <div class="brand-text">
              <?php echo $brand_text; ?>
            </div>
          </div>
        </div>
        
        <?php if ( $brand_cats['gadget'] == 'show' && !empty($brand_cats['show']['brand_cat_list']) ) { ?>
        <div class="row added__category">
          <?php
            $brand_cat_list = $brand_cats['show']['brand_cat_list'];
            foreach ($brand_cat_list as $brand_cat) { 
              if ( empty($brand_cat['brand_cat_image']) ) continue;

              $image = $brand_cat['brand_cat_image']['url'];
              $image_alt = get_post_meta( $brand_cat['brand_cat_image']['attachment_id'], '_wp_attachment_image_alt', true);  
            ?>
            <div class="col-6">
            <h6 class="section__title"><?php echo $brand_cat['brand_cat_title']; ?></h6>
            <a href="<?php echo $brand_cat['brand_cat_link']; ?>">
            <img class="img-responsive" src="<?php echo $image; ?>" alt="">
            </a>
            </div>
            <?php }  ?>
        </div>
        <?php } ?>
      </div>
    </section>

    <?php if (!empty($brand_slider)) { ?>
    <section class="section--gallery-slider">
      <div class="container gallery-slider owl-carousel owl-theme">
        <?php foreach ( $brand_slider as $brand_slide ) {
          if ( empty($brand_slide['image']) ) continue;
          $image = $brand_slide['image']['url'];
          $image_alt = get_post_meta( $brand_slide['image']['attachment_id'], '_wp_attachment_image_alt', true);
        ?>
          <div class="gallery-slider__item ">
            <img src="<?php echo $image; ?>" alt="<?php echo $image_alt; ?>">
          </div>
        <?php } ?>
      </div>
    </section>
    <?php } ?>

    <?php if ($video_visible == 'show') { ?>
    <section class="section section--video">
      <div class="video">
          <div class="video__bg" style="background-image: url(<?php echo $video_brand_image['url']; ?>)"></div>
          <div class="video__wrapper">
                  <a href="<?php echo $video_brand_link; ?>" rel="noopener noreferrer nofollow" title="<?php echo $video_brand_title; ?>" class="popup-youtube video__content">
                      <svg width="144" height="144" viewBox="0 0 144 144" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M66.1607 27.3281L27.625 48.7433C27.1801 48.9948 26.7932 49.0238 26.4643 48.8304C26.1548 48.6369 26 48.2887 26 47.7857V5.07143C26 4.56845 26.1548 4.22024 26.4643 4.02679C26.7932 3.83333 27.1801 3.86235 27.625 4.11384L66.1607 25.529C66.6057 25.7805 66.8281 26.0804 66.8281 26.4286C66.8281 26.7768 66.6057 27.0766 66.1607 27.3281Z" transform="translate(32 46)" fill="#FFF200"></path>
                          <circle cx="72" cy="72" r="67" stroke="#FFF200" stroke-width="10"></circle>
                      </svg>
                  </a>
          </div> 
      </div>
    </section>
    <?php } ?>


    <?php if (!empty($product_ids)) { 

    $brand_produts = wc_get_products( array(
      'status'   => 'publish',
      'include' => $product_ids,
      'per_page'    => 6,
      'order'	   => 'DESC',
    ));

    ?>

    <!-- brand-slider -->
    <section id="brand-slider" class="section section--brand-slider">
      <div class="container">
        <div class="row">
          <div class="col text-center">
            <h2 class="section__title"><?php _e('Товары бренда', 'tesoro-shop'); ?></h2>
          </div>
        </div>
        <div class="row">
            <div class="col">
                <div data-slider="new" class="brand-slider owl-carousel owl-theme">
                    <?php echo ts_build_products( $brand_produts, 'new' ); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="section__button-wrapper">
                    <a href="<?php echo get_post_type_archive_link( 'product' ) . 'filters/brend/' . $term->slug ; ?>" class="button section__button"><?php _e( 'ПЕРЕЙТИ В КАТАЛОГ', 'tesoro-shop' ); ?></a>
                </div>
            </div>
        </div>
      </div>
    </section>

   


<?php } ?>
</div>


<?php
get_footer();